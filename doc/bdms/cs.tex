\documentstyle[12pt,fullpage]{article}
%\documentstyle[14pt]{iheprep}
\begin{document}


\newenvironment{VL}[1]
   {\begin{list}{}%
     { \settowidth{\labelwidth}{bf #1}
       \setlength{\leftmargin}{\labelwidth}
       \renewcommand{\makelabel}[1]{\bf ##1\hfil}
     }%
   }%
{\end{list}}

\begin{titlepage}
\begin{center}
INSTITUTE FOR HIGH ENERGY PHYSICS\\
\vskip 3cm
   {COMPAS group}
%{Alekhin S.I., Ezhela V.V., Lugovsky S.B.,Nikolaev A.S.\\
%            Striganov S.I., Yushchenko O.P.}
\vskip 2cm

{\Huge PPDS.CS data base}  \\
\vskip 1cm
{\Large Integrated characteristics of reactions in particles collisions\\
              (description, revised December 1993)}

%\vskip 2cm
\vfill
Protvino 1993
\end{center}
\end{titlepage}
\newpage
\vtop{\hbox{\ }}  \vskip 4cm
\tableofcontents

\newpage
\vtop{\hbox{\ }}  \vskip 6cm
\begin{abstract}
  The description of computerized data compilation on integrated  
 characteristics of particle's collisions is presented. The scope of data
base are:
 inclusive and exclusive reaction integrated cross sections, 
 mean charged particles 
 multiplicity and  individual
 multiplicities  of  hadrons  produced  in hadron and lepton
 collisions. Compilation is maintained and developed in  the
 framework  of  Particle  Physics Data System (PPDS) under
 BDMS.V.4
\end{abstract}

\newpage
\vtop{\hbox{\ }}  \vskip 4cm
\section{INTRODUCTION}

    The data base  CS  (Cross  Sections)  includes  the  data  on
 integrated properties of particles collisions and is a development
 and update  of  the  HERA(CERN) and HERA-COMPAS compilations\cite{hera}.  
 The  CS  is
 maintained as one of the data bases of the Particle Physics Data
 System - PPDS\cite{ppds}, under BDMS.V.4\cite{bdms}

    This version of the CS data base contains the mostly full set
 of formally evaluated data covered the following sections:
\begin{itemize}
\item total cross sections of exclusive  and  inclusive  reaction
      (in full phase space, as a rule);
\item    cross section for $\gamma$ induced reactions;
\item  $\sigma/E$ for neutrino induced reactions;
\item  mean charged hadrons multiplicities;
\item  partial hadron multiplicities.
\end{itemize}
    The collection of the CS data base and formal  evaluation  of
 data have been made in several stages: merging of HERA(CERN)
 compilations translated on PPDL (Particle Physics Data Language)
 with  data from old UCRL and LBL compilations.

    In all cases the data were checked with original publications
 before storing in the CS data base. Practically all experimental
 data points presented in CS data base  are  twice  checked  with
 original publications.

    Where it was possible the preliminary data  are  marked  with
 special  mark  that  makes  it  easy to separate them from final
 results. Another special key marks the data obtained in not full
 phase space and/or model dependent data.

    The PPDL language and highly developed program tools  give  a
 possibility   to   establish   a   strong  control  of  reaction
 description and,  as  a  result,  reduces  an  amount  of  wrong
 descriptors  and,  in  many  cases,  rejects errors arosing from
 misprints in journals.

    CS data  compilation is  now  updated  regularly  by  data  from
the  REACTIONS data base\cite{ppds},\cite{rdb}.

\newpage
\section{DATA STRUCTURE IN COMPILATION}

    Basic unit of the CS data base under BDMS is a record - a set
 of data elements connected by hierarchical relations.

    A record contains the data belonging to  one  reaction  (RE),
 one observable (YN) and extracted from one publication (R).

    The record structure is shown in fig.1. The data elements are
 listed in table 1.
\begin{verbatim}

                        CS Data Structure.

  Biblio    Reaction        Data      Experimental     Service
  graphy   Descriptors     Points      Conditions    Descriptors
 *============================*========*=================*======*
 | ||| | | | | | | | | |   |  |        |   |  | | |    | |  | | |
 SC||| | RE| | | | | | |   YN |        |  KIN | |SIG   | |DEN | cr
   ||| |   | | | | | |MODE    |        |      | |      | |    |
   R|| |  REC| | | | |        |        |    CORR|     SB |   DCH
    || |     | | | | |      point     sys       |        |
    D| |    BP'| | |MULT   / | | \   /   \    COND       |
     | |       | | |     btm | |  y csys  \           ___ds__
     TY|      TP'|brv        | |  |     sys-err      /   |   \
       |         |        PLAB | ysys              DSA  DSR  dcc
       RR       FSP'           |
                              ECM



                             Fig. 1
\end{verbatim}

    Key data elements are in upper case.  The  quote  symbol  (')
 marks  the  virtual  data  elements,  which  are  created by the
 program from other data.

    The data elements fall into several categories, depending  on
 the information they contain:
\begin{itemize}
\item Bibliographic information:  SC, D, TY, R, RR.
\item The reaction description: RE, REC, MULT, MODE,  FSP,  BP, TP, BRV.
\item The experimental data description: YN, POINT, BTM,  PLAB,
 ECM, Y. These data elements contain the information about
 an observable, a value of  observable  and  a  values  of
 kinematic variables.
\item The  experimental  conditions  description:  SYS,   CSYS,
 SYS-ERR,  YSYS,  COND, CORR. These data elements contains
 information about systematical  errors,  corrections  and
 experimental conditions.
\item Comments to the data: KIN, SIG, CR.
\item Information on expert data evaluation: DS, DSA, DSR, DSC.
\item Service data elements:  REC-ID, SB, DEN, DCH.
\end{itemize}
    Next sections contain the full description of  data  elements
 and their admissable values.

\subsection{BIBLIOGRAPHIC INFORMATION}

\begin{description}
\item[SC]      is  short code.
         SC =$<$first author's first name$>\quad<$two  last  digits  of
         the year of publication$>$. For example: SC = ANTIPOV 85;

\item[D]        is the publication year. For example: D = 1987;

\item[TY]  is the publication type. According to the ideology of CS
         data  base, it contains the data points from the journal
         publications as a rule. We have restricted  ourselves  by
         this   choice,   keeping   in   mind  that  the  journal
         publications have an additional quality check during the
         editorial reading. But it is possible to insert into the
         data base the "hot" data from other sources - preprints,
         conference  proceedings  and  so on. Following types are
         allowed: \\

          JOUR - journal,
                PRER - preprint,
                CONF - conference,
                THES - thesis.\\

\item[R]  is the reference of the publication.
        The format of R for articles from journals :
        R =$<$PPDL-abbreviation of journal$> <$volume$>$,$<$page$>$;
        For example:  R = PR D25, 1034;
        PPDL-abbreviations see in the VOCABULARY data base [4].

        The format of R for preprints:
        R =$<$PPDL-abbreviation of institute$>$-$<$two last digits of
        the year of publication$>$-$<$preprint number$>$;
        For example: R = JINR-75-128;
        The format of R for conferences:
        R =$<$PPDL-abbreviation of conference$>$,$<$page$>$;
        For example: R = ROCHESTER-60, 1128

\item[RR]  the  reference  for  earlier  results,  preliminary  with
        respect  to  those  presented  in  the  record.  RR and R
        formats are identical exept for some deviations.
\end{description}
\subsection{THE REACTION DESCRIPTION}
\bigskip
\begin{description}
\item[RE]    is the reaction code, (see Appendix A).

\item[REC]   is  the  reaction  code  without  decay   brackets.   For
        reactions  without  resonance production REC is identical
        to RE.

\item[MULT]  is the final state multiplicity of the reaction presented
        in  REC.  It is calculated by the applied program for all
        classes of reactions except for MISC and $<$XX$>$PS. For  the
        classes  INCL,  INRP,  SEMI,  DSEM, and DSRP data element
        MULT contains the  multiplicity  of  the  detected  final
        state.

\item[FSP]   contains codes of particles in final state. The codes  of
        particles  produced  in resonance decays are not included
        into FSP. PPDL codes for particles see in [4]. FSP is the
        virtual  key,  i.e. it is missing in the record listings,
        but it is possible to retrieve the record with the  given
        value of FSP.
           Now we have additional possibility of  searching  with
        this  data element. This means a search of particles with
        desired properties. Format of this search is:
\begin{verbatim}
                      FSP < PROP(......);**
\end{verbatim}

        where points represent the property. Sign  '$<$'  indicates
        mode  of  operation for query procedures. Next properties
        are allowed:

\begin{center}
\begin{tabular}{ll}
               HADRON& -  no operation allowed  \\
               LEPTON& -  no operation allowed    \\
               MESON & -  no operation allowed      \\
               NUCLEUS&-  no operation allowed        \\
               HYPERON&-  no operation allowed          \\
               BARYON &-               \\
               CHARGED&-                 \\
               STRANGE&-                   \\
               CHARMED&-                     \\
               ISOMUL &-                       \\
               BEAUTY &-                         \\
               MASS   &-
\end{tabular}
\end{center}
\end{description}
        The meaning  of  every  property  is  transparent.  Every
        property can have operation (with exception for first five
        properties), for example -  CHARGED=-2,  means  particles
        with charge = -2. Allowed operations are :

\begin{center}
\begin{tabular}{ll}
                 =& -- equal to,   \\
                $>$&-- grater than, \\
                $<$&-- less than,    \\
                $<>$&-- not equal to.
\end{tabular}
\end{center}
\bigskip
        Note,         that the  presence  of  every  property  in
        query  string  means  that  this property is at least not
        equal to zero  (unless  zero  explicitly  required).
\newpage
  So, strings
\begin{verbatim}

            FI FSP<PROP(CHARGED<>0);**  and
            FI FSP<PROP(CHARGED);**
\end{verbatim}
are identical.
You can use more than one property in a single query. In
 this   case, separatee  between  them  with  commas.  All
 properties are considered to be connected by logical  AND
operation.

   Some examples of the PROP usage:

\begin{verbatim}
           FI FSP < PROP(HYPERON);**
\end{verbatim}
find all hyperons;

\begin{verbatim}
           FI FSP < PROP(CHARGED<0,HYPERON);**
\end{verbatim}
find all    negativelly charged hyperons;

\begin{verbatim}
           FI FSP < PROP(CHARGED=1,MASS>2.,MESON);**
\end{verbatim}
find all mesons with mass grater than 2. GeV and  charge
         = +1.
\bigskip


         Note, please ! In the  case  of  ANY  contradictions  in
         query THE LAST value is used ! So in the query


\begin{verbatim}
           FI FSP < PROP(CHARGED=1,CHARGED=0,MESON);**
\end{verbatim}
mesons WITH CHARGE=0 will be found.

\bigskip
\begin{description}
\item[BP]    is beam particle code, it is similar to FSP.

\item[TP]    is target particle code, it is similar to FSP.

\item[BRV]   product of all branchings  for  decays  of  final  states
        particles  specified  in  RE.  Used  to  calculate  cross
        sections from  Y  values  with  current  PDG  values  for
        branchings.

           The necessary branching fractions are  extracted  from
        the  data  base  (PP) containing particle properties . In
        records containing the flag SIG=SIG;  recalculations  not
        performed.

           The data-element Y contains the value of cross section
        times  the  branching ratios of all cascade decays in the
        final state as usual. The value of BRV then can  be  used
        straightforward  to  calculate the corrected value of the
        cross section. The user can obtain that  corrected  value
        in  some  modes  of the DOCUMENT (as it will be explained
        later).

           Sometimes it is  impossible  to  find  this  value  in
        publication:  authors  give the value of YN corrected for
        branching. In that case the value SIG  for  data  element
        SIG then appears (see description of data element SIG).

           If the experiment was carried in such a manner that  all
        decay  modes of a  resonance was detected, then the value of
        RE doesn't contain decay modes. In such a case RE and REC
        are  identical.  The information is presented in the same
        manner if the authors don't give the decay modes but  the
        value  of YN is already corrected for branching ratio. In
        this case the record contains flag SIG  (see  description
        of data element SIG).
\end{description}
\subsection{EXPERIMENTAL DATA DESCRIPTION}
\bigskip

\begin{description}
\item[YN]    describes the observable value and it`s units
        as it is given in publication. \\
        For example: YN = SIG/E IN FB/GEV;

\item[POINT] is a node data element used to couple the data related to
        one value of BTM.

\item[BTM]   describes the energy variables and their values for  beam
        particle.  BTM  contains information directrly taken from
        publication.

\item[PLAB]  is the beam momentum in laboratory system (GEV/C). It  is
        presented  in  the  form of a three-component vector: the
        mean value,  left  and  right  boundary  of  the  momenta
        spectrum.  Is calculated by system from BTM. For example:
        PLAB = 75.,74.,76.;

\item[ECM]   is the C.M.S. energy (GEV). The format  is  equivalen  to
        PLAB. Is calculated from BTM.


\item[Y]   is the value of YN for  reaction  given  in  RE  and  for
        corresponding  value  of  PLAB.  It is represented in the
        form of three-component vector: the value of YN, positive
        and negative errors. For example: Y=22.,1.,2.;

           The last two  components  of  Y  usually  specify  the
        statistical   error,   the   systematic  error  is  given
        separately (see 2.4). But if there is a combined error in
        publication,  consisting  of  statistic  fluctuations and
        systematics shifts, then this error is specified in Y and
        the corresponding string appears in the CSYS data element
        (see 2.4).

\end{description}
\subsection{DESCRIPTION OF EXPERIMENTAL CONDITIONS}
\bigskip

\begin{description}
\item[SYS]   is a  node  data  element.  It  is  designed  to  connect
        together the values and descriptions of the corresponding
        sources for the systematic errors belonging to all or  to
        the  majority  of  the  experimental  data  points in the
        record (see the description of YSYS data element).

\item[SYS-ERR]
        is the value of systematic error expressed  in  percents.
        It  is presented in the form of a two-component vector: a
        positive and a negative error.

\item[CSYS]  is a comment describing the source of the error specified
        in  SYS-ERR  or  YSYS.

        If Y contains errors that already  include  systematic
        deviations,  SYS-ERR is empty and CSYS = INCLUDED. If the
        systematic error is not given in the publication, then  CSYS  =
        NOT GIVEN.

           If authors do not give the source of systematic error,
        but give the value of this error CSYS = UNKNOWN.

           If authors  declare  that  the  errors  specified  are
        purely statistical, then CSYS = STATISTICAL ONLY.


\item[YSYS]  is a data element similar to SYS-ERR. It is presented  in
        the  case  when  for  some point/points in the record the
        systematic errors differs from that specified in SYS-ERR.
        Therewith  the  value  of systematic error common for the
        majority of  the  points  in  the  record  is  stored  in
        SYS-ERR.

\item[CORR]  is the description of the corrections applied by  authors
        during handling and analyzing data.

\item[COND]  is the description of experimental conditions (incomplete
        geometry  of  the  experimental  set-up,  cut-offs etc.)

        Including also the corrections not taken into account  by
        the  authors  but considered to be important according to
        the opinion of the encoder.
\end{description}

\subsection{COMMENTS TO THE DATA}
\bigskip

\begin{description}
\item[KIN]   describes the unusual for CS data base data:

\begin{tabular}{lll}

          KIN&= FORW;&- forward scattering,  \\
          KIN&= BACK;&- backward scattering,   \\
          KIN&= UL;  &- upper limit on cross section, \\
          KIN&= LL;  &- lower limit,      \\
          KIN&= PD;  &- preliminary data,   \\
          KIN&= OM;  &- order of magnitude,  \\
          KIN&= RDB; &- data were taken from data base  REACTIONS\\
             &       &\ without   second  checking  according  to
                        publication, \\
          KIN&= CALC;&- points  were  calculated  from  the  data\\
             &       &\ presented  in  the given publication with \\
             &       &\ the help of additional information.

\end{tabular}

\item[SIG]   is used to mark records in which the cross section  value
        has  been  corrected for branching. If the correction has
        been made by the authors of the publication SIG = SIG. If
        it  has  to be made by applied program SIG = CORR. If for
        some reasons the cross section value can not be corrected
        for  branching  this  element  is  not  included into the
        record.

\item[DS]    (Data Status) - expert flag of "reliable"  data.  It  has
        the form DS =$<$Expert code = latin initials$>$.
        All authors that have used these data  in  some  analysis
        can  be  considered  as  experts.

        The value DS = -$<$Expert code$>$means that expert gives the
        negative judgement on these data (see DSC).

\item[DSA]   first author's name. Is analogous to SC.

\item[DSR]   reference to the publication with expert  evaluation,  in
        the format of R.

\item[DSC]$<$Data Status Comment$>$- comment to the data. Contains the
        description  of  transformations  that  must  be  made to
        convert the data presented to be a part of basal  set  of
        data, and/or expert judgements on the data.

\item[CR]    is the comment for the record as a whole. It  appears  in
        the case when the above record structure does not make it
        possible to present the information from the  publication
        appropriately. It also contains the necessary explanation
        to the record.
\end{description}

\subsection{SERVICE DATA ELEMENTS}


\begin{description}
\item
[REC-ID]  is a record number assigned by the system when stored.
\item
[SB]    code of responsibility of the encoder. \\
        For example: SB = ENCODED 31 JUN 1980 BY SIA;
\item
[DEN]   is encoding date.

\item
[DCH]   is the lastest modification date.
\end{description}

\newpage
\section{DESCRIPTION OF OUTPUT}

    To output the information stored in the  compilation  a  user  can
 utilize  both  standard  facilities of the BDMS (commands PRINT,
 DUMP, LIST) and those supplied by the applied programs  (command
 DOCUMENT).  Using the command DOCUMENT one can get several types
 of output specified by  MODD,$<$number$>$ command,  which  must  be
 issued before DOCUMENT command.
 
\begin{verbatim}
   MODD,1
\end{verbatim}
   The  output  by  MODD,1  is  a  catalogue  of  the  reactions
 available in the current set.
    A part of the listing relating to a single reaction  has  two
 lines:  the  first  one  specifies the parameter as follows: the
 reaction code, the masses of  beam  and  target  particles,  the
 reaction threshold for the momentum in laboratory system and for
 the center of mass system energy (all in GeV), multiplicity  in
 the  final  state  and  the  data  element  YN.  The second line
specifies the number of points for a given reaction in the current set.

\begin{verbatim}
   MODD,2
\end{verbatim}
 In  addition  to  the  above  output  contains  also  the  lines
 describing  the  experimental  points.  These  lines  follow one
 describing the reaction. The  experimental  points  are  ordered
 according to the beam momentum.

    One line corresponds to one experimental point  and  contains
 the  following  information:  the mean value, the right and left
 boundaries of momenta spectrum,  the  same  parameters  for  the
 energy  in  c.m.s.  (all in GeV), the YN value, the negative and
 positive  errors  ,  warning  comment,  systematical  errors  in
 percents, SC of the record and reference to the publication.

    The warning comment is the symbol "W". It appears in the next
 cases:

\begin{itemize}
\item  there are data elements COND or KIN in the record \\
(exept for KIN = RDB; and KIN = CALC;);
\item  the data element SIG is absent or equal SIG.
\end{itemize}
    In the case when KIN =  RDB;  or  KIN  =  CALC;  the  warning
 comment equals "R" and "C" respectivelly.

\begin{verbatim}
   MODD,3
\end{verbatim}
gives the fullest  information  about  the  record.
 As a kinematical variables PLAB and ECM are used. The  value  of
YN  is  corrected for the current PDG branching ratio value from
BRV.
    If authors have corrected the value of YN  on  branching  and
 this fact is marked by encoder in the data element SIG, then the
 line

\begin{verbatim}
   CORRECTED BY AUTHORS
\end{verbatim}
 appears in the output for given record.
    If for some reasons the correction haven't been made then the
 line

\begin{verbatim}
   UNCORRECTED
\end{verbatim}
 appears in the record.
    The type of output according to MODD,4 is equivalent  to  the
 MODD,3 except the value of YN not corrected for current value of
 branching ratio. This mode is convenient in checking data.

    If SET that  have  been  formed  by  the  user  contains  the
 information  belonging  to  one  reaction  only,  it is strongly
 recommended to use outputs MODD,5 and MODD,6 instead  of  MODD,1
 and  MODD,2  respectively. They are complitely identical but are
 at least ten times faster.

\begin{verbatim}
   MODD,7
\end{verbatim}
    Technical format of  output  to  be  used  in  the  procedure
 CSBOOK.  Equivalent  to  MODD,6  with additional outputted lines
 that describes the decay brackets of decaying particles.
\begin{verbatim}
   MODD,8
\end{verbatim}
    Produces   the   output   in   the   format   of    CERN-HERA
 compilations.If   the  particle(s)  in  final  state  have  been
 observed in a particular decay mode(s) it will produce two lines
 for  a  single  entry  -  one  with  corrected cross section and
 another with uncorrected.
\begin{verbatim}
   MODD,9
\end{verbatim}
    Produces the output equivalent to one of REACTIONS data  base
 in the MODD,2 grouped according to reactions.
\begin{verbatim}
   MODD,10
\end{verbatim}
    Equivalent to MODD,9. Should be used if the set to be  output
 contains  the records with unique reaction. Is at least an order
 faster than MODD,9
\begin{verbatim}
   MODD,11
\end{verbatim}
    Equivalent to MODD,2. Supresses the output of PLAB value  and
 gives  the  overall systematic and individual errors as separate
 entries.
\begin{verbatim}
   MODD,12
\end{verbatim}
    Equivalent to MODD,11.Should be used when  the  selected  set
 contains the records with the same reaction.

\newpage
\section{DATA BASE USAGE}

    An access to the data base CS in the VMS media  is  given  by
 PPDS  macro-command  [3].  A powerfull internal help on the BDMS
 system ( commands: ?, ??, ?$<$command$>$)  and  on  the  data  base
 itself  (  commands:  HELPBASE,  FDT,  INDEX,  GUIDE  )  give  a
 possibility to become familiar with BDMS system and data base in
 a short time.

   Access to BDMS for interactive work with CS data base:

 \begin{verbatim}
   PPDS CS
 \end{verbatim}

 System invites user to type BDMS commands:

\begin{verbatim}
   CS:READ:EXEC>
\end{verbatim}
\bigskip
 Online help on BDMS commands of data retrieval and mani pulation

\begin{description}
\item
[?]          - list of BDMS commands;
\item
[??]         - list of BDMS commands with short description;
\item
[?$<$command$>$]- command description.
\end{description}

 Data base overview:

\begin{description}
\item
[HELPBASE]    - online handbook on data base, with  key  ac-
                 cess to the sections;
\item
[FDT]         - display the data base structure;
\item
[INDEX,$<$KDE$>$] - index of Key Data Elements;
\end{description}

 Retrieval and list of the data:

    The retrieval condition on the data, for  example,  on  cross
 section for the reaction\\
  $\pi^{-}p\to n\pi^{+}\pi^{-}$- must be as follows:

\begin{verbatim}
   FIND  RE = PI- P --> N PI+ PI-; AND YN = SIG;**
\end{verbatim}

    System then creates a current SET of the data. It  is  useful
 to   use   commands \\
   INDEX/SET,$<$params$>$ and  LIST,$<$params$>$ \\
to investigate the content of the set.

\begin{description}
\item
[LIST]        - list of the current set on the screen;
\end{description}

 Output of the data into external files:

\begin{description}
\item
[PRINT]       - output of the current SET into file  CS.PRN
                 in the format of LIST command;
\item
[DUMP]        - output of the current SET into fide  CS.DUM
                 in the format that is acceptable by BDMS;
\item
[DOCUMENT]    - output of the current SET into file  CS.DOC
                 in the format set previously by the command
                 MODD.
\end{description}
   The main mode of output is MODD,2. With the command :
\begin{verbatim}
   MODD,2 DOCU
\end{verbatim}
 all data from the current SET will be presented  as  compilation
 ordered by reactions and momentum (see 3.).

    There is a possibility to produce a  well-to-see  compilation
 of  tables  and  graphs  with  the  help  of CSBOOK system ( all
 consultations available from data base manager ).

    To  finish work with BDMS type STOP.

\newpage
\section{CONCLUSION}

    Describing compilation contains now about 21000 records, that
 corresponds  to  about  47000  experimental  points. All records
 except for very little part were checked by COMPAS group (IHEP).
 All these record have data element SB.

    In the future we plan to maintain the data base with the help
 of  PPDS's  data  bases  DOCUMENTS and REACTIONS with additional
 checking of the records  against  publications  whenever  it  is
 possible.

    We plan also future development of  the  data  base  in  some
 directions:   rejection   of   errors   in   data  presentation,
 construction of "the basal data sets" with the  help  of  expert
 evaluations  published elsewhere and stored in the data elements
 DS, DSA, DSR, DSC, new possibilities in data manipulation.

    All criticism on quality of data base,  proposals  on  future
 development will be accepted with pleasure and used in our work.
\par\bigskip
{\noindent\bf APPENDIX A.}
\par\bigskip

PPDL rules of writing the data element RE.\\
   Reaction Classification.

    In the PPDS data bases we use identical for all bases  syntax
 of reactions and decays:
\begin{verbatim}
   RE = B T --> <final state code>;
\end{verbatim}
 where B is the beam code, T is the  target  code  in  accordance
 with  the  PPDL  [4]. The type of the final state depends on the
 measurement technique and particle types  in  the  final  state.
 Below we shall consider various types of the final state code.
\par\bigskip
{\noindent\bf EXCLUSIVE REACTIONS.}
\par\bigskip

    If  only  stable  particles  (stable  particles  are  defined
 according  RPP  [6])  are  present in the final state and all of
 them are identified, the reaction with this final  state  relate
 to  the EXCL class (EXCLusive). If it isn't necessary to specify
 the particle decay mode in the final state, the final state code
 for such reactions is:
\begin{verbatim}
   H1  H2  ...  Hk,
\end{verbatim}
 here Hi is the code of a stable particle. If  a  particle  decay
 mode  has to be specified the final state code has the following
 form:
\begin{verbatim}
   H1 ...  R1 < Hi ... Hj >... Hk,
\end{verbatim}
 here R1 is the decaying particle code. According to this record,
 the  particles  having the codes Hi ... Hj are decay products of
 the particle with the code R1. In the  case  of  cascade  decays
 this  rule  is applied iteratively to all particles of the decay
 tree. For example:
\begin{verbatim}
   RE = PI- P --> A2(1310)0 < RHO+ < PI+ PI0 > PI- > N
\end{verbatim}
    Under  the  condition  of  full  identification  of  all  the
 particles  in  the  final state, the reaction with resonances in
 final state  relate  to  the  EXRP  class  (EXclusive  Resonance
 Production).  If  the  experimental  technique  does not make it
 possible to separate a few final states, the final state code is
 made  of  the codes of nonseparated final states combined by the
 sign "+" . For example:

\begin{verbatim}
   RE = PI- P --> K0 LAMBDA PI0 + K0  SIGMA0 PI0
\end{verbatim}
    To make the record shorter, the repeated group of  codes  may
 be put out of the brackets:
\begin{verbatim}
   RE = PI- P --> K0 PI0 ( LAMBDA + SIGMA0 )
\end{verbatim}
    Under the condition  of  total  identification  of  remaining
 particles,  the  reaction with similar ambiguities relate to the
 EXPS class (EXclusive Partial Sum).

\par\bigskip
{\noindent  \bf  A.2 INCLUSIVE REACTIONS.}
\par\bigskip

    If some separated final state is detected in  experiment  and
 its  guidance  is not detected, the observed reaction relates to
 inclusive one and summation over all undetectable  particles  is
 described  by a special code X (=ANYTHING). The final state code
 is of of the form:

\begin{verbatim}
   <code of the detected final state> X
\end{verbatim}
 Code of detected final state is formed by the same rule as  that
 of  the  final state for exclusive reactions. The classes of the
 inclusive reactions  are  similar  to  those  of  the  exclusive
 reactions:

\begin{VL}{INCL}
\item
[INCL]- similar to EXCL (INCLusive), \\
\item
[INRP]- similar to EXRP (INclusive Resonance Production) \\
\item
[INPS]- similar to EXPS (INclusive Partial Sum).
\end{VL}

\bigskip
    It should be noted, that the code X is treated by the  system
 as a special particle. Therefore when writing a reaction of INPS
 class the code of the detectable final  state  code  has  to  be
 enclosed  into  brackets.  If the code of detectable final state
 has a group of repeated codes out of the brackets the whole code
 may not necessarily be put into brackets. For example:

\begin{verbatim}
   RE = PI- P -->PI0 ( LAMBDA + SIGMA0 ) X;
\end{verbatim}
\newpage
\par\bigskip
{\noindent\bf A.3 SEMIINCLUSIVE REACTIONS.}
\par\bigskip

    If the undetected final state consists of neutral  particles,
 summation  over  all possible neutral states is described by the
 code (NEUTRALS) - zero or more  neutrals.  This  code  are  used
 similarly  to  that of X for inclusive reactions. The classes of
 semiinclusive reactions are as follows:

\begin{VL}{SEPS}
\item
[SEMI]- similar to INCL (SEMIinclusive) \\
\item
[SERP]- similar to INRP (SEmiinclusive Resonance Production)\\
\item
[SEPS]- similar to INPS (SEmiinclusive Partial Sum)
\end{VL}
\par\bigskip
{\noindent\bf A.4 DOUBLE SEMIINCLUSIVE REACTIONS.}
\par\bigskip

    In the case of summation being performed  over  neutrals  and
 the fixed number of tracks the code NCHARGED (NEUTRALS) is used.
 Here N is the number of tracks. This code is used  similarly  to
 that of X. The classes of the double semiinclusive reactions are
 as follows:

\begin{VL}{DSPS}
\item
[DSEM]- similar to INCL (Double SEMiinclusive)\\
\item
[DSRP]- similar to INRP (Double Semiincl. Resonance Production)      \\
\item
[DSPS]- similar to INPS (Double Semiinclusive Partial Sum).
\end{VL}
\par\bigskip
{\noindent\bf A.5 OTHER REACTIONS.}
\par\bigskip

    If the contribution from the "subtracted" final state  wasn't
 taken  into account during registration or processing, the final
 state code can  be  composed  of  two  codes  separated  by  the
 subtraction sign '-'. For example:

\begin{verbatim}
   RE = PI- P --> ( PI+ PI- - RHO0 < PI+ PI- > ) X
\end{verbatim}
 for the reaction of the type

\begin{verbatim}
   RE = PI- P --> X - PI- P
\end{verbatim}
 the following record is used:

\begin{verbatim}
   RE = PI- P --> INELASTIC
\end{verbatim}
    These as well as other reactions not included into the  above
 classification relate to the MISC class (MISClassified).

    Let us note  that  a  multiplier  of  the  particle  code  is
 allowed. For example:

\begin{verbatim}
   RE = PI- P --> PI- P 3PI0
\end{verbatim}
    The particle codes in the  record  of  the  final  state  are
 ordered  by the applied program. Therefore when making a request
 for retrieving the data on some reaction a  user  may  not  care
 about it.

    The above reaction classes are stored  in  the  data  element
 mode. The authors believe that the classification presented will
 help users in searching for required group of reactions.

\newpage
\begin{thebibliography}{99}
\bibitem{hera}
      Flaminio V., et al. -CERN-HERA-83-01.1983\\
      Flaminio V., et al. -CERN-HERA-83-02.1983\\
      Flaminio V., et al. -CERN-HERA-84-01.1984  \\
      Alekhin S.I., et al. -CERN-HERA-87-01.1987

Landolt -- B\"ornstein
\bibitem{ppds}
Rosenfeld A.H. //Ann.Rev.Nucl.Sci.1975.V.25.P.555 \\
Ezhela V.V. //Comp.Phys.Comm.1984.V.33.P.225\\
Alekhin S.I., et al. -IHEP-87-125.Serpukhov.1987
\bibitem{bdms}
Richards D.R. -- LBL-4683 revised.1978 \\
Alekhin S.I., et al. -IHEP-81-120.Serpukhov.1981
\bibitem{rdb}
      Alekhin S.I., et al. -IHEP-87-rdb.Serpukhov.1987
\bibitem{pdg}
Particle Data Group.   Phys.Rev.1992.V.D45.num.11.part.2
\bibitem{gr}
      Grudtsin S.N., et al. -IHEP-81-81.Serpukhov.1981

\end{thebibliography}

\newpage
 TABLE.  A  full  list  of  data   elements   with   their   main
 characteristics. Types of data elements:

\medskip
\par\noindent
 CHAR ~- character value, INTE -integer vector, REAL -real vector,

\medskip
\par\noindent
 NODE ~- pure node, CHARV - virtual key of the type CHAR.

 The length of the key of the type CHAR is given in the words and
 in the number of components to the vectors.

\medskip
 Multiplicity of data elements:
  S - single,    M - multiple,    O - multiple

 the order of  appearence is important.

 Format types:   C - controlled,  F - free.
\begin{center}

\medskip
{\scriptsize
\begin{tabular}{|r|r|r|r|c|c|}\hline
&&&&&\\
\multicolumn{1}{|c|}
{DATA}&
\multicolumn{1}{|c|}{PARENT'S}&
\multicolumn{1}{|c|}{TYPE}&
\multicolumn{1}{|c|}{KEY}&
\multicolumn{1}{|c|}{MULT.}&{FORMAT}\\
\multicolumn{1}{|c|}{ELEMENT}&
\multicolumn{1}{|c|}{NAME}&&
\multicolumn{1}{|c|}{LENGTH}&&\\
\multicolumn{1}{|c|}{NAME}&&&&&\\
&&&&&\\
 \hline
           &           &        &      &        &      \\
    SC     &       --  &  CHAR  &   4  &    S   &    C \\
    R      &       --  &  CHAR  &   5  &    M   &    C  \\
    TY     &       --  &  CHAR  &   1  &    S   &    C   \\
    D      &       --  &  INTE  &   1  &    S   &    F     \\
    RR     &       --  &  CHAR  &   5  &    M   &    F       \\
    RE     &       --  &  CHAR  &   40 &    S   &    C \\
    REC    &       --  &  CHAR  &   40 &    S   &    C   \\
    FSP    &       --  &  CHARV &   4  &    M   &    C     \\
    B       &      --  &  CHARV &   4  &    S   &    C       \\
    T       &      --  &  CHARV &   4  &    S   &    C         \\
    MULT    &      --  &  INTE  &   1  &    S   &    C           \\
    MODE    &      --  &  CHAR  &   1  &    S   &    C             \\
    BRV     &      --  &  REAL  &   -  &    S   &    F              \\
    YN      &      --  &  CHAR  &   2  &    S   &    C     \\
    POINT   &      --  &  NODE  &   -  &    M   &    -       \\
    BTM     &  POINT   &  CHAR  &   0  &    O   &    C    \\
    PLAB    &  POINT   &  REAL  &   1  &    O   &    C      \\
    ECM     &  POINT   &  REAL  &   1  &    O   &    C        \\
    Y       &  POINT  &   REAL  &   -  &    O   &    F          \\
    YSYS    &  Y       &  REAL  &   -  &    O   &    F            \\
    SYS     &      --  &  NODE  &   -  &    M   &    -              \\
    SYS-ERR &  SYS     &  REAL  &   -  &    O   &    F                \\
    CSYS    &  SYS     &  CHAR  &   -  &    O   &    F                  \\
    KIN     &      --  &  CHAR  &   1  &    M   &    C                    \\
    CORR    &      --  &  CHAR  &   -  &    M   &    F                     \\
    COND    &      --  &  CHAR  &   0  &    M   &    F \\
    CR      &      --  &  CHAR  &   -  &    M   &    F   \\
    SIG     &      --  &  CHAR  &   1  &    S   &    C     \\
    DS      &      --  &  CHAR  &   1  &    M   &    F       \\
    DSA     &  DS      &  CHAR  &   4  &    S   &    F         \\
    DSR     &  DS      &  CHAR  &   5  &    S   &    F         \\
    DSC     &  DS      &  CHAR  &   -  &    M   &    F             \\
    DEN     &      --  &  CHAR  &   -  &    S   &    C               \\
    DCH     &      --  &  CHAR  &   5  &    S   &    C     \\
    SB      &      --  &  CHAR  &   1  &    M   &    C       \\
            &          &        &      &        &         \\
\hline
\end{tabular}
}
\end{center}
\begin{flushright}
                 %\Revised November, 1992
\vskip 5mm
\today
\end{flushright}
\end{document}
