
struct record_key keys[] = {
{"SC"              ,_SC_                   ,_RECORD_    , 0, 0,0,0,0,0}, //....
{"IRN"             ,_IRN_                  ,_RECORD_    , 0, 0,0,0,0,0}, //....
{"R"               ,_R_                    ,_RECORD_    , 0, 0,0,0,0,0},
{"COD"             ,_COD_                  ,_R_         , 0, 0,0,0,0,0},
{"TY"              ,_TY_                   ,_R_         , 0, 0,0,0,0,0},
{"D"               ,_D_                    ,_R_         , 0, 0,0,0,0,0},
....
{"AUTHORS"         ,_AUTHORS_              ,_RECORD_    , 1, 0,0,0,0,0},
{"A"               ,_A_                    ,_AUTHORS_   , 0, 0,0,0,0,0},
{"I"               ,_I_                    ,_AUTHORS_   , 0, 0,0,0,0,0}, //....
{"T"               ,_T_                    ,_RECORD_    , 0, 0,0,0,0,0},
{"TITLE-TEX"       ,_TITLE_TEX_            ,_T_         , 0, 0,0,0,0,0}, //....
{"ABS"             ,_ABS_                  ,_RECORD_    , 0, 0,0,0,0,0},
{"EXPERIMENT"      ,_EXPERIMENT_           ,_RECORD_    , 1, 0,0,0,0,0},
{"AC"              ,_AC_                   ,_EXPERIMENT_, 0, 0,0,0,0,0},
{"DE"              ,_DE_                   ,_EXPERIMENT_, 0, 0,0,0,0,0},
{"PR"              ,_PR_                   ,_EXPERIMENT_, 0, 0,0,0,0,0},
{"COL"             ,_COL_                  ,_EXPERIMENT_, 0, 0,0,0,0,0},
{"RR"              ,_RR_                   ,_EXPERIMENT_, 0, 0,0,0,0,0}, //....
{"REAC-DATA"       ,_REAC_DATA_            ,_EXPERIMENT_, 1, 0,0,0,0,0},
{"RE"              ,_RE_                   ,_REAC_DATA_ , 0, 0,0,0,0,0}, //....
{"B"               ,_B_                    ,_REAC_DATA_ , 0, 0,0,0,0,0}, //....
{"DD"              ,_DD_                   ,_REAC_DATA_ , 0, 0,0,0,0,0}, //....
{"PART-PROP"       ,_PART_PROP_            ,_EXPERIMENT_, 1, 0,0,0,0,0},
{"P"               ,_P_                    ,_PART_PROP_ , 0, vy_init_P,s2t_P,t2s_P,t2h_P,eq_P },
{"PP"              ,_PP_                   ,_PART_PROP_ , 0, 0,0,0,0,0},
....
};

