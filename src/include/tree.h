#ifndef _tree_h_
#define _tree_h_
/****************************************************************************/

void
free_list(struct list *l);

struct listlist *
treelist2listlist (struct listlist *prev, struct node_list *no, int n);

struct node_list *
tree2treelist(struct node *);

int
eq_listlist(struct listlist *l1, struct listlist *l2);

struct listlist *
contract_listlist(struct listlist *l);

void
free_listlist(struct listlist *l);

void
print_listlist (struct listlist *l);


void
print_list(struct list *l);

static
char *
_list2s (struct list *l, char *buf);

static
char *
_listlist2s (struct listlist *l, char *buf);

#endif
/*------------------------------------------------------*/
