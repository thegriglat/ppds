#ifndef _sqlite_h_

#include "sqlite3.h"

#define SQL_MAX_LEN 1024

enum SQLITE_TYPE {
    INTEGER,
    REAL,
    TEXT,
    BLOB
};

void db_open();
void db_close();
sqlite3* get_db();
int db_create_table(const char* table, const char* description);
int db_get_last_id(const char* table, const int column);
int db_exec_noresult(const char* sql, const int nparams, const void** params, const enum SQLITE_TYPE* types);

void sqlite_stmt_free(sqlite3_stmt* stmt);

char ** unique_char_list(char ** input, const int n);
int charlist_len(char** input);

#define _sqlite_h_
#endif