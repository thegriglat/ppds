#ifndef _tree2base_h_
#include "dg/tree.h"
int
tree2base (struct node *no);

struct node *
base2tree (const int r);
std::string getValFromHash(const std::string type, const std::string hash);
#define _tree2base_h
#endif
