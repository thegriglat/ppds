#ifndef _database_h_

#define IDENT 0
#define IDENT_SHIFT 2

int isDictNode(char* type);
char* getDictValueById(char* table, int id);
int getParentId(int rec_id);
int* getDataId(int value_id);

#define _database_h_
#endif