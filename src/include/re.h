#ifndef _re_h_

struct list {
  int id, n;
  struct listlist *dec;
  struct list *prev, *next;
};

struct listlist {
  struct list *l;
  int n;
  struct listlist *prev, *next;
};

struct type_RE {
  struct listlist *is;
  struct listlist *fs;
};

enum op_type {
  _TERM_,
  _PLUS_,
  _MINUS_,
  _MUL_,
  _PRJ_
};

struct node {
  enum op_type op;
  int id, n;
  struct node *dec;
  struct node *left, *right;
};


struct node_list {
  enum op_type op;
  struct list *l;
  struct node_list *left, *right;
};



void
print_listlist (struct listlist *l);
void
_qn_listlist (int print,
	      struct listlist *l,
	      int *bar, int *lep, int *S, int *C, int *B, int *T,
	      double *m, double *I3, double *Q
	     );
void 
print_list(struct list *l);

void 
_qn_list(int print,
	 struct list *l,
	 int *bar, int *lep, int *S, int *C, int *B, int *T,
	 double *m, double *I3, double *Q
	);
void 
print_prt(int id);

int cross_listlist(struct listlist *l1, struct listlist* l2);
int cross_list(int n1, struct list* list1, int n2, struct list *list2);

struct list* copy_list(struct list *l);

struct listlist* copy_listlist(struct listlist* l);

int eq_listlist(struct listlist *l1,struct listlist *l2);
int eq_list(struct list *l1,struct list *l2);

struct list *cat(struct list *l1,int n1,struct list *l2,int n2);
struct list *contract_list(struct list *l, int (*eq_prt)(int,int));
struct list *last(struct list *l);
struct list *sort_list(struct list *l, int (*eq_prt)(int,int) );
struct list *tree2list(struct list *prev,struct node *no);
void mul(struct list *l,int n);
void free_list(struct list *l);

struct listlist *cat_listlist(struct listlist *l1,struct listlist *l2);
struct listlist *contract_listlist(struct listlist *l, int (*eq_prt)(int,int) );
struct listlist *last_listlist(struct listlist *l);
struct listlist *treelist2listlist(struct listlist *prev,struct node_list *no,int n);
void free_listlist(struct listlist *l);


struct node *copy_tree(struct node *no);
struct node_list *tree2treelist(struct node *);
struct node_list *tree2treelist(struct node *no);
struct node *new_node(enum op_type op,int id,int n,struct node *dec,struct node *left,struct node *right);
struct node *norm_tree(struct node *no);

void free_node_list(struct node_list *l);
void print_treelist(struct node_list *no);
void print_tree(struct node *no);
void free_tree (struct node *no);

void free_re_intermediate(void);

#define _re_h_
#endif
