#ifndef _dg_B_h


enum kinvars {
ECM,
PLAB_N,
PLAB,
ELAB_N,
ELAB,
TLAB_N,
TLAB,
NU,
Q2,
S,
W,
W2
};

enum units {
TEV,
GEV,
GEV2,
MEV,
MEV2,
EV,
EV2,
F_2
};


struct type_B {
	int n_range;
	float *xmin, *xmax;
	enum units unit;
	enum kinvars kin;
};


void*
s2t_B(char*);

char*
t2s_B(void *);

char*
t2h_B(void *);

int 
eq_B(void*, void*);

int 
cross_B(void*,void*);

void
free_B(void*);

long long encodeBeam(struct type_B* b);

void printBits(long long v);

unsigned char getMaskBit(float v);

void search_B(struct type_B *);

void dump_B();

#define _dg_B_h

#endif
