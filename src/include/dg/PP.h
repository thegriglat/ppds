#ifndef _dg_PP_h

struct type_PP {
  int nval;
  char **val;
};

void
vy_init_PP (void);

char*
t2s_PP(void *);

char*
t2h_PP(void *);

int
eq_PP (void *,void *);

void *
s2t_PP (char *);

#define _dg_PP_h

#endif
