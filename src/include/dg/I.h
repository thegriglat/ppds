#ifndef _dg_I_h

struct type_I {
  int nval;
  int nas;
  char **val; //abbreviation
  char **as;
  char *url;
};

void
vy_init_I (void);

char*
t2s_I(void *);

char*
t2h_I(void *);

int
eq_I (void *,void *);

void *
s2t_I (char *);

#define _dg_I_h

#endif
