#ifndef _dg_AC_h

struct type_AC {
  int nval;
  char **val;
};

void
vy_init_AC (void);

char*
t2s_AC(void *);

char*
t2h_AC(void *);

int
eq_AC (void *,void *);

void *
s2t_AC (char *);

#define _dg_AC_h

#endif
