#ifndef _dg_P_h

struct type_P {
  int id;
  int  bar,lep, S, C, B, T, anti, heavy_nuc, classname;
  double m, I3, Q;
  char orcode[32];
  int nval;
  char **val;
  long val0sum;
};

void
vy_init_P (void);



char*
t2s_P(void *);

char*
t2h_P(void *);

int
eq_P (void *,void *);

void *
s2t_P (char *);

char*
id2s_P(int id);

struct type_P*
id2t_P(int id);

#define _dg_P_h

#endif
