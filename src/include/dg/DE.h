#ifndef _dg_DE_h

struct type_DE {
  int nval;
  char **val;
  char *ident;
};

void
vy_init_DE (void);

char*
t2s_DE(void *);

char*
t2h_DE(void *);

int
eq_DE (void *,void *);

void *
s2t_DE (char *);

#define _dg_DE_h

#endif
