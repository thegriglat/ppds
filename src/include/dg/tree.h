#ifndef _dg_tree_h

enum keytype {
  _UNKNOWN_,
  _NOKEY_,
  _RECORD_,
  _SC_,
  _PDGSC_,
  _IRN_,
  _IRNW_,
  _R_,
  _COD_,
  _TY_,
  _D_,
  _KTO_,
  _KRD_,
  _CONF_,
  _AUTHORS_,
  _A_,
  _SPOKESMAN_,
  _SPP_,
  _I_,
  _ILBL_,
  _NA_,
  _T_,
  _TW_,
  _TITLE_TEX_,
  _TA_,
  _TA_TEX_,
  _ABS_,
  _ABS_TEX_,
  _DK_,
  _L_,
  _CIT_,
  _CCOD_,
  _CB_,
  _CB_TEX_,
  _URL_,
  _PPF_,
  _OR_,
  _SB_,
  _AV_,
  _COMP_,
  _ES_,
  _EXPERIMENT_,
  _AC_,
  _DE_,
  _PR_,
  _COL_,
  _RR_,
  _RCOD_,
  _PUBLISHED_PAPERS_,
  _PL_,
  _CE_,
  _CE_TEX_,
  _CD_,
  _CD_TEX_,
  _REAC_DATA_,
  _RE_,
  _FSP_,
  _BP_,
  _TP_,
  _B_,
  _PLAB_,
  _ECM_,
  _Q2_,
  _W2_,
  _NU_,
  _DD_,
  _INR_,
  _PART_PROP_,
  _P_,
  _PP_,
  _INP_,
  _CR_,
  _CR_TEX_,
  _CRW_,
  _INDEX_,
  _RD_,
  _CS_,
  _RPP_,
  _DEN_,
  _DCH_,
  _ACTION_,
  _QUESTION_,
  _ANSWER_,
  _ERROR_
};

struct record_key {
  char *s;
  enum keytype key, par;
  int  noval;
  void  (*vy_init)(void);
  void *(*s2t)(char *);
  char* (*t2s)(void *);
  char* (*t2h)(void *);
  int   (*eq )(void *,void *);
  void  (*t_free)(void*);
};

struct node {
  enum keytype type;
  char **val;
  int   nval;
  struct node **ch, *par;
  int   nch;
};

char *
k2s(enum keytype k);

int
k2noval(enum keytype k);

enum keytype
s2k(char *s);

enum keytype
k2par(enum keytype k);

int
k2ik (enum keytype k);

int
s2ik(char *s);

#define _dg_tree_h
#endif
