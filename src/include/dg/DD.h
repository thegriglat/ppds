#ifndef _dg_DD_h

struct type_DD {
  int nval;
  char **val;
};

void
vy_init_DD (void);

char*
t2s_DD(void *);

char*
t2h_DD(void *);

int
eq_DD (void *,void *);

void *
s2t_DD (char *);

#define _dg_DD_h

#endif
