#ifndef _dg_RE_h

void
vy_init_RE (void);

void *
s2t_RE (char *);

char*
t2h_RE(void *);

char*
t2s_RE(void * );

int
eq_RE (void *,void * );

void
free_RE (void*);

#define _dg_RE_h
#endif
