#ifndef _settings_h

#include "stdio.h"


#ifdef _DEBUG_
 #define DEBUG(...){printf("\nDEBUG : %s, %d : ",__FUNCTION__, __LINE__);printf(__VA_ARGS__);printf("\n");}
 #define _DBG_ 1
#else
 #define DEBUG(...)
 #define _DBG_ 0
#endif

#define likely(x) __builtin_expect((x),1)
#define unlikely(x) __builtin_expect((x),0)

#define WARNING(...){fprintf(stderr, "\nWARNING: ");fprintf(stderr, __VA_ARGS__);fprintf(stderr, "\n");}
#define ERROR(...){fprintf(stderr, "\nERROR: ");fprintf(stderr, __VA_ARGS__);fprintf(stderr, "\n");}

#define _settings_h
#endif