#ifndef _vy_P_h
int
exp_orcode ( char *s,
             int *bar,
             int *lep,
             int *S,
             int *C,
             int *B,
             int *P,
             int *heavy_nuc,
             int *classname,
             double *I,
             double *Q,
             double *m
           );


void
dump_tree_P (struct node *no, int id);

#define _vy_P_h
#endif
