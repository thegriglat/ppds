#ifndef _vy_tree_h
enum keytype {
  _UNKNOWN_,
  _NOKEY_,
  _RECORD_,
  _ABBREVIATION_,
  _AS_,
  _AT_,
  _NAME_,
  _USAGE_,
  _COMMENT_,
  _COMMENT_TEX_,
  _DESCRIPTION_,
  _ORCODE_,
  _QN_Q_,
  _QN_BAR_,
  _IDENT_,
  _Q_,
  _MASS_,
  _RC_,
  _RPP_,
  _CRPP_,
  _PN_,
  _SB_,
  _DEN_,
  _DCH_,
  _QUESTION_,
  _URL_
};

struct node {
  enum keytype type;
  char **val;
  int   nval;
  struct node **ch, *par;
  int   nch;
};

#define _vy_tree_h
#endif
