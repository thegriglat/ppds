#!/bin/bash

tmpdir=`mktemp -d`

test -z "$AR" && export AR="ar"

function rename(){
    local filename="$(basename $1)"
    local ext="${filename##*.}"
    echo $(echo $filename | cut -d- -f 1)-$(echo $1 | base64).$ext
}

function extractlib(){
    local lib="$1"
    local libname="$(basename $lib)"
    local d="$tmpdir/$lib"
    mkdir -p $d
    cd $d
    ar -x $OLDPWD/$lib
    find . -type f -name '*.o'| while read i; do
        mv -f "$i" "$(rename $i)"
    done
    cd - 1>/dev/null
}

outputlib="$1"
shift;

for l in $@; do
    if echo $l | grep -qE "\.o"; then
        # object file
        mkdir -p $tmpdir/$(dirname $l)
        cp -f $l $tmpdir/$(dirname $l)/$(rename $l)
    else
        # static library
        extractlib $l
    fi
done
"$AR" crs "$outputlib" $(find $tmpdir -type f -name '*.o')
rm -rf $tmpdir

