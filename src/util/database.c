#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "sqlite.h"

#include "settings.h"

#include "database.h"

int isDictNode(char* type){
    if (strcmp(type, "AC") == 0 ||
        strcmp(type, "DD") == 0 ||
        strcmp(type, "DE") == 0 ||
        strcmp(type, "I") == 0 ||
        strcmp(type, "P") == 0 ||
        strcmp(type, "PP") == 0
        )
        return 1;
    return 0;
}


char* getDictValueById(char* table, int id){
    sqlite3_stmt* stmt;
    char* sql = (char*)malloc(sizeof(char) * (51 + strlen(table) + 1));
    sprintf(sql, "select abbreviation from vy_%s where id = ? limit 1;", table);
    if (sqlite3_prepare(get_db(), sql, -1, &stmt, 0) != SQLITE_OK){
        ERROR("Cannot prepare SQL: %s", sql);
        exit(1);
    };
    sqlite3_bind_int(stmt, 1, id);
    int step = sqlite3_step(stmt);
    if (step != SQLITE_ROW){
        //
        ERROR("Id %d not found in table vy_%s", id, table);
        exit(1);
    }
    const char* val = (char*)sqlite3_column_text(stmt, 0);
    char* retval = (char*)malloc(sizeof(char) * (strlen(val) + 1));
    strcpy(retval, val);
    sqlite_stmt_free(stmt);
    free(sql);
    return retval;
}

int getParentId(int rec_id)
{
    // return parent id or -2 in case of error
    const char* sql = "select rec_id from dg_data where id = ?;";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(get_db(), sql, -1, &stmt, 0) != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", sql);
        return -2;
    };
    sqlite3_bind_int(stmt, 1, rec_id);
    const int step = sqlite3_step(stmt);
    if (step == SQLITE_ROW || step == SQLITE_DONE) {
        const int pid = sqlite3_column_int(stmt, 0);
        sqlite_stmt_free(stmt);
        return pid;
    }
    printf("Something wrong.\n");
    return -2;
};

int* getDataId(int value_id)
{
    int* result = NULL;
    int n_result = 0;
    const char* sql = "select id from dg_data where vid = ?;";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(get_db(), sql, -1, &stmt, 0) != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", sql);
        return (int*)NULL;
    };
    sqlite3_bind_int(stmt, 1, value_id);
    int step = sqlite3_step(stmt);
    while (step == SQLITE_ROW) {
        const int id = sqlite3_column_int(stmt, 0);
        int* tmp = (int*)realloc(result, sizeof(int) * (++n_result));
        if (tmp)
            result = tmp;
        else {
            ERROR("Cannot allocate memory!");
            exit(1);
        }
        result[n_result - 1] = id;
        step = sqlite3_step(stmt);
    }
    sqlite_stmt_free(stmt);
    return result;
};
