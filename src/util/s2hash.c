#include <crypt.h>
#include <stdlib.h>
#include <stdio.h>
#include "settings.h"

char *
s2hex (const char* s) {
	int j, a;
	char *buf = 0;
	for (j = 0; *s; s++) {
		if (*s < 0x10) {
			char *tmp = (char*)realloc(buf,sizeof(char)*(j + 1));
			if (tmp)
				buf = tmp;
			else {
				ERROR("Cannot allocate memory.");
				exit(1);
			}
			buf[j++] = (*s) < 0x0a ? '0' + (*s) : 'a' + (*s) - 0x0a;
		}
		else {
			char* tmp = (char*)realloc(buf,sizeof(char)*(j + 2));
			if (tmp)
				buf = tmp;
			else {
				ERROR("Cannot allocate memory");
				exit(1);
			}
			a = (*s) / 0x10;
			buf[j++] = a < 0x0a ? '0' + a : 'a' + a - 0x0a;
			a = (*s) - a * 0x10;
			buf[j++] = a < 0x0a ? '0' + a : 'a' + a - 0x0a;
		}
	}
	buf = (char*)realloc(buf,sizeof(char)*(j + 1));
	buf[j] = '\0';
	return buf;
}

char*
s2hash(const char* s) {
	return s2hex(crypt(s,"$1$") + 4);
}

long
s2sum(const char *s) {
	long sum = 0;
	if (!s)
		return 0;
	while (*s)
		sum = 256*sum + (*s++);
	return sum;
}

