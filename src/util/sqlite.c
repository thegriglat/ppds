#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "settings.h"


#include "sqlite.h"

static sqlite3 *db;

#define DEFAULT_DB_NAME "main.db"


void db_open()
{
    if (db)
        return;
    if (sqlite3_open(DEFAULT_DB_NAME, &db) == SQLITE_OK) {
        return;
    } else {
        ERROR("Cannot open database: %s", DEFAULT_DB_NAME);
        exit(1);
    }
};

void db_close()
{
    if (db)
        sqlite3_close(db);
}

sqlite3* get_db()
{
    if (!db)
        db_open();
    return db;
}

void sqlite_stmt_free(sqlite3_stmt* stmt){
    sqlite3_clear_bindings(stmt);
    sqlite3_reset(stmt);
    sqlite3_finalize(stmt);
}

int db_create_table(const char* table, const char* description)
{
    sqlite3 *_db = get_db();
    char sql[SQL_MAX_LEN];
    char sql_table_check[SQL_MAX_LEN];
    // check if table exists
    sprintf(sql_table_check, "SELECT count(*) FROM sqlite_master WHERE type = 'table' AND name = '%s'", table);
    sqlite3_stmt *res;
    sqlite3_prepare_v2(_db, sql_table_check, -1, &res, 0);
    int step = sqlite3_step(res);
    if (step == SQLITE_ROW) {
        if (sqlite3_column_int(res, 0) == 1) {
            // table exists
            sqlite_stmt_free(res);
            return 1;
        }
    }
    // end check
    sprintf(sql, "CREATE TABLE %s (%s);", table, description);
    char *err_msg = 0;
    DEBUG("sql: %s", sql);
    if (sqlite3_exec(_db, sql, 0, 0, &err_msg) != SQLITE_OK) {
        ERROR("Cannot create table %s  %s", table, err_msg);
        sqlite3_free(err_msg);
        return 0;
    };
    return 1;
}

int db_get_last_id(const char* table, const int column)
{
    char sql[SQL_MAX_LEN];
    sprintf(sql, "select max(id) from %s;", table);
    sqlite3_stmt *res;
    int rc = sqlite3_prepare(get_db(), sql, -1, &res, 0);
    if (rc != SQLITE_OK) {
        ERROR("Error running SQL: %s", sql);
        exit(1);
    }
    int step = sqlite3_step(res);
    if (step == SQLITE_ROW){
        const int r = sqlite3_column_int(res, column);
        sqlite_stmt_free(res);
        return r;
    }
    ERROR("Error fetching id from table %s", table);
    exit(1);
}

int db_exec_noresult(const char* sql, const int nparams, const void** params, const enum SQLITE_TYPE* types)
{
    sqlite3_stmt *stmt = NULL;
    const int stmt_rc = sqlite3_prepare(
                            get_db(),
                            sql,
                            -1,
                            &stmt,
                            0
                        );
    if (stmt_rc != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", sql);
        exit(1);
    }
    int i;
    for (i = 0; i < nparams; ++i) {
        const int position = i + 1;
        if (types[i] == INTEGER) {
            const int value = *((int *)params[i]);
            if (sqlite3_bind_int(
                        stmt,
                        position,
                        value
                    ) != SQLITE_OK) {
                ERROR("Cannot bind INTEGER %d in position %d", value, i);
                exit(1);
            }
        } else if (types[i] == REAL) {
            const double value = *((double *)params[i]);
            if (sqlite3_bind_double(
                        stmt,
                        position,
                        value
                    ) != SQLITE_OK) {
                ERROR("Cannot bind REAL %f in position %d", value, i);
                exit(1);
            }
        } else if (types[i] == TEXT) {
            const char* text = (char*)(params[i]);
            if (sqlite3_bind_text(stmt,
                                  position,
                                  text,
                                  strlen(text),
                                  SQLITE_STATIC
                                 ) != SQLITE_OK) {
                ERROR("Cannot bind TEXT '%s' in position %d", text, i);
                exit(1);
            }
        } else if (types[i] == BLOB) {
            ERROR("BLOB type is not supported yet.");
            exit(1);
        } else {
            ERROR("Unsipported type: %d", types[i]);
            exit(1);
        }
    }
    const int step = sqlite3_step(stmt);
    sqlite_stmt_free(stmt);
    if (step == SQLITE_BUSY) {
        ERROR("Cannot execute SQL '%s': Database is locked.", sql);
    }
    return step;
}

char ** unique_char_list(char ** input, const int n){
    if (n == 0){
        return NULL;
    }
    char ** new = NULL;
    int i;
    int n_elem = 0;
    for (i = 0; i < n; ++i){
        int j;
        for (j = 0; j < n_elem; ++j){
            if (strcmp(input[i], new[j]) == 0)
                break;
        }
        char ** tmp = (char**)realloc(new, sizeof(char*)*(n_elem + 1));
        if (tmp)
            new = tmp;
        else {
            ERROR("Cannot allocate memory.");
            exit(0);
        }
        new[n_elem] = (char*)malloc(sizeof(char) * (strlen(input[i]) + 1));
        strcpy(new[n_elem], input[i]);
        n_elem++;
    }
    return new;
}

int charlist_len(char** input){
    return sizeof(input) / sizeof(char*) + 1;
}