#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "str.h"

char* toUpper(char* text){
    char* p;
    char* newtext = (char*)malloc(sizeof(char) * (strlen(text) + 1));
    char* newp = newtext;
    for (p = text; *p; ++p, ++newp){
        if (*p >= 'a' && *p <= 'z')
            *newp = *p + 'A' - 'a';
        else
            *newp = *p;
    }
    *newp = '\0';
    return newtext;
}

char toLowerChar(char c)
{
    if (c >= 'A' && c <= 'Z')
        return c + 'a' - 'A';
    return c;
}