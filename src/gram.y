%{

#include <stdio.h>
#include <string.h>
#include "./lib.h"
#define YYSTYPE char*
void yyerror(const char *str)
{
        fprintf(stderr,"ошибка: %s\n",str);
}
 
int yywrap()
{
        return 1;
} 
   

%}
%union {int num; char *abbr;}         /* Yacc definitions */
%start line
%token SEMICOLON 
%token PARTICLE
%token ARROW
%token exit_command
%token print_left_command
%token print_right_command

%%


line:	reaction {;}
		| exit_command SEMICOLON {exit(EXIT_SUCCESS);}
		| print_left_command SEMICOLON {int i; for (i=0;i<l;i++) printf("%s\n", left_p[i].abbr);}
		| print_right_command SEMICOLON {int i; for (i=0;i<r;i++) printf("%s\n", right_p[i].abbr);}
		| line reaction {;}
		| line exit_command SEMICOLON {exit(EXIT_SUCCESS);}
		| line print_left_command SEMICOLON {int i; for (i=0;i<l;i++) printf("%s\n", left_p[i].abbr);}
		| line print_right_command SEMICOLON {int i; for (i=0;i<r;i++) printf("%s\n", right_p[i].abbr);}
	;

reaction: 	left_part ARROW right_part SEMICOLON {;}
		;
left_part:	left_particle
		|	left_part left_particle
		;
	
right_part:	right_particle
		| 	right_part right_particle
		;

left_particle:	PARTICLE
		{	
			int i = search($1);
			add_l($1);
//			printf("%d\n",l);
//			printf("ID:%d\n", i);
			printf("Left Particle %s\n", $1);
//			$$= symbolVal($1);
		}
		;	
right_particle:	PARTICLE
		{	
			int i = search($1);
			add_r($1);
//			printf("%d\n",r);
//			printf("ID:%d\n", i);
//			printf("%s\n", $1);
			printf("Right Particle %s\n", $1);
//			$$= symbolVal($1);
		}
		;
%%

struct Particle Particles[4321];

struct Particle left_p[4];
int l=0;
struct Particle right_p[4];
int r=0;
void add_l(char *left){
	left_p[l].abbr=left;
//	printf("Adding particle to the left part\n");
	l++;
}
void add_r(char *right){
	right_p[r].abbr=right;
//	printf("Adding particle to the right part\n");
	r++;
}
int search(char* key){
   	int i,j;
    	struct Particle p;
        	for (i=0; i<4321; i++){	
			for (j=1; j<i; j++){
                		if (strcmp(Particles[i].abbr, Particles[j].abbr)>0){
                        		p = Particles[i];
                        		Particles[i] = Particles[j];
                        		Particles[j] = p;
                		}
        		}
        	}
//	printf("%d\n",Particles[10].id);	

//	int ret = BinarySearch_Rec(Particles, "p", 0, 4400);
	int ret=-1;
    int left = 0;
    int right = 4350;
//	string key = "X";
    while (1)
    {
        int mid = left + (right - left) / 2;

        if (strcmp(Particles[mid].abbr,key)==0){
            	ret = Particles[mid].id;
		break;
	}
        if (strcmp(Particles[mid].abbr,key)<0)
            right = mid;
        else
            left = mid + 1;
    }

	return ret;
}


int main(){

	int n = 4321;
	struct Particle *Particles___ = make_part();
	printf("%s %s\n", Particles[0].abbr, Particles___[0].abbr);
	printf("OK\n");
 




//	printf("Abbr:%s Id:%d Orcode:%d\n", Particles___[1].abbr, Particles___[1].id, Particles___[1].orrcode);
	return yyparse();
}












/*static int BinarySearch_Rec(Particle array[], string key, int left, int right)
{
    int mid = left + (right - left) / 2;

    if (array[mid].abbr == key)
        return array[mid].id;

    else if (strcmp(array[mid].abbr.c_str(),key.c_str())>0)
        return BinarySearch_Rec(array, key, left, mid);
    else
        return BinarySearch_Rec(array, key, mid + 1, right);
}

int main(){
	printf("%d\n", Particles[10].id);
 
	
    	int i,j;
    	Particle p;
        	for (i=0; i<4400; i++){	
			for (j=1; j<i; j++){
                		if (strcmp(Particles[i].abbr.c_str(), Particles[j].abbr.c_str())>0){
                        		p = Particles[i];
                        		Particles[i] = Particles[j];
                        		Particles[j] = p;
                		}
        		}
        	}
	printf("%d\n",Particles[10].id);	

//	int ret = BinarySearch_Rec(Particles, "p", 0, 4400);
	int ret=-1;
    int left = 0;
    int right = 4350;
	string key = "X";
    while (true)
    {
        int mid = left + (right - left) / 2;

        if (strcmp(Particles[mid].abbr.c_str(),key.c_str())==0){
            	ret = Particles[mid].id;
		break;
	}
        if (strcmp(Particles[mid].abbr.c_str(),key.c_str())<0)
            right = mid;
        else
            left = mid + 1;
    }

	printf("%d\n", ret);
}
*/
//Поиск: сортируем массив по алфавиту, далее бинарный поиск
