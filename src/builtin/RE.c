#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "re.h"
#include "y.h" /* ../include/ */
#include "dg/RE.h"
#include "dg/P.h"

#include "settings.h"

#include "sqlite.h"

#include "database.h"
#include "builtin_search.h"


char* re2h(char*);

int
cross_RE(struct type_RE*, struct type_RE*);

static int done = 0;

#ifndef BUILTIN_EXE
#include "bash_builtin.h"
#include "signal.h"


void term (int signal){
	done = 1;
}

int 
RE_main(enum search_type, char*);

int
RE_builtin (list)
WORD_LIST *list;
{
    int ret = (EXECUTION_SUCCESS);
    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = term;
    sigaction(SIGINT, &action, NULL);

    done = 0;

    if (list == 0 || !(list->next)) {
        printf("Usage: RE %s|%s <statement>\n", STRONG_SEARCH_WORD, WEAK_SEARCH_WORD);
        fflush(stdout);
        return (EXECUTION_FAILURE);
    };
    reset_internal_getopt ();

    char* search_type_str = list->word->word;
    enum search_type stype = WEAK;
    if (strcmp(search_type_str, STRONG_SEARCH_WORD) == 0)
        stype = STRONG;
    char* query_str = list->next->word->word;
    if (RE_main(stype, query_str) < 0) 
	    ret = (EXECUTION_FAILURE);
    fflush (stdout);
    return ret;
}

int
RE_builtin_load (s)
char *s;
{
    fflush (stdout);
    return (1);
}

void
RE_builtin_unload (s)
char *s;
{
    fflush (stdout);
}

/* An array of strings forming the `long' documentation for a builtin xxx,
   which is printed by `help xxx'.  It must end with a NULL.  REy convention,
   the first line is a short description. */
char *RE_doc[] = {
    "RE utility",
    "",
    "RE itility for reaction search",
    (char *)NULL
};

/* The standard structure describing a builtin command.  bash keeps an array
   of these structures.  The flags must include REUILTIN_ENARELED so the
   builtin can be used. */
struct builtin RE_struct = {
    "RE",        /* builtin name */
    RE_builtin,      /* function implementing the builtin */
    BUILTIN_ENABLED,    /* initial flags for builtin */
    RE_doc,      /* array of long documentation strings. */
    "RE",        /* usage synopsis; becomes short_doc */
    0           /* reserved for internal use */
};

#endif

int
RE_main_strong (char* re_str) {
    // char *s1;
    int found = 0;
    char* db_reaction = NULL;
    char* value_sql = "select dg_data.rec_id, dg_value.value from dg_data inner join dg_value on dg_data.vid = dg_value.id where dg_value.tid = (select id from dg_type where type = ?) and dg_value.value like '%|' || ? ;";
    sqlite3_stmt *stmt;

    DEBUG("searching for: %s",re_str);
    char* re_hash = re2h(re_str);
    if (!re_hash) {
	    ERROR("errors in reaction: %s",re_str);
	    return (-1);
    }
    if (sqlite3_prepare(get_db(), value_sql, -1, &stmt, 0) != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", value_sql);
        return 0;
    };
    sqlite3_bind_text(stmt, 1, "RE", 2, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, re_hash, strlen(re_hash), SQLITE_STATIC);
    int step = sqlite3_step(stmt);
    int pid[2] = {-2, -2}; // new, old pid value
    while (step == SQLITE_ROW && done == 0) {
	const int dg_data_id = sqlite3_column_int(stmt, 0);
	char* val = (char*)sqlite3_column_text(stmt, 1);
	/**printf("v: re: %s\n",val);**/
	db_reaction = strtok_r(val, "|", &val);
        pid[1] = pid[0];
        pid[0] = dg_data_id;
        if (pid[0] != pid[1]) ++found;
        printf("%d\t%s\n", pid[0], db_reaction);
	step = sqlite3_step(stmt);
   }
   sqlite_stmt_free(stmt);
   return found;
};

int
RE_main_weak (char* re_str) {

	struct type_RE  *re1 = NULL, *re2 = NULL;
	int found = 0;
	char* db_reaction = NULL;

	DEBUG("searching for: %s",re_str);
	char* value_sql;
	value_sql = "select dg_data.rec_id, dg_value.value from dg_data inner join dg_value on dg_data.vid = dg_value.id where dg_data.tid = (select id from dg_type where type = ?);";
	re1 = (struct type_RE*)malloc(sizeof(struct type_RE));
	DEBUG("call s2listlist(re_str:%s: ...)\n",re_str);
	int r = s2listlist(re_str, &(re1->is),&(re1->fs));
	if (_DBG_) {
		if (re1->is) {
			DEBUG("\n\n >>>>> IS1 >>>>>>> \n\n");
			print_listlist(re1->is);
			DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
		if (re1->fs) {
			DEBUG("\n\n >>>>> FS1 >>>>>>> \n\n");
			print_listlist(re1->fs);
			DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
	}
	if (r > 0) {
		ERROR("errors in reaction: %s",re_str);
		return (-1);
	}
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(get_db(), value_sql, -1, &stmt, 0) != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", value_sql);
        return 0;
    };
    sqlite3_bind_text(stmt, 1, "RE", 2, SQLITE_STATIC);
    int step = sqlite3_step(stmt);
    int pid[2] = {-2, -2}; // new, old pid value
	while (step == SQLITE_ROW && done == 0) {
		int x = 0;
		const int dg_data_id = sqlite3_column_int(stmt, 0);
		char* val = (char*)sqlite3_column_text(stmt, 1);
		/**printf("v: re: %s\n",val);**/
		re2 = (struct type_RE*)malloc(sizeof(struct type_RE));
		db_reaction = strtok_r(val, "|", &val);
		int r = s2listlist((char*)db_reaction, &(re2->is),&(re2->fs));
		if (_DBG_) {
			if (re2->is) {
				DEBUG("\n\n >>>>> IS2 >>>>>>> \n\n");
				print_listlist(re2->is);
				DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
			}
			if (re2->fs) {
				DEBUG("\n\n >>>>> FS2 >>>>>>> \n\n");
				print_listlist(re2->fs);
				DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
			}
		}
		if (r > 0) {
			ERROR("skip reaction with errors: %s", val);
			free_RE(re2);
			step = sqlite3_step(stmt);
			continue;
		}
		DEBUG("{ %s | %s } = %d\n", re_str, val, x);
	    x = cross_RE(re1,re2);
		free_RE(re2);
		if (x != 0) {
            pid[1] = pid[0];
            pid[0] = dg_data_id;
            if (pid[0] != pid[1]) ++found;
            printf("%d\t%s\n", pid[0], db_reaction);
		}
		step = sqlite3_step(stmt);
	}

	// free_RE(re1); //FIXME: leak!!!
	sqlite_stmt_free(stmt);
	return found;
};

int 
RE_main (enum search_type stype, char* re_str) {
	int found = 0;
	if (strlen(re_str) == 0) {
		ERROR("empty reaction.");
		return (-1);
	}
	vy_init_P();
	fprintf(stderr, "RECORD\tREACTION\n");
	if (stype == STRONG)
		found = RE_main_strong(re_str);
	else
		found = RE_main_weak(re_str);
	if (found < 0)
		return (-1);
	fprintf(stderr, "Found %d records.\n", found);
	return 0;
};

#ifdef BUILTIN_EXE
int main (int argc, char** argv){
    if (argc != 3) {
        printf("Usage: RE %s|%s <statement>\n", STRONG_SEARCH_WORD, WEAK_SEARCH_WORD);
        return 0;
    };

    enum search_type stype = WEAK;
    if (strcmp(argv[1], STRONG_SEARCH_WORD) == 0)
        stype = STRONG;
    char* query_str = argv[2];
    return RE_main(stype, query_str);
}

#endif
