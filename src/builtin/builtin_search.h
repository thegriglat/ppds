#ifndef _builtin_search_h

#define STRONG_SEARCH_WORD "="
#define WEAK_SEARCH_WORD "@"

enum search_type {
    STRONG,
    WEAK
};

#define _builtin_search_h
#endif