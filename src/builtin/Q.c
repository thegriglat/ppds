#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "q.h"

#include "settings.h"
#include "sqlite.h"
#include "database.h"

#ifndef BUILTIN_EXE
#include "bash_builtin.h"


void
Q_main(char*);

int
Q_builtin (list)
WORD_LIST *list;
{
    int ret = (EXECUTION_SUCCESS);
    if (list == 0) {
        printf("Usage: Q <statement>\n");
        fflush(stdout);
        return ret;
    };
    reset_internal_getopt ();
    char* query = list->word->word;
    if (Q_search(query) != 0)
	    ret = (EXECUTION_FAILURE);
    fflush (stdout);
    return ret;
}

int
Q_builtin_load (s)
char *s;
{
    fflush (stdout);
    return (1);
}

void
Q_builtin_unload (s)
char *s;
{
    fflush (stdout);
}

/* An array of strings forming the `long' documentation for a builtin xxx,
   which is printed by `help xxx'.  It must end with a NULL.  Qy convention,
   the first line is a short description. */
char *Q_doc[] = {
    "Q search utility",
    "",
    "Q search itility for beam search",
    (char *)NULL
};

/* The standard structure describing a builtin command.  bash keeps an array
   of these structures.  The flags must include QUILTIN_ENAQLED so the
   builtin can be used. */
struct builtin Q_struct = {
    "Q",        /* builtin name */
    Q_builtin,      /* function implementing the builtin */
    BUILTIN_ENABLED,    /* initial flags for builtin */
    Q_doc,      /* array of long documentation strings. */
    "Q",        /* usage synopsis; becomes short_doc */
    0           /* reserved for internal use */
};

#endif

#ifdef BUILTIN_EXE
int main(int argc, char** argv){
    if (argc != 2){
        printf("Usage: Q <statement>\n");
        return 0;
    }
    return Q_search(argv[1]);
}

#endif
