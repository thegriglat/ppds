#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "dg/P.h"

#include "settings.h"
#include "sqlite.h"
#include "database.h"
#include "str.h"

#include "builtin_search.h"

static int done = 0;

#ifndef BUILTIN_EXE
#include "bash_builtin.h"
#include "signal.h"


int
P_main(char*);

void term(int signal){
    done = 1;
}

int
P_builtin (list)
WORD_LIST *list;
{
    int ret = (EXECUTION_SUCCESS);
    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = term;
    sigaction(SIGINT, &action, NULL);

    done = 0;

    if (list == 0) {
        printf("Usage: P <statement>\n");
        fflush(stdout);
        return (EXECUTION_FAILURE);
    };
    reset_internal_getopt ();
    if (strcmp(list->word->word, STRONG_SEARCH_WORD) != 0){
        printf("P search supports only strong search ('=' operator)!\n");
        return (EXECUTION_FAILURE);
    }
    char* p_str = list->next->word->word;
    if (P_main(p_str) < 0) 
	    ret = (EXECUTION_FAILURE);
    fflush (stdout);
    return ret;
}

int
P_builtin_load (s)
char *s;
{
    fflush (stdout);
    return (1);
}

void
P_builtin_unload (s)
char *s;
{
    fflush (stdout);
}

/* An array of strings forming the `long' documentation for a builtin xxx,
   which is printed by `help xxx'.  It must end with a NULL.  Py convention,
   the first line is a short description. */
char *P_doc[] = {
    "P search utility",
    "",
    "P search itility for beam search",
    (char *)NULL
};

/* The standard structure describing a builtin command.  bash keeps an array
   of these structures.  The flags must include PUILTIN_ENAPLED so the
   builtin can be used. */
struct builtin P_struct = {
    "P",        /* builtin name */
    P_builtin,      /* function implementing the builtin */
    BUILTIN_ENABLED,    /* initial flags for builtin */
    P_doc,      /* array of long documentation strings. */
    "P",        /* usage synopsis; becomes short_doc */
    0           /* reserved for internal use */
};

#endif

int
P_main (char* p_str)
{
	vy_init_P();
    struct type_P *b = (struct type_P*) s2t_P(p_str);
    if (!b) {
        ERROR("Cannot parse particle!");
        return (-1);
    }

    const char* value_sql = "select dg_data.rec_id from dg_data where dg_data.tid = (select id from dg_type where type = 'P') and dg_data.vid in (select id from vy_P where UPPER(abbreviation) = ?);";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(get_db(), value_sql, -1, &stmt, 0) != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", value_sql);
        exit(1);
    };
    char* upper_name = toUpper(p_str);
    sqlite3_bind_text(stmt, 1, upper_name, strlen(p_str), SQLITE_STATIC);
    int step = sqlite3_step(stmt);
    int found = 0;
    fprintf(stderr, "RECORD\tP\n");
    int pid[2] = {-2, -2}; // new, old pid value
    while (step == SQLITE_ROW && done == 0) {
        int p_db_id = sqlite3_column_int(stmt, 0);
        // check that pid changed
        pid[1] = pid[0];
        pid[0] = p_db_id;
        if (pid[0] != pid[1]) ++found;
        printf("%d\t%s\n", pid[0], p_str);
        step = sqlite3_step(stmt);
        if (done) break;
    }
    free(upper_name);
    sqlite_stmt_free(stmt);
    fprintf(stderr, "Found %d records.\n", found);
    return 0;
}

#ifdef BUILTIN_EXE
int main(int argc, char** argv){
  if (argc != 3){
    printf("Usage: P = <statement>\n");
    return 0;
  }
  return P_main(argv[2]);
}

#endif
