#include<stdio.h>
#include<stdlib.h>
#include<string.h>


#include "settings.h"
#include "sqlite.h"
#include "database.h"

static int done = 0;

#ifndef BUILTIN_EXE
#include "bash_builtin.h"
#include "signal.h"

void term(int signal){
    done = 1;
}

void
show_all_records_main();

int
show_all_records_builtin (list)
WORD_LIST *list;
{
    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = term;
    sigaction(SIGINT, &action, NULL);

    done = 0;

    show_all_records_main();
    fflush (stdout);
    return (EXECUTION_SUCCESS);
}

int
show_all_records_builtin_load (s)
char *s;
{
    fflush (stdout);
    return (1);
}

void
show_all_records_builtin_unload (s)
char *s;
{
    fflush (stdout);
}

/* An array of strings forming the `long' documentation for a builtin xxx,
   which is printed by `help xxx'.  It must end with a NULL.  show_all_recordsy convention,
   the first line is a short description. */
char *show_all_records_doc[] = {
    "show_all_records utility",
    "",
    "show_all_records utility for beam search",
    (char *)NULL
};

/* The standard structure describing a builtin command.  bash keeps an array
   of these structures.  The flags must include show_all_recordsUILTIN_ENAshow_all_recordsLED so the
   builtin can be used. */
struct builtin show_all_records_struct = {
    "show_all_records",        /* builtin name */
    show_all_records_builtin,      /* function implementing the builtin */
    BUILTIN_ENABLED,    /* initial flags for builtin */
    show_all_records_doc,      /* array of long documentation strings. */
    "show_all_records",        /* usage synopsis; becomes short_doc */
    0           /* reserved for internal use */
};

#endif

void
show_all_records_main()
{
    const char *value_sql = "select rec_id from dg_data group by rec_id;";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(get_db(), value_sql, -1, &stmt, 0) != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", value_sql);
        exit(1);
    };
    int step = sqlite3_step(stmt);
    while (step == SQLITE_ROW && done == 0) {
        int db_id = sqlite3_column_int(stmt, 0);
        printf("%d\n", db_id);
        step = sqlite3_step(stmt);
    }
    sqlite_stmt_free(stmt);
}

#ifdef BUILTIN_EXE

int main(int argc, char** argv){
    show_all_records_main();
    return 0;
}

#endif