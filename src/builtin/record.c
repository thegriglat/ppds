#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "settings.h"

#include "database.h"
#include "sqlite.h"

static int done = 0;

#ifndef BUILTIN_EXE
#include "bash_builtin.h"
#include "signal.h"

void term (int signal){
    done = 1;
}

void
record_main(char*);

int
record_builtin (list)
WORD_LIST *list;
{

    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = term;
    sigaction(SIGINT, &action, NULL);

    done = 0;

    if (list == 0) {
        printf("Usage: record <statement>\n");
        fflush(stdout);
        return (EXECUTION_SUCCESS);
    };
    reset_internal_getopt ();
    record_main(list->word->word);
    WORD_LIST* l;
    for (l = list->next; l && done == 0; l = l->next){
        record_main(l->word->word);
    }
    fflush (stdout);
    return (EXECUTION_SUCCESS);
}

int
record_builtin_load (s)
char *s;
{
    fflush (stdout);
    return (1);
}

void
record_builtin_unload (s)
char *s;
{
    fflush (stdout);
}

/* An array of strings forming the `long' documentation for a builtin xxx,
   which is printed by `help xxx'.  It must end with a NULL.  recordy convention,
   the first line is a short description. */
char *record_doc[] = {
    "record search utility",
    "",
    "record search itility for beam search\nUse 'record <id>' to show record content. Use 'record -1' to print all records",
    (char *)NULL
};

/* The standard structure describing a builtin command.  bash keeps an array
   of these structures.  The flags must include recordUILTIN_ENArecordLED so the
   builtin can be used. */
struct builtin record_struct = {
    "record",        /* builtin name */
    record_builtin,      /* function implementing the builtin */
    BUILTIN_ENABLED,    /* initial flags for builtin */
    record_doc,      /* array of long documentation strings. */
    "record",        /* usage synopsis; becomes short_doc */
    0           /* reserved for internal use */
};

#endif

struct row_id {
    int pid;
    int tid;
};

static struct row_id* rows_ids = NULL;
static int n_rows_ids = 0;

int isDotNeeded(struct row_id row){
    // return 1 if dot is needed else 0
    // if previous pid is the same -- dot needed
    if (n_rows_ids < 2) return 0;
    if (rows_ids[n_rows_ids - 1].tid == row.tid){
        return 1;
    }
    // look for the same tid on the same level
    int i;
    for (i = n_rows_ids - 1; i >= 0; --i){
        struct row_id r = rows_ids[i];
        if (r.tid == row.tid && r.pid == row.pid){
            return 1;
        }
    }
    return 0;
}

void appendPid(struct row_id row){
    rows_ids = (struct row_id *) realloc(rows_ids, sizeof(struct row_id) * (n_rows_ids + 1));
    rows_ids[n_rows_ids].tid = row.tid;
    rows_ids[n_rows_ids].pid = row.pid;
    ++n_rows_ids;
}

void free_row_id(){
    free(rows_ids);
    rows_ids = NULL;
    n_rows_ids = 0;
}

int print_row_by_recid(int rec_id){
    sqlite3_stmt* stmt;
    char* sql = "select dg_type.type, dg_value.value, dg_data.pid, dg_data.tid, dg_data.vid from dg_data inner join dg_type on dg_data.tid = dg_type.id left join dg_value on dg_data.vid = dg_value.id where dg_data.rec_id = ?;";

    if (sqlite3_prepare(get_db(), sql, -1, &stmt, 0) != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", sql);
        return 0;
    };
    sqlite3_bind_int(stmt, 1, rec_id);
    int step = sqlite3_step(stmt);
    if (step != SQLITE_ROW && step != SQLITE_DONE) {
        // rec_id does not exists!
        printf("Row %d does not exists!\n", rec_id);
        return 0;
    }
    while (step == SQLITE_ROW && done == 0){
        char* type = (char*)sqlite3_column_text(stmt, 0);
        char* value = (sqlite3_column_type(stmt, 1) == SQLITE_NULL) ? NULL : (char*)sqlite3_column_text(stmt, 1);
        const int pid = sqlite3_column_int(stmt, 2);
        const int tid = sqlite3_column_int(stmt, 3);
        const int vid = sqlite3_column_int(stmt, 4);
        struct row_id row;
        row.tid = tid;
        row.pid = pid;
        const char* dot = (isDotNeeded(row)) ? "." : "";
        appendPid(row);
        if (isDictNode(type)){
            char* val = getDictValueById(type, vid);
            printf("%s%s = %s;\n", type, dot, val);
            free(val);
        } else {
            if (value){
                if (strcmp(type, "RE") == 0){
                    char* val = strtok_r(value, "|", &value);
                    printf("%s%s = %s;\n", type, dot, val);
                } else
                    printf("%s%s = %s;\n", type, dot, value);
            } else
                printf("%s%s;\n", type, dot);
        }
        step = sqlite3_step(stmt);
    }
    printf("*E\n");
    sqlite_stmt_free(stmt);
    free_row_id();
    return 1;
}

int print_row(int rec_id, int ident, int usedot)
{
    sqlite3_stmt *stmt;
    const char* sql = "select id from dg_data where rec_id = ? and pid = -1 limit 1;";
    if (sqlite3_prepare(get_db(), sql, -1, &stmt, 0) != SQLITE_OK) {
            ERROR("Cannot prepare SQL: %s", sql);
            return 0;
    };
    sqlite3_bind_int(stmt, 1, rec_id);
    int step = sqlite3_step(stmt);
    if (step != SQLITE_ROW && step != SQLITE_DONE) {
        // rec_id does not exists!
        printf("Row %d does not exists!\n", rec_id);
        return 0;
    }
    sqlite_stmt_free(stmt);
    print_row_by_recid(rec_id);
    return 1;
}

void
record_main(char* rec_id)
{
    int id;
    if ((id = atoi(rec_id)) == 0) {
        printf("Argument '%s' is incorrect.\n", rec_id);
        return;
    }
    print_row(id, IDENT, 0);
};

#ifdef BUILTIN_EXE
int main(int argc, char** argv){
    if (argc == 1){
        printf("Usage: record <statement>\n");
        return 0;
    }
    int i = 0;
    for (i = 1; i < argc; ++i)
        record_main(argv[i]);
    return 0;
}

#endif