#ifndef _bash_builtin_h
// bash headers
#include <config.h>

#if defined (HAVE_UNISTD_H)
#  include <unistd.h>
#endif

#include <stdio.h>
#include "loadables.h"

#define _bash_builtin_h
#endif
// end bash headers