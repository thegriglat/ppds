#include<stdio.h>
#include<stdlib.h>
#include<string.h>



#include "dg/B.h"

#include "settings.h"
#include "sqlite.h"
#include "database.h"
#include "builtin_search.h"

static int done = 0;

#ifndef BUILTIN_EXE
#include "bash_builtin.h"
#include "signal.h"


void term(int signal){
    done = 1;
}

int
B_main(char*);

int
B_builtin (list)
WORD_LIST *list;
{
    int ret = (EXECUTION_SUCCESS);
    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = term;
    sigaction(SIGINT, &action, NULL);

    done = 0;

    if (list == 0 || !(list->next)) {
        printf("Usage: B %s <statement>\n", STRONG_SEARCH_WORD);
        fflush(stdout);
        return (EXECUTION_FAILURE);
    };
    reset_internal_getopt ();
    if (strcmp(list->word->word, STRONG_SEARCH_WORD) != 0){
        printf("B search supports only strong search ('=' operator)!\n");
        return (EXECUTION_FAILURE);
    }
    char* beam_str = list->next->word->word;
    if (B_main(beam_str) < 0) 
	    ret = EXECUTION_FAILURE;
    fflush (stdout);
    return ret;
}

int
B_builtin_load (s)
char *s;
{
    fflush (stdout);
    return (1);
}

void
B_builtin_unload (s)
char *s;
{
    fflush (stdout);
}

/* An array of strings forming the `long' documentation for a builtin xxx,
   which is printed by `help xxx'.  It must end with a NULL.  By convention,
   the first line is a short description. */
char *B_doc[] = {
    "B search utility",
    "",
    "B search itility for beam search",
    (char *)NULL
};

/* The standard structure describing a builtin command.  bash keeps an array
   of these structures.  The flags must include BUILTIN_ENABLED so the
   builtin can be used. */
struct builtin B_struct = {
    "B",        /* builtin name */
    B_builtin,      /* function implementing the builtin */
    BUILTIN_ENABLED,    /* initial flags for builtin */
    B_doc,      /* array of long documentation strings. */
    "B",        /* usage synopsis; becomes short_doc */
    0           /* reserved for internal use */
};

#endif

int
B_main(char* beam_str)
{
    struct type_B *b = (struct type_B*) s2t_B(beam_str);
    if (!b) {
        ERROR("Cannot parse beam");
        return (-1);
    }

    // const char *value_sql = "select id, value from dg_value where tid = (select id from dg_type where type = ?);";
    const char* value_sql = "select dg_data.rec_id, dg_value.value from dg_data inner join dg_value on dg_data.vid = dg_value.id where dg_data.tid = (select id from dg_type where type = ?);";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(get_db(), value_sql, -1, &stmt, 0) != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", value_sql);
        exit(1);
    };
    sqlite3_bind_text(stmt, 1, "B", 1, SQLITE_STATIC);
    int step = sqlite3_step(stmt);
    int found = 0;
    fprintf(stderr, "RECORD\tBEAM\n");
    int pid[2] = {-2, -2}; // new, old pid value
    while (step == SQLITE_ROW && done == 0) {
        int b_db_id = sqlite3_column_int(stmt, 0);
        const unsigned char* b_db_str = sqlite3_column_text(stmt, 1);
        struct type_B *b_db = (struct type_B*) s2t_B((char*)b_db_str);
        if (cross_B(b, b_db)) {
            // check that pid changed
            pid[1] = pid[0];
            pid[0] = b_db_id;
            if (pid[0] != pid[1]) ++found;
            printf("%d\t%s\n", pid[0], b_db_str);
        }
        free(b_db);
        step = sqlite3_step(stmt);
    }
    sqlite_stmt_free(stmt);
    free(b);
    fprintf(stderr, "Found %d records.\n", found);
    return 0;
}

#ifdef BUILTIN_EXE
int main(int argc, char** argv){
    if (argc != 3){
        printf("Usage B %s <statement>\n", STRONG_SEARCH_WORD);
        return 0;
    }
    return B_main(argv[2]);
}

#endif
