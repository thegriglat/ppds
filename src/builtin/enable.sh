#!/bin/bash

# enable/disable mode
mode="enable"
test "$1" = "-d" && mode="disable"

if test "$1" = "-h" -o "$1" = "--help"; then
    echo "Usage: source $0 [-d]"
    echo "  -d   unload modules"
    echo "(Un)load all builtins to bash."
    mode=""
fi

FIND_TEXT="show_all_records B record RE P Q SC AC DE DD A ABS ANSWER AV CB CB_TEX CD CD_TEX CE CE_TEX CIT COD COL COMP CONF CR CR_TEX CS D DCH DEN DK ECM ERROR I ILBL INDEX IRN IRNW KTO L NA PDGSC PL PP PPF PR QUESTION R RR RD RPP SB T TA TITLE_TEX TY URL"

for b in ${FIND_TEXT}; do
    fn="`dirname $BASH_SOURCE`/$b"
    if test -f "$fn"; then
        test "$mode" = "enable" && enable -f "$fn" $b
        test "$mode" = "disable" && enable -d $b
    else
        echo "File $fn not found!"
    fi
done