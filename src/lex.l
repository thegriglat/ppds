%{
#include <stdio.h>
#include "y.tab.h"
//extern YYSTYPE yylval;
%}

%%
"exit"		{return exit_command;}
"print_left"		{return print_left_command;}
"print_right"		{return print_right_command;}
\;		return SEMICOLON;
[a-zA-Z][a-zA-Z0-9*()+-]*	{yylval.abbr = strdup(yytext);return PARTICLE;}
-->		return ARROW;

%%
