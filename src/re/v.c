#include "dg/P.h"

int prt_id(char *s)
{
  struct type_P *p = (struct type_P *)s2t_P(s);
  return p ? p->id : -1;
}
