#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "re.h"
#include "y.h" /* ../include/ */
#include "dg/RE.h"

#include "settings.h"


extern
void
vy_init_P(); /* ../dg/P/f.c */

extern
int
cross_RE(struct type_RE *re1, struct type_RE *re2);


int
main (int argc, char** argv) {
	char *s1,*s2;
	int r, l1, l2, x;
	struct type_RE  *re1, *re2;
	if ((l1 = strlen(argv[1])) == 0) {
		ERROR("1st reaction empty.");
		return 1;
	}
	if ((l2 = strlen(argv[2])) == 0) {
		ERROR("2nd reaction empty.");
		return 1;
	}
	s1 = (char*)malloc(sizeof(char)*(l1+2));
	s2 = (char*)malloc(sizeof(char)*(l2+2));
	strcpy(s1,argv[1]);
	strcpy(s2,argv[2]);
	if (s1[l1-1] != ';') { s1[l1] = ';'; s1[l1+1] = '\0'; }
	if (s2[l2-1] != ';') { s2[l2] = ';'; s2[l2+1] = '\0'; }
	re1 = (struct type_RE*)malloc(sizeof(struct type_RE)),
	re2 = (struct type_RE*)malloc(sizeof(struct type_RE));
	DEBUG("call s2listlist(s1:%s: ...)\n",s1);

	vy_init_P();

	r = s2listlist(s1, &(re1->is),&(re1->fs));
	/*printf("s2listlist(): ret: %d\n",r);*/
	if (_DBG_) {
		if (re1->is) {
			DEBUG("\n\n >>>>> IS1 >>>>>>> \n\n");
			print_listlist(re1->is);
			DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
		if (re1->fs) {
			DEBUG("\n\n >>>>> FS1 >>>>>>> \n\n");
			print_listlist(re1->fs);
			DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
	}
	if (r > 0) {
		ERROR("%s: errors in reaction: %s",argv[0],s1);
		return 1;
	}

	r = s2listlist(s2, &(re2->is),&(re2->fs));

	if (_DBG_) {
		if (re2->is) {
			DEBUG("\n\n >>>>> IS2 >>>>>>> \n\n");
			print_listlist(re2->is);
			DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
		if (re2->fs) {
			DEBUG("\n\n >>>>> FS2 >>>>>>> \n\n");
			print_listlist(re2->fs);
			DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
	}

	if (r > 0) {
		ERROR("%s: errors in reaction: %s",argv[0],s2);
		return 1;
	}

	x = cross_RE(re1,re2);

	printf("%d\n",x);

	return 0;
}
