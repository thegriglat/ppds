#include<stdio.h>
#include<string.h>

#include "re.h"
#include "y.h" /* ../include/ */


extern  
void 
vy_init_P(); /* ../dg/P/f.c */

int
main (void) {
	char s[2048];
	int r, l, i;
	struct listlist *is, *fs;
	vy_init_P();
	printf("\n#####################\nRE?\n");
	while (fgets(s, sizeof (s), stdin)) {
		if ((l = strlen(s)) > 0) {
			for (i = l-1; i >= 0 && s[i] != ';' ; i--);
			if (i >= 0) {
				s[i+1] = '\0';
			}
			else {
				s[l-1] = ';';
				s[l] = '\0';
			}
		}
		printf("call s2listlist(s:%s: ...)\n",s);
		r = s2listlist(s, &is,&fs);
		printf("s2listlist(): ret: %d\n",r);
		if (is) {
			printf("\n\n >>>>> IS >>>>>>> \n\n");
			print_listlist(is);
			printf("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
		if (fs) {
			printf("\n\n >>>>> FS >>>>>>> \n\n");
			print_listlist(fs);
			printf("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
		printf("\n#########################################\n\nRE?\n");
	}
	return 0;
}
/*****/
