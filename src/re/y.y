%{
#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include <math.h>


/* lex stuff: */

typedef struct ppdsre_buffer_state *PPDSRE_BUFFER_STATE;

extern
PPDSRE_BUFFER_STATE
ppdsre_scan_string (const char*);

extern
void
ppdsre_delete_buffer (PPDSRE_BUFFER_STATE);
extern int yylex();

/***/


#include "re.h"
#include "settings.h"

extern
int eq_prt_id(int,int);

static
struct node
*fs, *is, *fs_norm, *is_norm;

static
struct node_list
*fs_norm_list, *is_norm_list;

static
struct listlist
*fs_listlist, *is_listlist, *fs_listlist_contr, *is_listlist_contr;


static
int error_count;

void
yyerror(char*);

%}

%token ARROW PRT NUM
%left '+' '-'
%left '*'
%left '.'

%union r_union_type {
        int id;
        struct node *no;
};

%%


input:
     input re ';' {
	if (_DBG_) {
     	   printf("\n>>>>>>>>>>>> RES: >>>>>>>>>>>>>>>>>> \n\n");
	   if (is) {
		printf("   --- IS: >>>>>>>>>\n");
		print_tree( is );
		printf("\n\n .... norm: .....\n\n");
		print_tree( is_norm );
		printf("\n\n ...... T2TL:  .....\n\n");
		print_treelist( is_norm_list );
		printf("\n\n ...... -> LL ....... \n\n");
		print_listlist( is_listlist );
		printf("\n\n ....., -> CLL .......\n\n");
		print_listlist( is_listlist_contr );
		printf("\n\n   <<<<<<<<<<<<<<<<<<<<<<<< IS ---\n\n");
	   }
	   if (fs) {
		printf("   --> FS: >>>>>>>>>\n");
		print_tree( fs );
		printf("\n\n .... norm: .....\n\n");
		print_tree( fs_norm );
		printf("\n\n ...... T2TL:  .....\n\n");
		print_treelist( fs_norm_list );
		printf("\n\n ...... -> LL ....... \n\n");
		print_listlist( fs_listlist );
		printf("\n\n ....., -> CLL .......\n\n");
		print_listlist( fs_listlist_contr );
		printf("\n\n   <<<<<<<<<<<<<<<<<<<<<<<< FS ---\n\n");
	   }
	}
     }
     |
     ;

re:
    expr ARROW expr {
    	is = $<no>1;
	fs = $<no>3;
    	is_norm = norm_tree(is);
	fs_norm = norm_tree(fs);
	is_norm_list = tree2treelist( is_norm );
	fs_norm_list = tree2treelist( fs_norm );
	is_listlist = treelist2listlist(NULL, is_norm_list, 1);
	fs_listlist = treelist2listlist(NULL, fs_norm_list, 1);
	is_listlist_contr = contract_listlist( is_listlist , eq_prt_id );
	fs_listlist_contr = contract_listlist( fs_listlist , eq_prt_id );
    }
    |
    expr            {
	/*DEBUG("   --- IS: >>>>>>>>>\n");*/
    	is = $<no>1;
/*	_DBG_&&print_tree( is );*/
	fs = NULL;
/*	DEBUG("\n\n .... norm: .....\n\n");*/
    	is_norm = norm_tree(is);
/*	_DBG_&&print_tree( is_norm );*/
	fs_norm = NULL;
/*	DEBUG("\n\n ...... T2TL:  .....\n\n");*/
	is_norm_list = tree2treelist( is_norm );
/*	_DBG_&&print_treelist( is_norm_list );*/
	fs_norm_list = NULL;
/*	DEBUG("\n\n ...... -> LL ....... \n\n");*/
	is_listlist = treelist2listlist(NULL, is_norm_list, 1);
/*	_DBG_&&print_listlist( is_listlist );*/
	fs_listlist =  NULL;
/*	DEBUG("\n\n ....., -> CLL .......\n\n");*/
	is_listlist_contr = contract_listlist( is_listlist , eq_prt_id );
/*	_DBG_&&print_listlist( is_listlist_contr );*/
	fs_listlist_contr = NULL;
    }
    ;

expr:
    PRT { $<no>$ = new_node(_TERM_, $<id>1,1, NULL, NULL,NULL); if ($<id>1 == -1) error_count ++; }
    |
    NUM PRT { $<no>$ = new_node(_TERM_, $<id>2,$<id>1, NULL,  NULL,NULL); if ($<id>2 == -1) error_count ++; }
    |
    PRT '<' expr '>' { $<no>$ = new_node(_TERM_, $<id>1,1, $<no>3, NULL,NULL); }
    |
    NUM PRT '<' expr '>' { $<no>$ = new_node(_TERM_, $<id>2,$<id>1, $<no>4, NULL,NULL); }
    |
    /*
    expr '.' expr { $<no>$ = new_node(_PRJ_,0,1, $<no>1,$<no>3); }
    |
    */
    expr '*' expr { $<no>$ = new_node(_MUL_,0,1, 0, $<no>1,$<no>3); }
    |
    expr '+' expr { $<no>$ = new_node(_PLUS_,0,1, 0, $<no>1,$<no>3); }
    |
    expr '-' expr { $<no>$ = new_node(_MINUS_,0,1, 0, $<no>1,$<no>3); }
    |
    '(' expr ')'  { $<no>$ = $<no>2; }
    ;
%%

void
ppdsreerror(char* s) {
	fprintf(stdin,"ERROR: RE: %s\n",s);
}


int
s2listlist (char* s, struct listlist** is_ll, struct listlist** fs_ll) {
	int ret, l;
	char *stmp = 0;
	PPDSRE_BUFFER_STATE buffer;
	DEBUG("(s:%s: ...) ...",s);
	if (!s) {
		fprintf(stdin,"ERROR: RE: s2listlist( s = NULL ...). EXIT(1)\n");
		exit(1);
	}
	if((l = strlen(s)) < 1) {
		printf("WARNING: RE: s2listlist(  strlen(s) == 0 ... ) => is = fs = NULL\n");
		*is_ll = *fs_ll = 0;
		return 0;
	}
	stmp = (char*)malloc(sizeof(char)*(strlen(s)+1));
	strcpy(stmp,s);
	if (stmp[l-1] != ';') {
		stmp = (char*)realloc(stmp,sizeof(char)*(l+2));
		stmp[l] = ';'; stmp[l+1] = '\0';
	}
	DEBUG("RE: s2listlist( %s ): bp000 >>>>>>\n",stmp);
	buffer = ppdsre_scan_string(stmp);
	error_count = 0;
	ret = ppdsreparse();
	/*printf("s2listlist: ret: %d err_cnt: %d\n",ret,error_count);*/
	if (error_count > 0)
		ret = 2;
	ppdsre_delete_buffer(buffer);
	*is_ll = is_listlist_contr;
	*fs_ll = fs_listlist_contr;
	if (is_norm) free_tree(is_norm);
	if (fs_norm) free_tree(fs_norm);
	free((void*)stmp);
	return ret;
}

void
free_re_intermediate(void) { /*FIXME: call after listlist's are free()'d, see ../dg/RE/f.c: free_RE() */
	if (is_norm_list) free_node_list(is_norm_list);
	if (fs_norm_list) free_node_list(fs_norm_list);
}

/*****/
