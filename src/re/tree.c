#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "re.h"

#include "settings.h"

#include "dg/P.h"

int
eq_prt_id(int id1, int id2) {
	return id1 == id2;
}
int
eq_prt_x(int id1, int id2) {
	return id1 == id2 || (id1*id2 == 0); /* everything annihilates with X (id = 0) */
}
int
eq_prt_Y(int id1, int id2) {
	return id1 == id2;
}

struct node *
new_node(enum op_type op, int id, int n, struct node *dec, struct node *left, struct node *right) {
	struct node *neww = (struct node *)malloc(sizeof(struct node));
	neww->op = op;
	neww->id = id;
	neww->n  = n;
	neww->dec = dec;
	neww->left = left ;
	neww->right = right;
	return neww;
}

struct node *
copy_tree (struct node *no) {
	struct node *neww = (struct node *) malloc(sizeof(struct node));
	neww->id = no->id;
	neww->n  = no->n;
	if (no->dec) {
		neww->dec = copy_tree(no->dec);
	}
	else {
		neww->dec = NULL;
	}
	if ((neww->op = no->op) != _TERM_) {
		neww->left = copy_tree(no->left);
		neww->right = copy_tree(no->right);
	}
	else {
		neww->left = neww->right = NULL;
	}
	return neww;
}

struct node *
norm_tree (struct node *no) {
	enum op_type op_left, op_right, op_old;
	struct node
	*neww_r = 0,
	*neww_l1 = 0, *neww_l2 = 0,
	*neww_node = 0,
	*neww_r1 = 0, *neww_r2 = 0,
	*neww_l = 0;
	if (!no) {
		return NULL;
	}
	op_old = no->op;
	if (op_old == _TERM_) {
		no->dec = norm_tree(no->dec);
		return no;
	}
	no->left = norm_tree(no->left);
	no->right = norm_tree(no->right);
	if (op_old == _MUL_ /*|| op_old == _PRJ_*/)  {
		if ((op_left = no->left->op) == _PLUS_ || op_left == _MINUS_) {
			neww_l1 = norm_tree(no->left->left);
			neww_l2 = norm_tree(no->left->right);
			neww_r = norm_tree(no->right);
			neww_node = (struct node *) malloc(sizeof(struct node));
			neww_node->op = op_old;
			neww_node->id = 0;
			neww_node->n  = 1;
			neww_node->dec = NULL;
			neww_node->left = neww_l2;
			neww_node->right = neww_r;
			no->right = neww_node;
			no->left->left = neww_l1;
			no->left->right= copy_tree(neww_r);
			no->left->op = op_old;
			no->op = op_left;
		}
		else if ((op_right = no->right->op) == _PLUS_ || op_right == _MINUS_) {
			neww_r1 = norm_tree(no->right->left);
			neww_r2 = norm_tree(no->right->right);
			neww_l = norm_tree(no->left);
			neww_node = (struct node *) malloc(sizeof(struct node));
			neww_node->op = op_old;
			neww_node->id = 0;
			neww_node->n  = 1;
			neww_node->dec = NULL;
			neww_node->left = neww_r1;
			neww_node->right = neww_l;
			no->left = neww_node;
			no->right->left = neww_r2;
			no->right->right = copy_tree(neww_l);
			no->right->op = op_old;
			no->op = op_right;
		}
	}
	no->left = norm_tree(no->left);
	no->right = norm_tree(no->right);
	return no;
}

void
print_prt(int id) {
	printf("prt(%s)",id2s_P(id));
	return;
}

void
print_tree(struct node *no) {
	/*  DEBUG("print_tree(): op = %d\n",(int)no->op);*/
	switch(no->op) {
	case _TERM_:
		if(no->n != 1) {
			printf("%d*",no->n);
		}
		print_prt(no->id);
		if (no->dec) {
			printf(" < ");
			print_tree(no->dec);
			printf(" > ");
		}
		break;
	case _PLUS_:
	case _MINUS_:
		printf(no->op == _PLUS_ ? " + ( " : " - ( " );
		print_tree(no->left);
		printf(" , ");
		print_tree(no->right) ;
		printf(" ) ");
		break;
	case _MUL_:
		printf(" * ( ");
		print_tree(no->left);
		printf(" , ");
		print_tree(no->right);
		printf(" ) ");
		break;
	/*
	case _PRJ_:
		printf(" . ( ");
		print_tree(no->left);
		printf(" , ");
		print_tree(no->right);
		printf(" ) ");
		break;
	*/
	default:
		printf("ERROR: print_tree(): no->op = %d\n",(int)no->op);
		break;
	}
	/*  DEBUG("print_tree(): bp1");*/
	return;
}

void
free_tree (struct node *no) {
	switch(no->op) {
	case _TERM_:
		if (no->dec) {
			free_tree(no->dec);
		}
		break;
	case _PLUS_:
	case _MINUS_:
		free_tree(no->left);
		free_tree(no->right) ;
		break;
	case _MUL_:
		free_tree(no->left);
		free_tree(no->right);
		break;
	default:
		fprintf(stdin,"ERROR: free_tree(): no->op = %d\n",(int)no->op);
		break;
	}
	free((void*) no);
	return;
}

void
free_node_list (struct node_list *no) { /* ATTENTION: lists are free()'d before calling this */
	switch(no->op) {
		case _TERM_:
			break;
		case _PLUS_:
		case _MINUS_:
			if (no->left)
				free_node_list(no->left);
			if (no->right)
				free_node_list(no->right);
			break;
		default:
			fprintf(stdin,"ERROR: free_node_list: unknown op: %d\n",no->op);
			exit(1);
	}
	free((void*)no);
	return;
}


struct list *
last (struct list *l) {
	struct list *no = l;
	if (!no)
		return 0;
	while (no->next) {
		no = no->next;
	}
	return no;
}

void
mul (struct list *l, int n) {
	struct list *no = l;
	for (; no; no = no->next)
		no->n *= n;
	return;
}

struct list *
cat (struct list *l1, int n1, struct list *l2, int n2) {
	struct list *l1_last = last (l1);
	if (n1 != 1) {
		mul(l1,n1);
	}
	if (n2 != 1) {
		mul(l2,n2);
	}
	if (!l1_last)
		return l2;
	else
		l1_last->next = l2;
	if (l2)
		l2->prev = l1_last;
	return l1;
}

void
free_list(struct list *l) {
	struct list *no, *no1;
	DEBUG("FL(): >>>>>");
	if (!l) {
		DEBUG("FL:bp000: l == 0, ret <<<<<");
		return;
	}
	for (no = l; no;) {
		if (no->dec) {
			DEBUG("FL:bp001: call FLL( no->dec ) >>>>");
			free_listlist(no->dec);
			DEBUG("FL:bp001-1: <<<<<<<");
		}
		no1 = no->next;
		DEBUG("FL:bp0002 free(no)");
		free((void *)no);
		no = no1;
	}
	DEBUG("FL: return <<<<<<");
	return;
}

struct list *
tree2list(struct list *prev, struct node *no) {
	struct list *neww = NULL;
	struct node_list *tl = NULL;
	DEBUG("BP1111111: t2l() .... >>>>>");
	switch(no->op) {
	case _TERM_:
		neww = (struct list *)malloc(sizeof(struct list));
		DEBUG("t2l:bp000: op = _TERM_ id: %d n: %d >>>\n",no->id,no->n);
		neww->id = no->id;
		neww->n  = no->n;
		if (no->dec) {
			DEBUG("t2l:bp0001: dec != 0: call tl2ll( ... t2tl(no->dec)) >>>>");
			neww->dec = treelist2listlist(NULL, tl = tree2treelist(no->dec), 1);
			if (tl)
				free_node_list(tl);
			DEBUG("t2l:bp0001-1:  <<<<<<");
		}
		else {
			DEBUG("t2l:bp0002: dec == 0");
			neww->dec = NULL;
		}
		if (prev) {
			prev->next = neww;
		}
		neww->prev = prev;
		neww->next = NULL;
		DEBUG("t2l:bp000-1: <<<<<");
		break;
	case _MUL_:
		DEBUG("t2l:bp001: op = _MUL_ neww = cat (t2l(left),t2l(right) >>>>");
		neww = cat( tree2list(NULL,no->left),1, tree2list(NULL,no->right),1 );
		DEBUG("t2l:bp001-1 <<<<<");
		if (prev) {
			prev->next = neww;
		}
		neww->prev = prev;
		break;
	default:
		fprintf(stdin,"ERROR: tree2list(): wrong no->op: %d\n",(int)no->op);
		exit(1);
		break;
	}
	DEBUG("BP1111111-1: t2l() <<<<<<<<");
	return neww;
}

struct list *
sort_list (struct list *l, int (*eq_prt)(int,int) ) {
	struct list *no = l, *no1, *no2;
	struct listlist *no_dec;
	int min_id, no_id, no_n, id_x = 9998; /* keep X to the left of YY  FIXME: hardcoded 9999*/
	DEBUG("sort_list(): bp0000 >>>> ");
	if (!l) {
		return l;
	}
	do {
		if (eq_prt == eq_prt_Y && no->id == 9999)
			break; /* only the part of the list to the left of YY is sorted */
		if (no->dec) {
			DEBUG("sort_list(): bp0001: no->dec = contract_listlist() >>>");
			no->dec = contract_listlist(no->dec, eq_prt);
			DEBUG("sort_list(): bp0001-1:  contract_listlist() returned <<<<");
		}
		min_id = ( no->id ? no->id : id_x ); /* X must always be in the end of list: p1 p2 ... X */
		no2 = NULL;
		for (no1 = no->next; no1; no1 = no1->next) {
			if (eq_prt == eq_prt_Y && no1->id == 9999)
				break;
			if ((no1->id ? no1->id : id_x ) < min_id) {
				no2 = no1;
				min_id = ( no1->id ? no1->id : id_x );
			}
		}
		if (min_id < ( no->id ? no->id : id_x ) ) {
			no_id = no->id;
			no_n = no->n;
			no_dec = no->dec;
			no->id = no2->id;
			no->n = no2->n;
			no->dec = no2->dec;
			no2->id = no_id;
			no2->n = no_n;
			no2->dec = no_dec;
		}
	} while ((no = no->next));
	DEBUG("sort_list(): bp0000-1: return l <<<");
	return l;
}

struct list *
contract_list(struct list *l, int (*eq_prt )(int,int) ) {
	struct list *no = l, *no1, *no2;
	if(_DBG_) {
		printf("contract_list: bp0000 >>>>> \n");
		print_list(l);
	}
	do {
		if (no->dec) {
			no->dec = contract_listlist(no->dec, eq_prt);
		}
	} while ((no = no->next));
	no = l;
	if (_DBG_) { printf("before do {:\n"); print_list(l); printf("contract_list(): do {...\n"); }
	do {
		if(_DBG_) { printf("do { .. .%d*no->id : %d\n", no->n,no->id); print_list(l); printf("\n"); }
		for (no1 = no->next; no1;) {
			if (_DBG_) {
				printf("contract_list: %d*no->id: %d , %d*no1->id: %d bp00001 ... eq_listlist(c_ll,cll) ...",
					no->n, no->id,
					no1->n,no1->id);
				print_list(l);printf("\n");
			}
			if ( eq_prt(no1->id, no->id)
			     &&
			     (eq_prt == eq_prt_Y
			         ?
				 ( cross_listlist(no1->dec,no->dec) != 0 ) /* order!!! */
				 :
			         ( no1->id * no->id != 0 || (no1->id == 0 && no->id == 0)
				    ?
				    eq_listlist( no->dec, no1->dec )
				    :
				    1
			         )
		             )
			   ) {
				DEBUG(" no ~ no1 \n");
				if (eq_prt == eq_prt_id) {
					no->n += no1->n;
					if (no->id == 0) { /* X X == X */
						if (no->n > 1)
							no->n = 1;
						else if (no->n < -1)
							no->n = -1;
					}
					no1->prev->next = no1->next;
					if ((no2 = no1->next)) {
				  		no2->prev = no1->prev;
					}
					free_listlist(no1->dec);
					free((void *)no1);
					no1 = no2;
				}
				else
				if (eq_prt == eq_prt_x) { /* a X^-n -> a^0 X^-n, */
					DEBUG(" eq_prt != eq_prt_id \n");
					if (no->id != 0) {
						DEBUG("no->id != 0\n");
						if (no1->id != 0) {
							DEBUG("no1->id != 0\n");
							no->n += no1->n;
							no1->prev->next = no1->next;
							if ((no2 = no1->next)) {
				  				no2->prev = no1->prev;
							}
							free_listlist(no1->dec);
							free((void *)no1);
							no1 = no2;
						}
						else {
							DEBUG("/* X from no1 swallows no->id and remains */\n");
							if (no->n * no1->n < 0) {
								no->n = 0;
								DEBUG("no->n = 0\n");
							}
							no1 = no1->next;
						}
					}
					else {
						DEBUG ("/* no: X */\n");
						if (no1->id == 0) {
							DEBUG("/* X^(no->n) X^(no1->n) */\n");
							no1 = no1->next;
						}
						else {
							if(_DBG_)
								printf("/* X ^(no->n) a^(no1->n) -> X^(no->n) */\n");
							if (no->n * no1->n < 0) {
								DEBUG("no->n * no1->n < 0\n");
								no1->prev->next = no1->next;
								if ((no2 = no1->next)) {
				  					no2->prev = no1->prev;
								}
								free_listlist(no1->dec);
								free((void *)no1);
								no1 = no2;
							}
							else {
								no1 = no1->next;
							}
						}
					}
				}
				else { /* cross uses eq_prt_Y() */
					if (no->n + no1->n < 0) {
						no1->n += no->n;
						no->n = 0;
						no1 = no1->next;
					}
					else {
						no->n += no1->n;
						if (no->id == 0) { /* X X == X */
							if (no->n > 1)
								no->n = 1;
							else if (no->n < -1)
								no->n = -1;
						}
						no1->prev->next = no1->next;
						if ((no2 = no1->next)) {
				  	  		no2->prev = no1->prev;
						}
						free_listlist(no1->dec);
						free((void *)no1);
						no1 = no2;
					}
				}
			}
			else {  /* no->id !~ no1->id */
				if(_DBG_)
					printf ("no1 = no1->next ... \n");
				no1 = no1->next;
			}
		}
	} while ((no = no->next) && (eq_prt == eq_prt_Y ? no->id != 9999 : 1));
	if(_DBG_) {
		printf("removing 0*a ...: "); print_list(l); printf(" \n");
	}
	for (no = l; no;) {
		DEBUG("%d * no->id: %d ... \n",no->n,no->id);
		if (no->n == 0) {
			if (no->prev) {
				no->prev->next = no->next;
				if (no->next)
					no->next->prev = no->prev;
				no2 = no->next;
				free_listlist(no->dec);
				free((void*)no);
				no = no2;
			}
			else {
				if ((l = no->next))
					l->prev = 0;
				free_listlist(no->dec);
				free((void*)no);
				no = l;
			}
		}
		else {
			DEBUG("%d*no->id: %d ...\n",no->n,no->id);
			if (no->id == 0) {
				DEBUG("no->id == 0 ...\n");
				for (no1 = no->next; no1;) {
					DEBUG("%d*no1->id: %d\n",no1->n,no1->id);
					if (no1->id == 0 && no1->n != 0) {
						DEBUG("eq_ll(no->dec,no1->dec) ? ..\n");
						if (eq_listlist(no->dec,no1->dec)) {
							DEBUG("/* X^n < state > X^n1 < state > ->  X^(n+n1) < state > */\n");
							no->n += no1->n;
							no1->prev->next = no1->next;
							if ((no2 = no1->next))
								no1->next->prev = no1->prev;
							free_listlist(no1->dec);
							free((void*)no1);
							no1 = no2;
							DEBUG("no1 = no2\n");
						}
						else {
							no1 = no1->next;
							DEBUG("no1 = no1->next\n");
						}
					}
					else {
						no1 = no1->next;
						DEBUG("! (no1->id == 0 && no1->n != 0) ==> no1 = no1->next\n");
					}
				}
				no = no->next;
			}
			else {
				DEBUG("no->id != 0 ==> no = no->next\n");
				no = no->next;
			}
		}
	}
	if(_DBG_) {
		printf("0*a removed: "); print_list(l); printf(" \n");
	}
	no2 = sort_list(l,eq_prt);
	if(_DBG_) {
		printf("sorted: "); print_list(no2); printf("\n");
	}
	return no2;
}


int
eq_list(struct list *l1, struct list *l2) {
	struct list *no1 = l1, *no2 = l2;
	for(;;) {
		if (no1 == NULL && no2 == NULL) {
			return 1;
		}
		if (no1 == NULL || no2 == NULL) {
			return 0;
		}
		if (!(no1->id == no2->id && no1->n == no2->n && eq_listlist(no1->dec,no2->dec))) {
			return 0;
		}
		no1 = no1->next;
		no2 = no2->next;
	}
}

/*------------------------------------------------------*/

int
eq_listlist(struct listlist *l1, struct listlist *l2) {
	struct listlist *no1 = l1, *no2 = l2;
	for (;;) {
		if (no1 == NULL && no2 == NULL) {
			return 1;
		}
		if (no1 == NULL || no2 == NULL) {
			return 0;
		}
		if (! (no1->n == no2->n && eq_list(no1->l,no2->l)) ) {
			return 0;
		}
		no1 = no1->next;
		no2 = no2->next;
	}
}

struct list*
copy_list(struct list *l);

struct listlist*
copy_listlist(struct listlist* l) {
	struct listlist *no, *nop, *non, *ln = 0;
	if (!l)
		return 0;
	nop = 0;
	for (no = l; no; no = no->next) {
		non = (struct listlist*)malloc(sizeof(struct listlist));
		non->n = no->n;
		non->l = copy_list(no->l);
		if (!(non->prev = nop))
			ln = non;
		else
			nop->next = non;
		non->next = 0;
		nop = non;
	}
	return ln;
}

struct list*
copy_list(struct list* l) {
	struct list *ln = 0, *no, *nop, *non;
	if (!l)
		return 0;
	nop = 0;
	for (no = l; no; no = no->next) {
		non = (struct list*)malloc(sizeof(struct list));
		non->id = no->id;
		non->n  = no->n;
		non->dec = (no->dec ? copy_listlist(no->dec) : 0 );
		if (!(non->prev = nop))
			ln = non;
		else
			nop->next = non;
		non->next = 0;
		nop = non;
	}
	return ln;
}

int
cross_listlist(struct listlist *l1, struct listlist* l2);

int
cross_list(int n1, struct list* list1, int n2, struct list *list2) {
	struct list
	*l1 = copy_list(list1),
	*l2 = copy_list(list2),
	*lx;
	int x = 0;
	mul(l1,-1);
	mul(l2, 1);
	if(_DBG_) {
		printf("==== cross list ====\n >>>>>> list1 >>> \n");
		print_list(list1);
		printf("\n<<<< list1 -> l1 >>>>>\n");
		print_list(l1);
		printf("\n<<<< l1\n >>>>>>> list2 >>>\n");
		print_list(list2);
		printf("\n<<<< list2 -> l2 >>>\n");
		print_list(l2);
	}
	lx = cat(l2,1,l1,1);
	if(_DBG_) {
		printf("\n >>>> l2 l1^-1 >>>>\n");
		print_list(lx);
		printf("\n>>> contr. >>>\n");
	}
	lx = contract_list(lx, eq_prt_Y);
	if(_DBG_) {
		print_list(lx);
		printf("\n========\n");
	}
	/*
	for (x = 1, no = lx; no; no = no->next) {
		if (no->n != 0 && !(no->id == 0 && no->dec == 0) ) { // <state1|state2> = X^n => <|> = 1 //
			x = 0;
			break;
		}
	}
	*/
	x = ( lx ? (lx->id == 9999 ? 1 : 0) : 1 );
	if (lx)
		free_list(lx);
	if (_DBG_) {
		printf("cross list{ %d * ( ",n1); print_list(list1); printf(" ) | %d * ( ",n2);
		print_list(list2);
		printf(" }\n");
		printf(" ====> cross_list: ret %d * %d*%d;\n\n",x,n1,n2);
	}
	return x * n1 * n2;
}

int
cross_listlist(struct listlist *l1, struct listlist *l2) {
	struct listlist *no1,*no2;
	int x = 0;
	if (l1 == 0 && l2 == 0)
		return 1;
	for (no1 = l1; no1; no1 = no1->next)
		for (no2 = l2; no2; no2 = no2->next)
			x += cross_list(no1->n,no1->l, no2->n,no2->l);
	return x;
}

void
free_listlist(struct listlist *l)  { //FIXME
	struct listlist *no, *no1;
	if(!l) {
		return;
	}
	for (no = l; no;) {
		if (no->l) {
			DEBUG("FLL: no: print_list(no->l) ");
			if (_DBG_)
				print_list(no->l);
			DEBUG("FLL: bp000: free_list(no->l) >>>>");
			free_list(no->l);
			DEBUG("FLL: bp000-1: <<<<");
		}
		no1 = no->next;
		free((void *)no);
		no = no1;
	}
	return;
}

struct node_list *
tree2treelist (struct node *no) {
	struct node_list *neww = (struct node_list *)malloc(sizeof(struct node_list));
	struct node_list *tl = NULL;
	DEBUG("BP000000: t2tl() >>>>>>");
	switch(no->op) {
	case _TERM_:
		DEBUG("op = _TERM_: no->id = %d no->n = %d\n",no->id,no->n);
		neww->op = _TERM_;
		neww->left =
		neww->right = NULL;
		neww->l  = (struct list *)malloc(sizeof(struct list));
		neww->l->id = no->id;
		neww->l->n  = no->n;
		if (no->dec) {
			DEBUG(" bp000: no->dec != 0 tl2ll( ... t2tl( no->dec )) >>>> ");
			neww->l->dec = treelist2listlist(NULL, tl = tree2treelist(no->dec), 1);
			if (tl)
				free_node_list(tl);
			DEBUG(" bp000-1: <<< ");
		}
		else {
			DEBUG(" bp001: no->dec = 0");
			neww->l->dec = NULL;
		}
		neww->l->prev =
		neww->l->next = NULL;
		break;
	case _MUL_:
		neww->op = _TERM_;
		neww->left =
		neww->right = NULL;
		DEBUG(" b002: op = * ; neww->l = cat t2l(left) t2l(right) >>>> ");
		neww->l = cat( tree2list(NULL,no->left),1, tree2list(NULL,no->right),1 );
		DEBUG(" bp002-1: <<<<");
		break;
	/*
	case _PRJ_:
		neww->op = _TERM_;
		neww->left =
		neww->right = NULL;
		neww->l = cat( tree2list(NULL,no->left),-1, tree2list(NULL,no->right),1 );
		break;
	*/
	case _PLUS_:
		neww->op = _PLUS_;
		DEBUG(" bp003: neww->l,r = t2tl(left), t2tl(right) >>>>> ");
		neww->left  = tree2treelist(no->left);
		neww->right = tree2treelist(no->right);
		DEBUG(" bp003-1: <<<<< ");
		break;
	case _MINUS_:
		neww->op = _MINUS_;
		neww->left  = tree2treelist(no->left);
		neww->right = tree2treelist(no->right);
		break;
	default:
		fprintf(stdin,"ERROR: unknown no->op\n");
		exit(1);
		break;
	}
	DEBUG("BP000000-1: <<<<<<<<<<,");
	return neww;
}

void
print_list(struct list *l) {
	struct list *no = l;
	if (!l) {
		/**/printf(" null_list ");/**/
		return;
	}
	do {
		printf("%d*%s ",no->n, id2s_P(no->id));
		if (no->dec) {
			printf("< ");
			print_listlist(no->dec);
			printf(" > ");
		}
	} while ((no = no->next));
	return;
}
void
_qn_list(int print,
	 struct list *l,
	 int *bar, int *lep, int *S, int *C, int *B, int *T,
	 double *m, double *I3, double *Q
	) {
	struct list *no = l;
	struct type_P *p = 0;
	int bar_dec, lep_dec, S_dec, C_dec, B_dec, T_dec,
	    dbar, dlep ,dS, dC,  dB   ,dT;
	double m_dec, I3_dec, Q_dec,
	       dm, dI3, dQ;
	int anti;
	int X_found = 0, Q_undef = 0;
	*bar =  *lep =  *S = *C = *B = *T = 0;
	*m = *I3 = *Q = 0.;
	if (!l) {
		/*printf(" null ");*/
		return;
	}
	do {
		if (print) printf("%d*%s ",no->n, id2s_P(no->id));
		if (!(p = id2t_P(no->id))) {
			WARNING("_qn_list(): skip broken prt(id = %d)\n",no->id);
			dbar = dlep = dS = dC = dB = dT = 0;
			dm = dI3 = dQ = 0.;
		}
		else {
			if (print) printf(" { bar:%d lep:%d S:%d C:%d B:%d T:%d is-anti:%d hnuc:%d class:%d m:%lf I3:%lf Q:%lf } ",
				p->bar,p->lep,p->S,p->C,p->B,p->T,p->anti, p->heavy_nuc, p->classname,
				p->m,p->I3,p->Q
		      	);
			if (no->id == 0)
				X_found = 1;
			else if (p->classname == 79) /*FIXME: neutral, hadron0, charged+- */
				Q_undef = 1;
			else if (p->Q == -9999)
				Q_undef = 1;
			anti = p->anti != 0 ? p->anti : 1;
			dbar = p->bar * anti;
			dlep = p->lep * anti;
			dS   = p->S   * anti;
			dC   = p->C   * anti;
			dB   = p->B   * anti;
			dT   = p->T   * anti;
			dm   = no->id == 0 ? 0. : p->m  ;
			dI3  = p->I3  * anti; /*FIXME!!!!*/
			dQ   = p->Q  ;
		}
		if (no->dec) {
			if (print) printf("< ");
			_qn_listlist(print,
				     no->dec,
				     &bar_dec,&lep_dec,&S_dec,&C_dec,&B_dec,&T_dec,
				     &m_dec, &I3_dec, &Q_dec
				    );
			if (print) printf(" > ");
			if (bar_dec != -9999 && dbar != bar_dec) dbar = bar_dec;
			if (lep_dec != -9999 && dlep != lep_dec) dlep = lep_dec;
			if (S_dec   != -9999 && dS   != S_dec  ) dS   = S_dec;
			if (C_dec   != -9999 && dC   != C_dec  ) dC   = C_dec;
			if (B_dec   != -9999 && dB   != B_dec  ) dB   = B_dec;
			if (T_dec   != -9999 && dT   != T_dec  ) dT   = T_dec;
			if (I3_dec  != -9999 && dI3  != I3_dec ) dI3  = I3_dec;
			if (Q_dec   != -9999 && dQ   != Q_dec  ) dQ   = Q_dec;
			if (m_dec   != -9999 && m_dec > dm     ) dm   = m_dec;
		}
		*bar += dbar * no->n;
		*lep += dlep * no->n;
		*S   += dS   * no->n;
		*C   += dC   * no->n;
		*B   += dB   * no->n;
		*T   += dT   * no->n;
		*I3  += dI3  * no->n;
		*Q   += dQ   * no->n;
		*m   += dm   * no->n;
	} while ((no = no->next));
	if (X_found || *bar < -9999) *bar = -9999;
	if (X_found || *lep < -9999) *lep = -9999;
	if (X_found || *S   < -9999) *S   = -9999;
	if (X_found || *C   < -9999) *C   = -9999;
	if (X_found || *B   < -9999) *B   = -9999;
	if (X_found || *T   < -9999) *T   = -9999;
	if (X_found || *I3  < -9999) *I3  = -9999;
	if (X_found || Q_undef || *Q   < -9999) *Q   = -9999;
	if (*m   < -9999) *m   = -9999;
	if (print) printf(" {{ bar:%d lep:%d S:%d C:%d B:%d T:%d  m:%lf I3:%lf Q:%lf }} ",
			*bar,*lep,*S,*C,*B,*T,
			*m,*I3,*Q
        );
	return;
}

void
print_treelist(struct node_list *no) {
	switch(no->op) {
	case _TERM_:
		print_list(no->l);
		break;
	case _PLUS_:
	case _MINUS_:
		printf(no->op == _PLUS_ ? " + [ " : " - [ ");
		print_treelist(no->left);
		printf (" , ");
		print_treelist(no->right);
		printf (" ] ");
		break;
	default:
		fprintf(stdin,"ERROR:print_treelist(): wrong no->op = %d\n",(int)no->op);
		exit(1);
		break;
	}
}

/******/

struct listlist *
last_listlist(struct listlist *l) {
	struct listlist *no = l;
	while (no->next) {
		no = no->next;
	}
	return no;
}

struct listlist *
cat_listlist( struct listlist *l1, struct listlist *l2 ) {
	struct listlist *l1_last = last_listlist(l1);
	l1_last->next = l2;
	l2->prev = l1_last;
	return l1;
}

struct listlist *
treelist2listlist (struct listlist *prev, struct node_list *no, int n) {
	struct listlist *neww = NULL;
	/*printf(" T2LL{ ");*/
	switch(no->op) {
	case _TERM_:
		/*printf(" dbg: _term_: n: %d list( ",n); print_list(no->l); printf(" ) ");*/
		neww = (struct listlist *) malloc(sizeof(struct listlist));
		neww->l = no->l;
		neww->n = n;
		if (prev) {
			prev->next = neww;
		}
		neww->prev = prev;
		neww->next = NULL;
		break;
	case _PLUS_:
		/*printf(" dbg: plus_cat< ");*/
		neww = cat_listlist( treelist2listlist(NULL,no->left,n),
				     treelist2listlist(NULL,no->right,n)
				   );
		/*printf( " > ");*/
		if (prev) {
			prev->next = neww;
		}
		neww->prev = prev;
		break;
	case _MINUS_:
		/*printf(" dbg: minus_cat< ");*/
		neww = cat_listlist( treelist2listlist(NULL,no->left,n),
				     treelist2listlist(NULL,no->right,-n)
				   );
		/*printf(" > ");*/
		if (prev) {
			prev->next = neww;
		}
		neww->prev = prev;
		break;
	default:
		fprintf(stdin,"ERROR: treelist2listlist(): wrong no->op: %d\n",no->op);
		exit(1);
		break;
	}
	/*printf(" ret neww: n: %d } ",neww->n);*/
	return neww;
}

struct listlist *
contract_listlist(struct listlist *l,  int (*eq_prt )(int,int) ) {
	struct listlist *no = l, *no1, *no2, *no_first = l;
	if (_DBG_) {
		printf("CLL(): bp000: contrast_listlist() ... >>>>>\n");
		print_listlist(l);
	}
	if (l == NULL) {
		DEBUG("CLL(0): bp000-1: <<<<");
		return l;
	}
	do {
		if(_DBG_) {
			printf("CLL: bp001: no->l = contract_list(no->l) >>>>\n");
			print_list(no->l);
		}
		no->l = contract_list(no->l, eq_prt);
		if(_DBG_) {
			print_list(no->l);
			printf("CLL: bp001-1: <<<<<<\n");
		}
	} while ((no = no->next));
	DEBUG("CLL(): bp000.1: all lists contracted ------- ");
	no = no_first = l;
	do {
		for (no1 = no->next; no1;) {
			DEBUG("CLL: if eq_list ... ");
			if (eq_list(no->l,no1->l)) {
				DEBUG("CLL: bp002: no->l .eq. no1->l: no->n += %d >>>>\n",no->n);
				no->n += no1->n;
				no1->prev->next = no1->next;
				if ((no2 = no1->next)) {
				  	no2->prev = no1->prev;
				}
				DEBUG("CLL: bp0021: free_listlist_el(no1) >>>>>");
				free_list(no1->l);
				free((void *)no1);
				DEBUG("CLL: bp0021-1: <<<<< ");
				no1 = no2;
			}
			else {
				DEBUG("CLL: bp003: >>> <<<");
				no1 = no1->next;
			}
		}
		if (no->n == 0) {
			DEBUG("CLL: bp004: no->n == 0 >>>>");
			if (no->prev) {
				no->prev->next = no->next;
			}
			else {
				no_first = no->next;
			}
			if ((no2 = no->next)) {
				no2->prev = no->prev;
			}
			DEBUG("CLL: bp0041: free_listlist_el(no) >>>>>");
			free_list(no->l);
			free((void *)no);
			DEBUG("CLL: bp0041-1 <<<<< ");
			no = no2;
		}
		else {
			no = no->next;
		}
	} while (no);
	DEBUG("CLL: bp000-1: <<<<<<<<<");
	return no_first;
}

void
print_listlist (struct listlist *l) {
	struct listlist *no = l;
	if (!no) {
		printf(" [NULL] ");
		return;
	}
	do {
		printf(" (%d)*[ ",no->n);
		print_list(no->l);
		printf(" ]");
	} while ((no = no->next));
	return;
}

void
_qn_listlist (int print,
	      struct listlist *l,
	      int *bar, int *lep, int *S, int *C, int *B, int *T,
	      double *m, double *I3, double *Q
	     ) {
	struct listlist *no = l;
	int bar_p = -9999, lep_p = -9999, S_p = -9999, C_p = -9999, B_p = -9999, T_p = -9999;
	int bar_list = 0, lep_list = 0, S_list = 0, C_list = 0, B_list = 0, T_list = 0;
	double I3_p = -9999, Q_p = -9999;
	double m_list = 0, I3_list = 0, Q_list = 0;
	if (!no) {
		*bar = *lep = *S = *C = *B = *T = 0;
		*m = *I3 = *Q = 0.;
		if (print) printf(" [NULL] ");
		return;
	}
	*bar = *lep = *S = *C = *B = *T = -9999;
	*m = *I3 = *Q = -9999.;
	DEBUG("_qn_listlist .... ");
	do {
		DEBUG("bp000: C_:%d",C_list);
		if (print) printf(" (%d)*[ ",no->n);
		_qn_list(print,
			 no->l,
		   	 &bar_list, &lep_list, &S_list, &C_list, &B_list, &T_list,
		  	 &m_list, &I3_list, &Q_list
			);
		DEBUG("bp001: C_:%d",C_list);
		if (print) printf(" ]");
		if (bar_p != -9999) {
			DEBUG("bp1 C_: %d",C_list);
			if (bar_p != bar_list) {
				*bar = -9999;
				if (print) printf("WARNING: bar(prev list):%d != bar(list):%d\n",bar_p,bar_list);
			}
			if (lep_p != lep_list) {
				*lep = -9999;
				if (print) printf("WARNING: lep(prev list):%d != lep(list):%d\n",lep_p,lep_list);
			}
			if (S_p != S_list)     {
				*S   = -9999;
				if (print) printf("WARNING: S(prev list):%d != S(list):%d\n",S_p,S_list);
			}
			if (C_p != C_list)     {
				*C   = -9999;
				if (print) printf("WARNING: C(prev list):%d != C(list):%d\n",C_p,C_list);
			}
			if (B_p != B_list)     {
				*B   = -9999;
				if (print) printf("WARNING: B(prev list):%d != B(list):%d\n",B_p,B_list);
			}
			if (T_p != T_list)     {
				*T   = -9999;
				if (print) printf("WARNING: T(prev list):%d != T(list):%d\n",T_p,T_list);
			}
			if (I3_p != I3_list)   {
				*I3  = -9999;
				if (print) printf("WARNING: I3(prev list):%lf != I3(list):%lf\n",I3_p,I3_list);
			}
			if (Q_p != Q_list)     {
				*Q   = -9999;
				if (print) printf("WARNING: Q(prev list):%lf != Q(list):%lf\n",Q_p,Q_list);
			}
			DEBUG("bp2 C_: %d",C_list);
		}
		else { /*FIXME: take QN's from the first list: */
			DEBUG("bp3");
			*bar = bar_list; *lep = lep_list; *S = S_list; *C = C_list; *B = B_list; *T = T_list;
			*Q = Q_list; *I3 = I3_list;
		}
		if (m_list > *m)
			*m = m_list;
		bar_p = bar_list; lep_p = lep_list; S_p = S_list; C_p = C_list; B_p = B_list; T_p = T_list;
		I3_p = I3_list; Q_p = Q_list;
		DEBUG("C(all)_:%d",C_list);
	} while ((no = no->next));
	if (*m < 0)
		*m = -9999.;
	if (print) printf(" {{{ bar:%d lep:%d S:%d C:%d B:%d T:%d  m:%lf I3:%lf Q:%lf }}} ",
			*bar,*lep,*S,*C,*B,*T,
			*m,*I3,*Q
        );
	return;
}

