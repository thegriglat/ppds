#include<stdlib.h>
#include<stdio.h>
#include<string.h>

#include "re.h"
#include "dg/P.h"
#include "settings.h"

const char *_s_null_ = " [NULL] ";

char* _list2s(struct list* l);

char*
_listlist2s (struct listlist *l)
{
  struct listlist *no = l;
  char *buf = 0, *p = 0, tmpbuf[1024], *newbuf;
  int lbuf, shift;
  if (!no) {
    buf = (char*)malloc(sizeof(char)*(strlen(_s_null_)+1));
    strcpy(buf,_s_null_);
    return buf;
  }
  p = buf = (char*)malloc(sizeof(char));
  *p = '\0';
  shift = 0;
  do {
    DEBUG(": (%d)*[ ...",no->n);
    sprintf(tmpbuf," (%d)*[ ",no->n);
    lbuf = strlen(tmpbuf) + strlen(buf); DEBUG("lbuf: %d",lbuf);
    buf = (char*)realloc(buf,sizeof(char)*(lbuf+1));
    p = buf + shift;
    strcpy(p,tmpbuf);
    p = buf + lbuf;
    shift = lbuf;
    DEBUG(": buf = _list2s(...)");
    newbuf = _list2s(no->l);
    lbuf = strlen(newbuf) + strlen(buf); DEBUG("lbuf: %d (%d + %d)",lbuf,(int)strlen(newbuf),(int)strlen(buf));
    buf = (char*)realloc(buf,sizeof(char)*(lbuf+1));
    p = buf + shift;
    strcpy(p,newbuf);
    free((void*)newbuf);
    p = buf + lbuf;
    shift = lbuf;
    DEBUG(": buf:%s: ... ]\n",buf);
    sprintf(tmpbuf," ]");
    lbuf = strlen(tmpbuf) + strlen(buf);DEBUG("lbuf: %d",lbuf);
    buf = (char*)realloc(buf,sizeof(char)*(lbuf+1));
    p = buf + shift;
    strcpy(p,tmpbuf);
    p = buf + lbuf;
    shift = lbuf;
    DEBUG(" <... buf:%s:",buf);
  } while ((no = no->next));
  DEBUG(" :: return buf:%s:",buf);
  return buf;
}

char*
_list2s(struct list *l)
{
  struct list *no = l;
  char *buf = 0, *p = 0, tmpbuf[1024], *newbuf;
  int lbuf, shift;
  if (!no) {
    buf = (char*)malloc(sizeof(char)*(strlen(_s_null_)+1));
    strcpy(buf,_s_null_);
    return buf;
  }
  p = buf = (char*)malloc(sizeof(char));DEBUG("p:%p: buf:%p:",(void*)&p, (void*)&buf);
  *p = '\0';
  shift = 0;
  do {
    DEBUG(":bp00 buf:%s:",buf);
    DEBUG(":bp01 n:%d s(no->id):%s buf:%s:",no->n,id2s_P(no->id),buf);
    sprintf(tmpbuf,"%d*%s ",no->n,id2s_P(no->id));DEBUG("p:%p: buf:%p:",(void*)&p,(void*)&buf);
    DEBUG("tmpbuf:%s:",tmpbuf);
    lbuf = strlen(tmpbuf) + strlen(buf);DEBUG("p:%p: buf:%p:",(void*)&p, (void*)&buf);
    DEBUG("lbuf: %d (%d+%d)",lbuf,(int)strlen(tmpbuf),(int)strlen(buf));
    buf = (char*)realloc(buf,sizeof(char)*(lbuf+1));DEBUG("realloc -> p:%p: buf:%p:",(void*)&p, (void*)&buf);
    p = buf + shift;
    strcpy(p,tmpbuf); DEBUG("p:%p: buf:%p: *p:%c: buf:%s: tmpbuf:%s:",(void*)&p,(void*)&buf,*p,buf,tmpbuf);
    p = buf + lbuf;
    shift = lbuf;
    if (no->dec) {
      lbuf = 2 + strlen(buf);DEBUG("lbuf: %d",lbuf);
      buf = (char*)realloc(buf,sizeof(char)*(lbuf+1));DEBUG("lbuf: %d",lbuf);
      p = buf + shift;
      strcpy(p,"< ");
      p = buf + lbuf;
      shift = lbuf;
      DEBUG("buf:%s:", buf);
      newbuf = _listlist2s(no->dec);
      lbuf = strlen(newbuf) + strlen(buf);DEBUG("lbuf: %d",lbuf);
      buf = (char*)realloc(buf,sizeof(char)*(lbuf+1));
      p = buf + shift;
      strcpy(p,newbuf);
      free((void*)newbuf);
      p = buf + lbuf;
      shift = lbuf;
      DEBUG("buf:%s:", buf);
      lbuf = 3 + strlen(buf);DEBUG("lbuf: %d",lbuf);
      buf = (char*)realloc(buf,sizeof(char)*(lbuf+1));
      p = buf + shift;
      strcpy(p," > ");
      p = buf + lbuf;
      shift = lbuf;
    }
  } while ((no = no->next));
  DEBUG(":: return buf:%s:\n",buf);
  return buf;
}


