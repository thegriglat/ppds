#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "re.h"
#include "y.h" /* ../include/ */
#include "dg/RE.h"

#include "settings.h"


extern
void
vy_init_P(); /* ../dg/P/f.c */

int
main (int argc, char** argv) {
	char *s;
	int r, l;
	struct listlist *is, *fs;
	int bar, lep, S,C,B,T;
	double m, I3, Q;
	vy_init_P();
	if (argc < 2) {
		fprintf(stderr,"Usage: re_hash \"a b --> с d x\"\n");
		return 1;
	}
	if ((l = strlen(argv[1])) == 0) {
		ERROR("empty reaction.");
		return 1;
	}
	s = (char*)malloc(sizeof(char)*(l+2));
	strcpy(s,argv[1]);
	if (s[l-1] != ';') {
		s[l] = ';';
		s[l+1] = '\0';
	}
	DEBUG("call s2listlist(s:%s: ...)\n",s);
	r = s2listlist(s, &is,&fs);
	/*printf("s2listlist(): ret: %d\n",r);*/
	if (_DBG_) {
		if (is) {
			DEBUG("\n\n >>>>> IS >>>>>>> \n\n");
			print_listlist(is);
			DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
		if (fs) {
			DEBUG("\n\n >>>>> FS >>>>>>> \n\n");
			print_listlist(fs);
			DEBUG("\n\n <<<<<<<<<<<<<<<< \n\n");
		}
	}
	if (r > 0) {
		ERROR("%s: errors in reaction: %s",argv[0],s);
		return 1;
	}
	if (is) {
		printf("\n");
		_qn_listlist(1,
			is,
			&bar,&lep,&S,&C,&B,&T,
			&m,&I3,&Q
			);
		printf("\n");
	}
	if (fs) {
		printf("\n");
		_qn_listlist(1,
			fs,
			&bar,&lep,&S,&C,&B,&T,
			&m,&I3,&Q
			);
		printf("\n");
	}
	printf("\n");
	return 0;
}
