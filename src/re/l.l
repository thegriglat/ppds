%{
#include <stdio.h>
#include <stdlib.h>

#include "y.tab.h"

#include "v.h"

#include "settings.h"

%}

%%
[ \t]+\-\-\>[ \t]+ {
	DEBUG("lex: arr:");
	return ARROW;
}

[A-Za-z][A-Za-z0-9\.=_\*\+\-\(\)\/]*|\([A-Za-z][A-Za-z0-9\.=_\*\+\-\(\)\/]*\) {
	DEBUG("lex: prt: %s -> %d\n",ppdsretext, prt_id(ppdsretext) );
	ppdsrelval.id = prt_id(ppdsretext);
	return PRT;
}

[0-9]+ {
	DEBUG("lex: num: %d\n", atoi(ppdsretext));
	ppdsrelval.id = atoi(ppdsretext);
	return NUM;
}

^[ \t]+        { DEBUG("l:^[ ]\n"); }

[ \t]*\;       { DEBUG("l:;\n"); return ';'; }

[ \t]+\+[ \t]+ { DEBUG("l:+\n"); return '+'; }

[ \t]+\-[ \t]+ { DEBUG("l:-\n"); return '-'; }

\([ \t]+       { return '('; }

[ \t]+\)       { return ')'; }

[ \t]+\<[ \t]+ { DEBUG("l:<\n"); return '<'; }

[ \t]+\>       { DEBUG("l:>\n"); return '>'; }

[ \t]+\.[ \t]+ { return '.'; }

[ \t]+         { DEBUG("l:*\n"); return '*'; }

.              { fprintf(stdin,"ERROR: re/l.l: Invalid char:%c:\n",*ppdsretext); }



%%
int ppdsrewrap(void) {
	return 1;
}


/*
int  main(void) {
	ppdsrelex();
	return 0;
}
*/
