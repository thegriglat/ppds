%{
#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include <math.h>

#include "dg/tree.h"

#include "settings.h"

#include "extra_keys.h"

/* lex stuff: */

typedef struct quebuffer_state *QUE_BUFFER_STATE;

extern
QUE_BUFFER_STATE
que_scan_string (const char*);

extern
void
que_delete_buffer (QUE_BUFFER_STATE);

extern
int
yylex();

/***/




void
yyerror(const char*);

enum que_node_type { _Q_TERM_, _Q_AND_, _Q_OR_, _Q_JOIN_ };
enum optype        { _Q_EQ_, _Q_X_ };

struct que_node {
	enum que_node_type type;
	enum keytype key;
	enum optype   op;
	int not;
	char *val;
	struct que_node *left, *right;
};

struct que_node* que_no_cur = 0;

struct que_node*
new_que_node(enum que_node_type type, enum keytype key, enum optype op, char *val, struct que_node* left, struct que_node* right) {
	struct que_node *no = (struct que_node*)malloc(sizeof(struct que_node));
	DEBUG("new_que_node(%d,%d, \"%s\", ....)",type,key,val);
	switch(type) {
		case _Q_TERM_:
			no->type = type;
			no->key  = key;
			no->op   = op;
			no->not  = 0;
			no->val = (char*)malloc(sizeof(char)*(strlen(val)+1));
			strcpy(no->val,val);
			no->left = no->right = 0;
			break;
		case _Q_JOIN_:
		case _Q_AND_:
		case _Q_OR_:
			no->type = type;
			no->not = 0;
			no->val = 0;
			no->left = left;
			no->right = right;
			break;
		default:
			ERROR("q.y: unknown node type: %d",type);
			exit(1);
	}
	return no;
}

void
print_que_tree (struct que_node* no, int ident) {
	int i;
	char *s;
	if (no->not) {
		for (i = 0; i < ident; i++)
			putchar(' ');
		printf("NOT\n");
	}
	for (i = 0; i < ident; i++)
		putchar(' ');
	switch(no->type) {
		case _Q_OR_ :
		case _Q_AND_:
		case _Q_JOIN_:
			printf( no->type == _Q_AND_ ? "AND\n" : ( no->type == _Q_OR_ ? "OR\n" : "JOIN\n" ));
			print_que_tree(no->left, ident + 2);
			/*printf(" , ");*/
			print_que_tree(no->right, ident + 2);
			/*printf(" ) ");*/
			break;
		case _Q_TERM_:
			s = k2s(no->key);
			printf("%s %c \"%s\"\n",
				(s ? s : ( (int)no->key == FS ? "FS" : ( (int)no->key == RES ? "RES" : "IS" ) ) ) ,
				( no->op == _Q_EQ_ ? '=' : '@' ),
				no->val
			);
			break;
		default:
			printf(" #ERROR: no->type: %d # ", no->type);
			break;
	}
	return;
}

extern
char
quebuf[65536];

extern
enum { _in_val_, _out_ } quest;

%}

%token KEY NOT OR AND JOIN VAL END
%left OR
%left AND
%left NOT
%left JOIN

%union r_union_type {
        int id;
        struct que_node *no;
};

%%

input:
     expr END { DEBUG("print_tree ..."); }
     |
     ;

expr:
    KEY '=' VAL   { DEBUG("no = key=val"); que_no_cur = $<no>$ = new_que_node(_Q_TERM_, $<id>1, _Q_EQ_ , quebuf, NULL,NULL); }
    |
    KEY '@' VAL   { DEBUG("no = key~val"); que_no_cur = $<no>$ = new_que_node(_Q_TERM_, $<id>1, _Q_X_ , quebuf, NULL,NULL); }
    |
    NOT expr      { DEBUG("no -> !no"); (que_no_cur = $<no>$ = $<no>2)->not = ! $<no>2->not; }
    |
    expr JOIN expr { DEBUG("no = join(e,e)"); que_no_cur = $<no>$ = new_que_node(_Q_JOIN_,0,0,0, $<no>1,$<no>3); }
    |
    expr AND expr { DEBUG("no = and (e,e)"); que_no_cur = $<no>$ = new_que_node(_Q_AND_,0,0,0, $<no>1,$<no>3); }
    |
    expr OR  expr { DEBUG("no = or(e,e)"); que_no_cur = $<no>$ = new_que_node(_Q_OR_ ,0,0,0, $<no>1,$<no>3); }
    |
    '(' expr ')'  { DEBUG("e = ( e )"); que_no_cur = $<no>$ = $<no>2; }
    ;
%%

void
queerror(const char* s) {
	fprintf(stdin,"ERROR: QUE: %s\n",s);
}

int
Q_search(char* query) {
	QUE_BUFFER_STATE buffer;
	int ret;
	char *s = 0, *p, *ps;
	/*
	char stmp[1024];
	printf ("in> ");
	if (!fgets(stmp,1024,stdin))
		return 1;
	printf("in: %s\n",stmp);
	*/
	s = (char*)malloc(sizeof(char)*(strlen(query)+1));
	for (p = query, ps = s; (*ps = ( *p >= 'a' && *p <= 'z' ? 'A' + *p - 'a' : *p )); p++, ps++);
	quest = _out_;
	buffer = que_scan_string(s);
        if ((ret = queparse()) != 0) {
		fprintf(stderr,"ERROR: syntax error in the query: %s\n",s);
		return 1;
	}
	if (que_no_cur)
		print_que_tree(que_no_cur, 0);
	que_delete_buffer(buffer);
	free(s);
	return 0;
}

