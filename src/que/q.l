%{
#include <stdio.h>
#include <stdlib.h>

/*#include "y.tab.h"*/

#include "dg/tree.h"

#include "settings.h"

#include "extra_keys.h"

#include "y.tab.h"

char quebuf[65536], *p, *p_text;

enum { _in_val_, _out_ } quest;

%}

%%

RES {
	if (quest == _out_) {
/**/		
		quelval.id = RES;
		DEBUG("lex: key: RES: %s -> %d ret KEY",quetext, quelval.id);
		return KEY;
/**/
	}
	else {
		for (p_text = quetext; *p_text; p++, p_text++) {
			*p = *p_text;
			DEBUG("*p++ = %c",*p);
		}
	}
}

FS|IS {
	if (quest == _out_) {
/**/
		quelval.id = (strcmp("FS",quetext) == 0 ? FS : IS);
		DEBUG("lex: key: %s -> %d ret KEY",quetext, quelval.id);
		return KEY;
/**/
	}
	else {
		for (p_text = quetext; *p_text; p++, p_text++) {
			*p = *p_text;
			DEBUG("*p++ = %c",*p);
		}
	}
}

SC|PDGSC|IRN|IRNW|R|COD|TY|D|KTO|KRD|CONF|AUTHORS|A|SPOKESMAN|SPP|I|ILBL|NA|T|TW|TITLE-TEX|TA|TA-TEX|ABS|ABS-TEX|DK|L|CIT|CCOD|CB|CB-TEX|URL|PPF|SB|AV|COMP|ES|EXPERIMENT|AC|DE|PR|COL|RR|RCOD|PUBLISHED-PAPERS|PL|CE|CE-TEX|CD|CD-TEX|REAC-DATA|RE|FSP|BP|TP|B|PLAB|ECM|Q2|W2|NU|DD|INR|PART-PROP|P|PP|INP|CR|CR-TEX|CRW|INDEX|RD|CS|RPP|DEN|DCH|ACTION|QUESTION|ANSWER|ERROR {
	if (quest == _out_) {
		DEBUG("lex: key: %s -> %d ret KEY",quetext, s2k(quetext) );
/**/
		quelval.id = s2k(quetext);
		return KEY;
/**/
	}
	else {
		for (p_text = quetext; *p_text; p++, p_text++) {
			*p = *p_text;
			DEBUG("*p++ = %c",*p);
		}
	}
}

= {
	if (quest == _in_val_) {
		DEBUG("*p++ = %c",*quetext);
		*p ++ = *quetext;
	}
	else {
		DEBUG("lex: return '='");
		quest = _in_val_;
		p = quebuf;
/**/
		return '=' ;
/**/
	}
}

~~ {
	if (quest == _in_val_) {
		for (p_text = quetext; *p_text; p++, p_text++) {
			*p = *p_text;
			DEBUG("*p++ = %c",*p);
		}
	}
	else {
		DEBUG("lex: return '='");
		quest = _in_val_;
		p = quebuf;
/**/
		return '@' ;
/**/
	}
}

NOT|not {
	if (quest == _in_val_) {
		for (p_text = quetext; *p_text; p++, p_text++) {
			*p = *p_text;
			DEBUG("*p++ = %c",*p);
		}
	}
	else {
		DEBUG("lex: return NOT:");
/**/
		return NOT;
/**/
	}
}

OR|or {
	if (quest == _in_val_) {
		for (p_text = quetext; *p_text; p++, p_text++) {
			*p = *p_text;
			DEBUG("*p++ = %c",*p);
		}
	}
	else {
		DEBUG("lex: return OR:");
/**/
		return OR;
/**/
	}
}

AND|and {
	if (quest == _in_val_) {
		for (p_text = quetext; *p_text; p++, p_text++) {
			*p = *p_text;
			DEBUG("*p++ = %c",*p);
		}
	}
	else {
		DEBUG("lex: return AND:");
/**/
		return AND;
/**/
	}
}

\& {
	if (quest == _in_val_) {
		for (p_text = quetext; *p_text; p++, p_text++) {
			*p = *p_text;
			DEBUG("*p++ = %c",*p);
		}
	}
	else {
		DEBUG("lex: return JOIN:");
/**/
		return JOIN;
/**/
	}
}


\; {
	if (quest == _in_val_) {
		*p = '\0';
		DEBUG("lex: ret VAL:%s:",quebuf);
		quest = _out_;
		p = quebuf;
/**/
		return VAL;
/**/
	}
	else {
		DEBUG("*p++ = %c",*quetext);
		if (!p)
			p = quebuf;
		*p ++ = *quetext;
	}
}


\( {
	if (quest == _out_) {
		DEBUG("lex: return '('");
/**/
		return '(';
/**/
	}
	else {
		DEBUG("*p++ = %c",*quetext);
		*p ++ = *quetext;
	}
}

\) {
	if (quest == _out_) {
		DEBUG("lex: return ')'");
/**/
		return ')';
/**/
	}
	else {
		DEBUG("*p++ = %c :%s:",*quetext,quetext);
		*p ++ = *quetext;
	}
}

[ \t] {
	if (quest == _in_val_) {
		DEBUG("*p++ = %c",*quetext);
		*p ++ = *quetext;
	}
	else {
		DEBUG("skip ' '");
	}
}

. {
	if (quest == _in_val_) {
		DEBUG("*p++ = %c (quetext:%s:)",*quetext,quetext);
		*p ++ = *quetext;
	}
	else {
		ERROR("q.l: spurios '%c' on input",*quetext);
	}
}

## {
	return END;
}


%%
int quewrap(void) {
	return 1;
}


/**
int  main(void) {
	quest = _out_;
	quelex();
	return 0;
}
**/
