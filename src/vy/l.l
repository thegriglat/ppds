%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "settings.h"

#include "sqlite.h"

#include "vy/tree.h"

#include "vy/P.h"

int n_P;

char buf[2048], *p, *py;

enum { _val_, _inkey_, _ex_, _er_ } st;

int mult_val;

enum keytype key;

struct {
	char *s;
	enum keytype key, par;
} keys[] = {
{"ABBREVIATION",_ABBREVIATION_, _RECORD_},
{"AS"          ,_AS_          , _RECORD_},
{"AT"          ,_AT_          , _RECORD_},
{"NAME"        ,_NAME_        , _RECORD_},
{"USAGE"       ,_USAGE_       , _RECORD_},
{"COMMENT"     ,_COMMENT_     , _RECORD_},
{"COMMENT-TEX" ,_COMMENT_TEX_ , _RECORD_},
{"DESCRIPTION" ,_DESCRIPTION_ , _RECORD_},
{"ORCODE"      ,_ORCODE_      , _RECORD_},
{"QN_Q"        ,_QN_Q_        , _RECORD_},
{"QN_BAR"      ,_QN_BAR_        , _RECORD_},
{"IDENT"       ,_IDENT_       , _RECORD_},
{"Q"           ,_Q_           , _RECORD_},
{"MASS"        ,_MASS_        , _RECORD_},
{"RC"          ,_RC_          , _RECORD_},
{"RPP"         ,_RPP_         , _RECORD_},
{"CRPP"        ,_CRPP_        , _RECORD_},
{"PN"          ,_PN_          , _RECORD_},
{"SB"          ,_SB_          , _RECORD_},
{"DEN"         ,_DEN_         , _RECORD_},
{"DCH"         ,_DCH_         , _RECORD_},
{"QUESTION"    ,_QUESTION_    , _RECORD_},
{"URL"         ,_URL_         , _RECORD_},
{"RECORD"    ,_RECORD_    , _UNKNOWN_}
};


struct node
*no_root, *no_cur, *no_par;

struct node*
new_node(enum keytype type, struct node *par) {
	struct node *no = (struct node*)malloc(sizeof(struct node));
	no->type = type;
	no->par = par;
	no->ch = NULL;
	no->nch = 0;
	no->val = NULL;
	no->nval = 0;
	if (par) {
		par->ch = (struct node**)realloc(par->ch, sizeof(struct node*) * (++ par->nch));
		par->ch[ par->nch - 1 ] = no;
	}
	return no;
}

void
val_node(struct node* no, char* val) {
	if (!no) {
		ERROR("val_node(NULL, %s) called. EXIT", val);
		exit(1);
	}
	no->val = (char**)realloc(no->val, sizeof(char*) * (no->nval + 1));
	if (val)
		while (*val == ' ' || *val == '\t' || *val == '\n')
			val++;
	no->val[ no->nval ] = (char*)malloc(sizeof(char) * (strlen(val) + 1));
	strcpy(no->val[ no->nval++ ], val );
	return;
}

void
free_tree (struct node* no) {
	if (!no)
		return;
	if (no->val) {
		for (; no->nval > 0; no->nval--)
			free((void*)no->val[ no->nval - 1 ]);
		free((void*)no->val);
	}
	if (no->ch) {
		for (; no->nch > 0; no->nch --)
			free_tree(no->ch[ no->nch - 1 ]);
		free((void*)no->ch);
	}
	free((void*)no);
	return;
}

char*
k2s(enum keytype k) { /*FIXME*/
	int ik;
	for (ik = 0; keys[ik].s; ik++) {
		if (keys[ik].key == k) {
			return keys[ik].s;
			break;
		}
	}
	return 0;
}

void
print_tree (struct node *no, int ident) {
	int i;
	for (i = 0; i < ident; i++)
		putchar(' ');
	if (!no) {
		printf("[NULL]\n");
		return;
	}
	printf("%s = ", k2s(no->type));
	for (i = 0; i < no->nval; i++)
		printf("%s; ", no->val[i]);
	printf("\n");
	for (i = 0; i < no->nch; i++)
		print_tree(no->ch[i], ident+1);
	return;
}

int db_get_next_table_id (const char* prefix){
	char table_name[SQL_MAX_LEN];
	sprintf(table_name, "vy_%s", prefix);
	return db_get_last_id(table_name, 0) + 1;
}

void dump_tree_ABBR(struct node *no, const char* prefix){
	int i;
	printf("%s: ", prefix);
	const int id = db_get_next_table_id(prefix);
	for (i = 0; i < no->nch; ++i)
		if (no->ch[i]->type == _ABBREVIATION_){
			int j;
			for (j = 0; j < no->ch[i]->nval; ++j){
				printf("%s ", no->ch[i]->val[j]);
				char sql[SQL_MAX_LEN];
				sprintf(sql, "INSERT into vy_%s values (?, ?);", prefix);
				const char *abbr = no->ch[i]->val[j];
				const void * params[] = {(void*)&id, (void*)abbr};
				const enum SQLITE_TYPE types[] = {INTEGER, TEXT};
				int rc = db_exec_noresult(
					sql,
					2,
					params,
					types
					);
				if (rc != SQLITE_DONE){
					ERROR("SQLITE ERROR!");
					db_close();
					exit(1);
				}
			}
			break;
		}
	printf("\n");
}

void dump_tree_DE(struct node *no, const char* prefix){
	int i;
	printf("%s: ", prefix);
	int abbr_i = 0;
	int ident_i = 0;
	const int id = db_get_next_table_id(prefix);
	for (i = 0; i < no->nch; ++i){
		if (no->ch[i]->type == _ABBREVIATION_){
			int j;
			abbr_i = i;
			for (j = 0; j < no->ch[i]->nval; ++j){
				printf("%s", no->ch[i]->val[j]);
				if (j != no->ch[i]->nval - 1)
					printf(",");
			}
		}
		else if (no->ch[i]->type == _IDENT_){
			printf("|%s", no->ch[i]->val[0]);
			ident_i = i;
		}
	}
	int q;
	DEBUG("nabbr = %d", no->ch[abbr_i]->nval);
	for (q = 0; q < no->ch[abbr_i]->nval; ++q){
		char sql[SQL_MAX_LEN];
		sprintf(sql, "INSERT into vy_%s values (?, ?, ?);", prefix);
		const char *abbr = no->ch[abbr_i]->val[q];
		const char *ident = no->ch[ident_i]->val[0];
		const void * params[] = {(void*)&id, (void*)abbr, (void*)ident};
		const enum SQLITE_TYPE types[] = {INTEGER, TEXT, TEXT};
		int rc = db_exec_noresult(
			sql,
			3,
			params,
			types
			);
		if (rc != SQLITE_DONE){
			ERROR("SQLITE ERROR!");
			db_close();
			exit(1);
		};
	}
	printf("\n");
}

void dump_tree_I(struct node *no){
	int i;
	printf("I: ");
	char found = 1;
	int abbr_i = -1, as_i = -1, url_i = -1;
	const int id = db_get_next_table_id("I");
	for (i = 0; i < no->nch; ++i){
		if (no->ch[i]->type == _AS_)
			found |= (1 << 1);
		if (no->ch[i]->type == _URL_)
			found |= (1 << 2);
	}
	printf("%d ", found);
	for (i = 0; i < no->nch; ++i){
		if (no->ch[i]->type == _ABBREVIATION_){
			int j;
			abbr_i = i;
			for (j = 0; j < no->ch[i]->nval; ++j){
				printf("%s", no->ch[i]->val[j]);
				if (j != no->ch[i]->nval - 1)
					printf(",");
			}
		}
		else if (no->ch[i]->type == _AS_){
			printf(" ");
			int j;
			as_i = i;
			for (j = 0; j < no->ch[i]->nval; ++j){
				printf("%s", no->ch[i]->val[j]);
				if (j != no->ch[i]->nval - 1)
					printf(",");
			}
		}
		else if (no->ch[i]->type == _URL_){
			printf(" %s", no->ch[i]->val[0]);
			url_i = i;
		}
	}
	printf("\n");
	int q;
	for (q = 0; q < no->ch[abbr_i]->nval; ++q){
		const char *abbr = no->ch[abbr_i]->val[q];
		const char *url = url_i != -1 ? no->ch[url_i]->val[0] : "";
		if (as_i != -1){
			// _AS_ found
			int w;
			for (w = 0; w < no->ch[as_i]->nval; ++w){
				const char *as = no->ch[as_i]->val[w];
				const void * params[] = {(void*)&id, (void*)abbr, (void*)as, (void*)url};
				const enum SQLITE_TYPE types[] = {INTEGER, TEXT, TEXT, TEXT};
				int rc = db_exec_noresult(
					"INSERT into vy_I values (?, ?, ?, ?);",
					4,
					params,
					types
					);
				if (rc != SQLITE_DONE){
					ERROR("SQLITE ERROR!");
					db_close();
					exit(1);
				};
			}
		} else {
			const char *as = "";
			const void * params[] = {(void*)&id, (void*)abbr, (void*)as, (void*)url};
			const enum SQLITE_TYPE types[] = {INTEGER, TEXT, TEXT, TEXT};
			int rc = db_exec_noresult(
				"INSERT into vy_I values (?, ?, ?, ?);",
				4,
				params,
				types
				);
			if (rc != SQLITE_DONE){
				ERROR("SQLITE ERROR!");
				db_close();
				exit(1);
			};
		}
	}
}

void dump_tree_R(struct node *no){
	int i;
	printf("R: ");
	char found = 1;
	for (i = 0; i < no->nch; ++i){
		if (no->ch[i]->type == _AS_)
			found |= (1 << 1);
		if (no->ch[i]->type == _AT_)
			found |= (1 << 2);
		if (no->ch[i]->type == _IDENT_)
			found |= (1 << 3);
	}
	int abbr_i = -1,
	    as_i = -1,
	    ident_i = -1,
	    at_i = -1;
    const int id = db_get_next_table_id("R");
	printf("%d|", found);
	for (i = 0; i < no->nch; ++i)
		if (no->ch[i]->type == _ABBREVIATION_){
			int j;
			abbr_i = i;
			for (j = 0; j < no->ch[i]->nval; ++j){
				printf("%s", no->ch[i]->val[j]);
				if (j != no->ch[i]->nval - 1)
					printf(",");
			}
		}
		else if (no->ch[i]->type == _AS_){
			printf("|");
			int j;
			as_i = i;
			for (j = 0; j < no->ch[i]->nval; ++j){
				printf("%s", no->ch[i]->val[j]);
				if (j != no->ch[i]->nval - 1)
					printf(",");
			}
		}
		else if (no->ch[i]->type == _IDENT_ ){
			printf("|%s", no->ch[i]->val[0]);
			ident_i = i;
		}
		else if (no->ch[i]->type == _AT_){
			printf("|%s", no->ch[i]->val[0]);
			at_i = i;
		}
	printf("\n");
	for (i = 0; i < no->ch[abbr_i]->nval; ++i){
		const char *abbr = no->ch[abbr_i]->val[i];
		const char *ident = ident_i != -1 ? no->ch[ident_i]->val[0] : "";
		const char *at = at_i != -1 ? no->ch[at_i]->val[0] : "";
		if (as_i != -1){
			// _AS_ found
			int w;
			for (w = 0; w < no->ch[as_i]->nval; ++w){
				const char *as = no->ch[as_i]->val[w];
				const void * params[] = {(void*)&id, (void*)abbr, (void*)ident, (void*)as, (void*)at};
				const enum SQLITE_TYPE types[] = {INTEGER, TEXT, TEXT, TEXT, TEXT};
				int rc = db_exec_noresult(
					"INSERT into vy_R values (?, ?, ?, ?, ?);",
					5,
					params,
					types
					);
				if (rc != SQLITE_DONE){
					ERROR("SQLITE ERROR!");
					db_close();
					exit(1);
				}
			}
		} else {
			char *as = "";
			const void * params[] = {(void*)&id, (void*)abbr, (void*)ident, (void*)as, (void*)at};
			const enum SQLITE_TYPE types[] = {INTEGER, TEXT, TEXT, TEXT, TEXT};
			int rc = db_exec_noresult(
				"INSERT into vy_R values (?, ?, ?, ?, ?);",
				5,
				params,
				types
				);
			if (rc != SQLITE_DONE){
				ERROR("SQLITE ERROR:!");
				db_close();
				exit(1);
			}
		}
	}
}

void dump_tree_sqlite(struct node *no){
	const int id = db_get_last_id("vy_data", 0) + 1;
	int i;
	char inserted = 0;
	for (i = 0; i < no->nch; ++i){
		const char* column_name = k2s(no->ch[i]->type);
		int j;
		char * val_union = NULL;
		for (j = 0; j < no->ch[i]->nval; ++j){
			if (!val_union){
				val_union = (char*)malloc(sizeof(char) * (strlen(no->ch[i]->val[j]) + 1));
				strcpy(val_union, no->ch[i]->val[j]);
			} else {
				char * tmp = realloc(val_union, sizeof(char) * (strlen(val_union) + strlen(no->ch[i]->val[j]) + 1 + 1));
				if (tmp)
					val_union = tmp;
				else {
					ERROR("Cannot allocate memory.");
					exit(0);
				}
				strcat(val_union, "|");
				strcat(val_union, no->ch[i]->val[j]);
			};
		}
		if (!inserted){
				// const char* sql = (char*)malloc(sizeof(char) * 1024);
				char *sql = (char*)malloc(sizeof(char) * (1024 + 1));
				sprintf(sql, "insert into vy_data (\"%s\") values (?);", column_name);
				const void * params[] = {(void*)(val_union)};
				const enum SQLITE_TYPE types[] = {TEXT};
				db_exec_noresult(sql,
					1,
					params,
					types);
				inserted = 1;
			} else {
				char* sql = (char*)malloc(sizeof(char) * (1024 + 1));
				sprintf(sql, "update vy_data set \"%s\" = ? where id = ?;", column_name);
				const void * params[] = {(void*)(val_union), (void*)(&id)};
				const enum SQLITE_TYPE types[] = {TEXT, INTEGER};
				db_exec_noresult(sql,
					2,
					params,
					types);
		}
	}
}

void
dump_tree(struct node *no) {
	char *usage = 0;
	int i;
	if (no->type != _RECORD_) {
		ERROR("dump_tree( %s ) called. EXIT", k2s(no->type));
		exit(1);
	}
	for (i = 0; i < no->nch; i++) {
		if (no->ch[i]->type == _USAGE_) {
			usage = no->ch[i]->val[0];
			break;
		}
	}
	if (usage == 0) {
		printf("WARNING: no USAGE key in the record. Don't know how to dump it\n");
		return;
	}
	if (strcmp(usage,"P") == 0) {
		dump_tree_P(no,n_P);
		n_P++;
	}
	else if (strcmp(usage, "DD") == 0){
		dump_tree_ABBR(no, "DD");
	}
	else if (strcmp(usage, "PP") == 0){
		dump_tree_ABBR(no, "PP");
	}
	else if (strcmp(usage, "AC") == 0){
		dump_tree_ABBR(no, "AC");
	}
	else if (strcmp(usage, "DE") == 0){
		dump_tree_DE(no, "DE");
	}
	else if (strcmp(usage, "I") == 0){
		dump_tree_I(no);
	}
	else if (strcmp(usage, "R") == 0){
		dump_tree_R(no);
	}
	else if (strcmp(usage, "RR") == 0){
		dump_tree_DE(no, "RR");
	}
	else {
		printf("INFO: USAGE = %s, no dump required\n", usage);
	}
	dump_tree_sqlite(no);
	return;
}

enum keytype
k2par(enum keytype k) {
	int ik;
	for (ik = 0; keys[ik].s; ik++) {
		if (keys[ik].key == k) {
			return keys[ik].par;
		}
	}
	return _UNKNOWN_;
}

struct node*
find_par(enum keytype key, struct node* no) {
	if (!no){
		DEBUG("!no");
		return NULL;
	}
	print_tree(no, 0);
    if (no->type == _RECORD_){
        DEBUG("no->type == _RECORD_");
        return no;
    }
	if (no->type == k2par(key)){
		return no;
	} else{
		return find_par(key,no->par);
	}
}

enum keytype
s2k(char *s) {
    DEBUG("s2k(%s)", s);
	enum keytype ik = _UNKNOWN_;
	for (int k = 0; keys[k].s; k++) {
		if (strcmp(keys[k].s, s) == 0) {
			ik = keys[k].key;
			break;
		}
	}
    DEBUG("RETURN %d" ,ik);
	return ik;
}

%}

%%

ABBREVIATION|AS|AT|NAME|USAGE|COMMENT|COMMENT-TEX|ORCODE|IDENT|QN_Q|QN_BAR|Q|MASS|RC|RPP|CRPP|PN|SB|DEN|DCH|DESCRIPTION|QUESTION|URL {
/*	printf("yytext:%s:\n",yytext);*/
	switch (st) {
		case _val_:
			for (py = yytext; *py; *p++ = *py++);
			break;
		case  _ex_ :
		case  _er_ :
			if ((key = s2k(yytext)) == _UNKNOWN_) {
				ERROR("unknown key: %s. EXIT",yytext);
				exit(1);
			}
			mult_val = 0;
			st = _inkey_;
			break;
		case  _inkey_:
			ERROR("spurious \"%s\" on the input. EXIT",yytext);
			exit(1);
			break;
	}
}

"." {
	switch(st) {
		case _val_:
			*p++ = *yytext;
			break;
		case _ex_:
		case _er_:
			ERROR("spurious '.' after ';' or '*E'. EXIT");
			exit(1);
			break;
		case _inkey_:
			mult_val = 1;
			break;
	}
}

"=" {
	switch (st) {
		case  _val_:
			*p++ = *yytext;
			break;
		case  _ex_:
			p = buf;
			*p++ = *yytext;
			st = _val_;
			break;
		case  _er_:
			ERROR("spurious '=' after '*E'. EXIT");
			exit (1);
			break;
		case  _inkey_:
			if (mult_val) {
				;
			}
			else {
				if ((no_par = find_par(key,no_cur))) {
                    DEBUG("KEY = %d", key);
					no_cur = new_node(key,no_par);
				}
				else {
					ERROR("can't find parent for child %s. EXIT",
						k2s(key)
					);
					exit(1);
				}
			}
			p = buf;
			st = _val_;
			break;
	}
}

";" {
	switch (st) {
		case  _val_:
			if (p > buf && *(p-1) == '\\') {
				*p++ = *yytext;
			}
			else {
				*p = '\0';
				printf("key: %s%sval: %s\n",k2s(key), mult_val ? " +" : "  ",  buf);
				val_node(no_cur,buf);
				p = buf;
				st = _ex_;
			}
			break;
		case  _ex_:
			break;
		case  _er_:
			ERROR("spurious ';' after '*E'. EXIT");
			exit(1);
			break;
		case  _inkey_:
			ERROR("spurious ';' after '='. EXIT");
			exit(1);
			break;
	}
}

"*E" {
	switch (st) {
		case  _val_:
			for (py = yytext; *py; *p++ = *py++);
			break;
		case  _ex_:
			key = _NOKEY_;
			st = _er_;
			printf("EOR, current record: --\n");
			print_tree(no_root,0);
			printf ("Dumping ...\n");
			dump_tree(no_root);
			printf("======================================\nNew record ...........\n");
			free_tree(no_root);
			no_par = no_cur = no_root = new_node(_RECORD_, NULL);
			break;
		case  _er_:
			break;
		case  _inkey_:
			ERROR("spurious '*E' after key. EXIT");
			exit(1);
			break;
	}
}

[ \t\n] {
	switch (st) {
		case  _val_:
			*p++ = *yytext;
			break;
		case  _ex_:
		case  _er_:
		case  _inkey_:
			break;
	}
}

[^ \t\n] {
	switch (st) {
		case  _val_:
			*p++ = *yytext;
			break;
		case  _ex_:
			st = _val_;
			*p++ = *yytext;
			break;
		case  _er_:
			ERROR("spurious '%c' after '*E'. EXIT",*yytext);
			exit(1);
		case  _inkey_:
			ERROR("spurious '%c' after key. EXIT",*yytext);
			exit(1);
	}
}




%%
int yywrap(void) {
	return 1;
}


int  main(void) {
	n_P = 0;
	st = _er_;
	no_par = no_cur = no_root = new_node(_RECORD_,NULL);
	db_exec_noresult("BEGIN TRANSACTION", 0, 0, 0);
	ppdsvylex();
	db_exec_noresult("END TRANSACTION", 0, 0, 0);
	db_close();
	return 0;
}
