#include <stdio.h>
#include <stdlib.h>

#include "vy/tree.h"
#include "vy/P.h"

#include<stdio.h>
#include<stdlib.h>

#include "sqlite.h"
#include "settings.h"

double
_round_(double x) {
	int s = (x < 0. ? -1 : 1);
	double a = s*x;
	if (a - (int)a >= 0.5)
		a = (int)(a+1);
	else
		a = (int)a;
	return s*a;
}

void
dump_tree_P (struct node *no, int id) {
	int ia = -1, i, j, orcode = -1, ident = -1;
	int bar,lep, S, C, B, P, heavy_nuc, classname;
	double m_or = -1., mass = -1., I, Q, QN_Q = -9998;
	int QN_BAR = -9998;
	printf("INFO: dump particle: ...............\n");
	for (i = 0; i < no->nch; i++) {
		switch (no->ch[i]->type) {
		case _ABBREVIATION_:
			printf("  abbr:");
			ia = i;
			for (j = 0; j < no->ch[i]->nval; j++) {
				printf(" %s;",no->ch[i]->val[j]);
			}
			printf("\n");
			break;
		case _ORCODE_:
			for (j = 0; j < no->ch[i]->nval; j++) {
				printf(" .... or: %s\n",no->ch[i]->val[j]);
				if (j == 0) {
					orcode = atoi(no->ch[i]->val[j]);
					exp_orcode( no->ch[i]->val[j],
					            &bar,&lep,
					            &S, &C, &B,
					            &P,
					            &heavy_nuc,&classname,
					            &I,&Q,
					            &m_or
					          );
					printf(" ........\n");
				}
			}
			break;
		case _QN_Q_:
			QN_Q = atof(no->ch[i]->val[0]);
			break;
		case _QN_BAR_:
			QN_BAR = atoi(no->ch[i]->val[0]);
			break;
		case _MASS_:
			if (no->ch[i]->nval > 1) {
				printf("WARNING: > 1 masses for a particle, taking the 1st\n");
			}
			mass = atof(no->ch[i]->val[0]);
			break;
		case _IDENT_:
			if (ident != -1) {
				WARNING("> 1 IDENT's for a particle");
			}
			ident = atoi(no->ch[i]->val[0]);
			break;
		default:
			break;
		}
	}
	if (m_or != mass) {
		printf("WARNING: mass = %lf != mass from orcode: %lf\n", mass, m_or);
	}
	if (QN_Q != -9998) {
		printf("WARNING: QN_Q = %lf overrides Q from orcode: %lf\n",QN_Q,Q);
		Q = QN_Q;
	}
	if (QN_BAR != -9998) {
		printf("WARNING: QN_BAR = %d overrides baryon number from the orcode: %d\n",QN_BAR,bar);
		bar = QN_BAR;
	}
	printf("PRT(or, m, bar,lep, S C B, P, I 3Q, hnuc, classname):\n");
	const double q3 = (Q == -5. ? -9999
							    : (Q == -9999. ? Q : _round_(3.*Q))
							    );
	printf("PRT: %d %d %d  %d %d %d %d  %d %d %d  %lf %lf %lf  %d",
		 id,
		 bar,
		 lep,
		 S,C,B,0,
		 P,heavy_nuc,classname,
		 mass,I,
		 q3, /*FIXME: dirty hack: ORCODE = -5 -> -9999 for generic particles */
		 orcode                     /*write out 3Q to always have integer charges*/
	);
	if (ia >= 0) {
		for (j = 0; j < no->ch[ia]->nval; j++) {
			printf(" %s",no->ch[ia]->val[j]);
		}
		printf("\n");
	}

	// dump to sqlite
	// ID INTEGER, BAR INTEGER, LEP INTEGER, "
	// "S INTEGER, C INTEGER, B INTEGER, T INTEGER, ANTI INTEGER, HEAVY_NUC INTEGER, "
	// "classname INTEGER, M REAL, 'I3' REAL, Q REAL, ORCODE INTEGER, ABBREVIATION TEXT;
	const enum SQLITE_TYPE types[] = {
		INTEGER, INTEGER, INTEGER, INTEGER, INTEGER,
		INTEGER, INTEGER, INTEGER, INTEGER, INTEGER,
		REAL, REAL, REAL, INTEGER, TEXT
	};
	const int rec_id = db_get_last_id("vy_P", 0) + 1;
	printf("VY_P RECID = %d\n", rec_id);
	const int T = 0;
	if (ia >= 0) {
		for (j = 0; j < no->ch[ia]->nval; j++) {
			const char *abbr = no->ch[ia]->val[j];
			const void * params[] = {
				(void*)&rec_id,
				(void*)&bar,
				(void*)&lep,
				(void*)&S,
				(void*)&C,
				(void*)&B,
				(void*)&T,
				(void*)&P,
				(void*)&heavy_nuc,
				(void*)&classname,
				(void*)&mass,
				(void*)&I,
				(void*)&q3,
				(void*)&orcode,
				(void*)abbr
			};
			int rc = db_exec_noresult(
				"INSERT into vy_P values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
				15,
				params,
				types
				);
			if (rc != SQLITE_DONE){
				ERROR("SQLITE ERROR!");
				db_close();
				exit(1);
			}
		}
	}
	// end dump
	return;
}
