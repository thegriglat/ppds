#include<stdio.h>
#include<stdlib.h>

#include "vy/tree.h"
#include "settings.h"

int
exp_orcode ( char *s,
             int *bar,
             int *lep,
             int *S,
             int *C,
             int *B,
             int *P,
             int *heavy_nuc,
             int *classname,
             double *I,
             double *Q,
             double *m

           )
{

  int nm, ns, i;
  enum { baryon, strange, mass, iso, anti, charge, charge3, end } check;

  *bar =  *lep = *heavy_nuc = *classname =
                                  *S = *C = *B = *P = 0;
  *I = *Q = 0.;
  *m = -1.;

  if (!s) {
    ERROR("orcode( NULL ). EXIT");
    return -1;
  }

  printf("%s\n",s);

  for (check = baryon;;) {

    if (check == end) {
      if (*s) {
        ERROR("extra trailing digits: %s", s);
        return -1;
      } else {
        break;
      }
    }

    if (!*s) {
      ERROR("more digits expected.");
      return -1;
    }

    if (check == baryon) {
      switch (*s) {
      case '2':
        *classname = 79;
        printf("for abbreviations from classes 7) and 9). exit.\n");
        return 0;
        break;
      case '4':
        *classname = 6;
        printf("for abbreviations from class 6 ???\n");
        return 0;
        break;
      case '6':
        printf("gamma:\n");
        *bar = 0;
        *lep = 0;
        s++;
        check = strange;
        break;
      case '8':
        printf("lepton:\n");
        *bar = 0;
        *lep = 1;
        s++;
        check = strange;
        break;
      case '1':
        if (*(s+1) == '6') {
          *bar = 9999;
          *lep = 0;
          printf("heavy nucleus, |bar| > 4\n");
          *heavy_nuc = 1;
        } else if (*(s+1) == '8') {
          *bar = 0;
          *lep = 0;
          printf("DD or NON-RES\n");
        } else {
          *bar = *(s+1) - '0';
          *lep = 0;
          printf("baryon/meson: bar = %d\n",*bar);
        }
        s += 2;
        check = strange;
        break;
      default:
        ERROR("wrong orcode[0] = %d. EXIT", s[0] - '0');
        return -1;
      }
    } else if (check == strange) {
      ns = (*s) - '0';
      if (*bar != 0) {
        if (ns == 2) {
          printf("Z* baryon: exotic I= unspecified, S=+1 baryon of unspecified mass\n");
          *S = 1;
          *C = 0;
          *B = 0;
        } else if (ns <= 7) {
          *S = 7 - ns;
          *B = *C = 0;
          printf("baryon except Z*, S = %d\n",*S);
        } else if (ns == 8) {
          printf("c-baryon\n");
          *S = 0;
          *C = 1;
          *B = 0;
        } else if (ns == 9) {
          printf("b-baryon\n");
          *S = 0;
          *C = 0;
          *B = 1;
        } else {
          ERROR("wrong NS: %c",*s);
          return -1;
        }
      } else {
        if (ns == 6) {
          printf("c-meson\n");
          *B = 0;
          *S = 0;
          *C = 1;
        } else if (ns == 7) {
          printf("b-meson\n");
          *B = 1;
          *S = 0;
          *C = 0;
        } else {
          *S = *s - '0' - 3;
          *B = *C = 0;
          printf("meson: S = %d\n",*S);
        }
      }
      check = mass;
      s ++;
    } else if (check == mass) {
      for (i = 0, nm = 0; i < 4 && *(s+i); i ++) {
        nm = 10*nm + *(s+i) - '0';
      }
      if (i < 4) {
        ERROR("< 4 NM digits. EXIT");
        return -1;
      }
      printf("INFO: NM = %d\n",nm);
      if (*bar == 0) {
        *m = nm - 1000;
        printf("meson: m = %lf MeV\n",*m);
      } else if (abs(*bar) <= 4) {
        *m = 9000 - nm;
        printf("baryon(|B|<=4): m = %lf MeV\n",*m);
      } else {
        printf("xz: B = %d NM = %d\n", *bar,nm);
      }
      s += 4;
      if (!(*heavy_nuc)) {
        check = iso;
      } else {
        check = charge3;
      }
    } else if (check == iso) {
      *I = (double)(*s - '0' - 3) / 2.;
      printf("I = %lf\n",*I);
      s ++;
      check = anti;
    } else if (check == anti) {
      switch (*s) {
      case '7':
        *P = 1;
        printf("particle\n");
        break;
      case '3':
        *P = -1;
        printf("anti-particle\n");
        break;
      case '6':
        *P = 2;
        printf("KS\n");
        break;
      case '4':
        *P = 3;
        printf("KL\n");
        break;
      default:
        WARNING("undefined NP = %c", *s);
        break;
      }
      s ++;
      check = charge;
    } else if (check == charge) {
      *Q = *s - '0' - 5;
      printf("Q = %lf\n",*Q);
      s ++;
      check = end;
    } else if (check == charge3) {
      for (i = 0, *Q = 0.; i < 3 && *(s+i); i++) {
        *Q = 10*(*Q) + *(s+i) - '0';
      }
      if (i < 3) {
        ERROR("3 digits expected for heavy nucleus' charge");
        return -1;
      }
      printf("heavy nuc.: Q = %lf\n",*Q);
      s++;
      check = end;
    } else {
      ERROR("unknown check = %d", check);
      return -1;
    }
  }

  if (*bar == 0) {
    if (*I == -1.5) {
      *I = 0;
    }
    if (*Q == -5.) {
      *Q = 0.;
    }
  }

  return 0;
}
/***********************************************************

 Each abbreviation has a number - OR-code, which serve to
 order names in final state, in this number, following numbers
 are packed from left to right: NB, NS, NM, II, NP, NC.

 NB (2 FIGURES)= B+10 for mesons and baryons with /B/ <=4; B -
 baryon number
 = 2 for abbreviations from classes 7) and 9)
 = 4 for abbreviations from class 6
 = 6 for PHOTON
 = 8 for LEPTONS
 = 16 for heavy nuclei with /B/>4
 = 18 for abbreviations DD, NON-RES
 NS (1 FIGURE ) = 3+/S/ for mesons, where S-strangeness
 = 7-/S/ for baryons except Z*
 = 2 for Z*
 = 6 for charmed mesons
 = 7 for beauty mesons,
 = 8 for charmed baryons.
 = 9 for beauty baryons.
 NM (4 FIGURES) = 9000-M for baryons with /b/<=4, where M - mass
 in MeV.
 = 1000+M for mesons
 = 1000+10*|B|+5 for nuclei with /B/>4.
 = 1000+10*|B|+5+N for isotopes where -5<=N<=4
 NI (1 FIGURE ) = 2*I+3, where I - isotopic spin
 NP (1 FIGURE ) = 7 for particles, 3 for antiparticles, 6 for
 KS, 4 for KL.

 NC (1 FIGURE ) = C+5, where C - charge. For heavy nucleus
 (NB=16) NI, NP, NC contain charge of nuclei.

***************************************************************/

/*
int
main (int argc, char** argv) {
  orcode(argv[1]);
  return 0;
}
*/
