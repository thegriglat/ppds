#include<sys/stat.h>
#include<sys/types.h>
#include<fcntl.h>
#include<dirent.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

#include "dg/tree.h"

#include "util.h"

#include "settings.h"


extern
struct record_key
keys[];

static int
record_cur = 0;

static 
char 
*base = NULL, 
*curdir = ".", 
*base_records = NULL;

static FILE*
new_rec_file (char *basepath, int* rec_num);

static int
del_rec_file(int rec_num);

static char*
hash_from_val(enum keytype key, char *val);

/*
static void
touch_file(char *file, char *dir);
*/

static int
write_node(FILE *f_rec, struct node *no, int ident);


int
tree2base (struct node* no) {
	DIR *d;
	FILE *f_rec = 0;
	int rec_num;
	if (!no) {
		printf("tree2base( NULL ) called!");
		return 0;
	}
	if (!base) {
		if (!(base = getenv("PPDS_BASE"))) {
			printf("no PPDS_BASE env. variable, using ./\n");
			base = curdir;
		}
		else {
			printf("INFO: PPDS_BASE = %s\n",base);
			while (!(d = opendir(base))) {
				printf("INFO: can't open %s, trying to create ...\n",base);
				if (mkdir(base,0770) < 0) {
					ERROR("can't create %s [EXIT]",base);
					exit(1);
				}
			}
			closedir(d);
		}
	}
	if (!base_records) {
		base_records = (char*)malloc(sizeof(char) * (strlen(base) + strlen("/records/") + 1));
		sprintf(base_records,"%s/records/",base);
		printf("INFO: dir. with records: %s\n",base_records);
	}
	while (!(d = opendir(base_records))) {
		printf("INFO: can't open %s, trying to create ... \n",base_records);
		if (mkdir(base_records,0770) < 0) {
			ERROR("can't create %s [EXIT]",base_records);
			exit(1);
		}
	}
	closedir(d);
	if (!(f_rec = new_rec_file(base_records,&rec_num))) {
		ERROR("can't create new record file in %s [EXIT]",base_records);
		exit(1);
	}
	if (!write_node(f_rec,no,0)) {
		ERROR(
			"tree2base(): dump to base failed, removing the incomplete record: %d"
			"       Remove references to it from %s/*/r !!!!\n",	
				rec_num, base
		);
		if (!del_rec_file(rec_num)) {
			exit(1);
		}
		return 0;
	}
	fclose(f_rec);
	return 1;
}

// static char*
// node_dir(char* dir, char* keyname) {
// 	DIR *d = 0;
// 	struct dirent *entr = 0;
// 	int lk, k_found;
// 	char *new_name = 0;
// 	DEBUG("node_dir(%s,%s) ... \n",dir,keyname);
// 	if (!dir) {
// 		ERROR("node_dir(NULL,...) called [EXIT]");
// 		exit(1);
// 	}
// 	if (!(d = opendir(dir))) {
// 		ERROR("node_dir(%s,%s): can't open %s [EXIT]",dir,keyname,dir);
// 		exit(1);
// 	}
// 	if (!keyname) {/* root node! */
// 		DEBUG("node_dir(...,NULL): call closedir(%s) ...\n",dir);
// 		closedir(d);
// 		DEBUG("node_dir() bp000: return dir;\n");
// 		return dir;
// 	}
// 	lk = strlen(keyname);
// 	for(k_found = 0;;) {
// 		entr = readdir(d);
// 		if (entr) {
// 			if (entr->d_type == 4) {
// 				if (strcmp(entr->d_name,".") == 0 || strcmp(entr->d_name,"..") == 0) 
// 					continue;
// 				if (strncmp(entr->d_name,keyname,lk) == 0 
// 				    && ( entr->d_name[lk] == '\0' || entr->d_name[lk] == '.' ) ) 
// 					k_found ++;
// 			}
// 		}
// 		else
// 			break;
// 	}
// 	closedir(d);
// 	DEBUG("node_dir(): k_found = %d\n",k_found);
// 	new_name = (char*)malloc(sizeof(char) * (strlen(dir) + 1 + strlen(keyname) + 132));
// 	sprintf(new_name,"%s/%s.%d",dir,keyname,k_found);
// 	DEBUG("node_dir(): new_name:%s:\n",new_name);
// 	if (mkdir(new_name,0770) < 0) {
// 		ERROR("can't create %s/ [EXIT]",new_name);
// 		exit(1);
// 	}
// 	DEBUG("node_dir(): bp001: return new_name;\n");
// 	return new_name;
// }

static int
write_node(FILE *f_rec, struct node *no, int ident) {
	char *hash_val = NULL;
	int i;
	DEBUG("write_node( ...)\n");
	if (!f_rec) {
		ERROR("write_node(NULL, ...) called [EXIT]");
		exit(1);
	}
	if (!no) {
		printf("write_node(..., NULL, ...L) called. Doing nothing.\n");
		return 1;
	}
	if (no->type == _SC_)
		printf("INFO: SC = %s;\n",no->val[0]);
	for (i = 0; i < ident; i++)
		fputc(' ',f_rec);
	fprintf(f_rec,"%s {\n",k2s(no->type));
	if (!k2noval(no->type)) {
		for (i = 0; i < no->nval; i++) {
			if (!(hash_val = hash_from_val(no->type,no->val[i]))) {
				ERROR("can't find hash for %s = %s; Check the vocabulary?", 
					k2s(no->type), no->val[i]
				);
				return 0;
			}
			for (i = 0; i < ident+2; i++)
				fputc(' ',f_rec);
			fprintf(f_rec,"v:%s\n",hash_val);
		}
	}
	for (i = 0; i < no->nch; i++) 
		if (!write_node(f_rec, no->ch[i], ident+4)) 
			return 0;
	for (i = 0; i < ident; i++)
		fputc(' ',f_rec);
	fputc('}',f_rec);
	fputc('\n',f_rec);
	free((void*)hash_val);
	return 1;
}




static FILE*
new_rec_file (char *basepath, int *rec_num) {
	DIR *bd = 0;
	struct dirent *entr = 0;
	char *rec_path = NULL;
	int rec_max = 0,  i;
	FILE *f_rec = 0;
	if (!(bd = opendir(basepath))) {
		ERROR("can't open %s [EXIT]",basepath);
		exit(1);
	}
	for(;;) {
		entr = readdir(bd);
		if (entr) {
			if (entr->d_type == 8) {
				if (entr->d_name[0] == '0') {
					ERROR(
						"record name starts with `0': %s%s. EXIT",
						base_records,entr->d_name
					);
					exit(1);
				}
				for (*rec_num = 0, i = 0; entr->d_name[i]; i++) {
					if (entr->d_name[i] >= '0' && entr->d_name[i] <= '9') {
						*rec_num = 10*(*rec_num) + entr->d_name[i] - '0';
					}
					else {
						ERROR(
							"non-numerical record name: %s%s. EXIT",
							base_records,entr->d_name
						);
						exit(1);
					}
				}
/*				printf("%d: %s rec: %d\n",entr->d_type,entr->d_name, rec_num);*/
				if (*rec_num > rec_max)
					rec_max = *rec_num;
			}
		}
		else
			break;
	} 
	closedir(bd);
	record_cur = *rec_num = rec_max + 1;
	DEBUG("max record number: %d, using %d for the new record\n",rec_max,*rec_num);
	rec_path = (char*)malloc(sizeof(char) * (strlen(base_records) + 100)); /*FIXME*/
	sprintf(rec_path,"%s%d",base_records,*rec_num);
	if (!(f_rec = fopen(rec_path,"w"))) {
		ERROR("can't create record file: %s  [EXIT]",rec_path);
		exit(1);
	}
	free((void*)rec_path);
	return f_rec;
}

int
del_rec_file(int rec_num) {
	char *rec_path = (char*)malloc(sizeof(char) * (strlen(base_records) + 100)); /*FIXME*/
	sprintf(rec_path,"%s%d",base_records,rec_num);
	if (remove(rec_path) < 0) {
		ERROR("can't remove %s [EXIT]",rec_path);
		exit(1);
	}
	free((void*)rec_path);
	return 1;
}


static char*
write_val_hash(char *fn_hsh, char *fn_val, char *val, enum keytype key) {
	void *t;
	char hsh[64], *h,*p, c;
	int rec_pos, n;
	int found = 0;
	FILE *f_hsh = fopen(fn_hsh,"r"), 
	     *f_val = 0;
	int ik = k2ik(key);
	h = (keys[ik].s2t && keys[ik].t2h) ? keys[ik].t2h(t = keys[ik].s2t(val)) : s2hash(val);
	if (keys[ik].t_free)
		keys[ik].t_free(t);
	if (!h){
		fclose(f_hsh);
		return NULL;
	}
	DEBUG("new hash: %s s:%s:",h,val);
	if (!f_hsh) {
		f_hsh = fopen(fn_hsh,"w");
		if ((f_val = fopen(fn_val,"r"))) {
			ERROR(
				"hash file %s is empty while the record file %s exists [EXIT]", 
				fn_hsh, fn_val
			);
			fclose(f_val);
			fclose(f_hsh);
			exit(1);
		}
		f_val = fopen(fn_val,"w");
		fprintf(f_hsh,"%s %10d\n",h, 0);
		fclose(f_hsh);
		for (p = h; *p; p++)
			fputc(*p,f_val);
		fputc(' ',f_val);
		for (p = val; *p; p++)
			fputc(*p, f_val);
		fputc('\0',f_val);
		fputc('\n',f_val);
		fclose(f_val);
	}
	else {
		found = 0;
		DEBUG("write_val_hash(): looking through %s ...\n",fn_hsh);
		while ((n = fscanf(f_hsh,"%s%d",hsh,&rec_pos)) >= 0)  {
			DEBUG("n: %d",n);
			DEBUG("h,p: %s %d",hsh,rec_pos);
			if (strcmp(hsh,h) == 0)  {
				DEBUG("hash found\n");
				found = 1;
				break;
			}
		}
		fclose(f_hsh);
		if (!found) {
			DEBUG("hash not found\n");
			f_hsh = fopen(fn_hsh,"a");
			f_val = fopen(fn_val,"a");
			rec_pos = ftell(f_val);
			DEBUG("f_val pos: %d\n",rec_pos);
			fprintf(f_hsh,"%s %d\n",h,rec_pos);
			fclose(f_hsh);
			for (p = h; *p; p++)
				fputc(*p,f_val);
			fputc(' ',f_val);
			for (p = val; *p; p++)
				fputc(*p,f_val);
			fputc('\0',f_val);
			fputc('\n',f_val);
			fclose(f_val);
		}
		else {
			if (_DBG_) {
				printf("checking rec at pos = %d\n",rec_pos);
				f_val = fopen(fn_val,"r");
				fseek(f_val,rec_pos,SEEK_SET);
				printf("read: hsh:");
				while ((c = fgetc(f_val)) != ' ')
					putchar(c);
				printf(": rec:");
				while ((c = fgetc(f_val)) != '\0')
					putchar(c);
				printf(":\n");
				fclose(f_val);
			}
		}
	}
	return h;
}


static char*
hash_from_val(enum keytype key, char *val) {
	char *key_name = k2s(key),
	     *key_dir  = (char*)malloc(sizeof(char) * (strlen(base) + 1 + strlen(key_name) + 1)),
	     *hash_file_path = 0,
	     *val_file_path = 0,
	     *hash = 0;
	DIR *d;
	FILE *f_record = 0;
	sprintf(key_dir,"%s/%s",base,key_name);
	while (!(d = opendir(key_dir))) {
		printf("INFO: can't open %s, trying to create ... \n",key_dir);
		if (mkdir(key_dir,0770) < 0) {
			ERROR("can't create %s [EXIT]",key_dir);
			exit(1);
		}
	}
	closedir(d);
	hash_file_path = (char*)malloc(sizeof(char) * (strlen(key_dir) + 1 + 1 + 1));
	val_file_path  = (char*)malloc(sizeof(char) * (strlen(key_dir) + 1 + 1 + 1));
	sprintf(hash_file_path,"%s/h",key_dir);
	sprintf(val_file_path, "%s/v",key_dir);
	if (!(hash = write_val_hash(hash_file_path,val_file_path, val, key))){
		free(val_file_path);
		free(hash_file_path);
		return NULL;
	}
	free((void*)hash_file_path);
	sprintf(val_file_path, "%s/r",key_dir);
	f_record = fopen(val_file_path,"a");
	fprintf(f_record,"%s %d\n",hash,record_cur);
	fclose(f_record);
	free((void*)val_file_path);
	free((void*)key_dir);
	return hash;
}

/*
static void
touch_file (char *fname, char* dir) {
	char *new_path;
	DIR *d;
	FILE *f;
	if (!(d = opendir(dir))) {
		ERROR("%s doesn't exist [EXIT]",dir);
		exit(1);
	}
	closedir(d);
	new_path = (char*)malloc(sizeof(char) * (strlen(dir) + 1 + strlen(fname) + 1));
	sprintf(new_path,"%s/%s",dir,fname);
	if (!(f = fopen(new_path,"a"))) {
		ERROR("can't create %s  [EXIT]",new_path);
		exit(1);
	}
	fclose(f);
	free((void*)new_path);
	return;
}
*/
