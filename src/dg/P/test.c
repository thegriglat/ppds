#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "dg/P.h"

#include "settings.h"


extern  
void 
vy_init_P(); /* ../dg/P/f.c */

int
main (int argc, char** argv) {
	char *p, *ps, *stmp;
	struct type_P *t = 0;
	int r, l, i;
	if (argc < 2) {
		ERROR("Usage: test {particle}\n");
		return 1;
	}
	if ((l = strlen(argv[1])) == 0) {
		ERROR("no particle given");
		return 1;
	}
	stmp = (char*)malloc(sizeof(char)*(l+1));
	for (p = argv[1], ps = stmp; *p; p++) {
		if (*p == ' ' || *p == '\t' || *p == '\n') {
			if (ps > stmp)
				break;
		}
		else {
			*ps ++ = ( *p >= 'a' && *p <= 'z' ? *p - 'a' + 'A' : *p );
		}
	}
	*ps = '\0';
	vy_init_P();
	if ((t = (struct type_P*) s2t_P(stmp)) == NULL ){
		ERROR("unknown particle: %s",stmp);
		return 1;
	}
	printf("id: %d\n",t->id);
	for (i = 0; i < t->nval; i ++)
		printf("%s\n",t->val[i]);
	return 0;
}
