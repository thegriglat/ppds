#include "dg/P.h"
#include "settings.h"
#include "util.h"

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include "str.h"

#include "sqlite.h"

static
int n_P = 0;

static
struct type_P *  _P = NULL;

struct name_index {
    long sum;
    int  i;
};

void
vy_init_P (void)
{
    if (_P) {
        return;
    }
    // _P = (struct type_P *) malloc(sizeof(struct type_P) * (n = 256) );
    const char* value_sql = "select ID, BAR, LEP, S, C, B, T, ANTI, HEAVY_NUC, classname, M, I3, Q, ORCODE, ABBREVIATION from vy_P;";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(get_db(), value_sql, -1, &stmt, 0) != SQLITE_OK) {
        ERROR("Cannot prepare SQL: %s", value_sql);
        exit(1);
    };
    int step = sqlite3_step(stmt);
    int id = 0, id_old = 0;
    struct type_P* cur_P = NULL;
    while (step == SQLITE_ROW) {
        id_old = id;
        id = sqlite3_column_int(stmt, 0);
        const int bar = sqlite3_column_int(stmt, 1);
        const int lep = sqlite3_column_int(stmt, 2);
        const int S = sqlite3_column_int(stmt, 3);
        const int C = sqlite3_column_int(stmt, 4);
        const int B = sqlite3_column_int(stmt, 5);
        const int T = sqlite3_column_int(stmt, 6);
        const int anti = sqlite3_column_int(stmt, 7);
        const int heavy_nuc = sqlite3_column_int(stmt, 8);
        const int classname = sqlite3_column_int(stmt, 9);
        const double m = sqlite3_column_double(stmt, 10);
        const double I3 = sqlite3_column_double(stmt, 11);
        const double Q = sqlite3_column_double(stmt, 12);
        const int orcode = sqlite3_column_int(stmt, 13);
        const char* abbreviation = (char*)sqlite3_column_text(stmt, 14);
        if (id != id_old) {
            // add new item to _P
            ++n_P;
            _P = (struct type_P*)realloc(_P, sizeof(struct type_P) * (n_P));
            cur_P = &_P[n_P - 1];
            cur_P->id = id;
            cur_P->bar = bar;
            cur_P->lep = lep;
            cur_P->S = S;
            cur_P->C = C;
            cur_P->B = B;
            cur_P->T = T;
            cur_P->anti = anti;
            cur_P->heavy_nuc = heavy_nuc;
            cur_P->classname = classname;
            cur_P->m = m;
            cur_P->I3 = I3;
            cur_P->Q = Q;
            sprintf(cur_P->orcode, "%d", orcode);
            cur_P->nval = 1;
            cur_P->val = (char**)malloc(sizeof(char*));
            cur_P->val[0] = (char*)malloc(sizeof(char) * (strlen(abbreviation) + 1));
            strcpy(cur_P->val[0], abbreviation);
            cur_P->val0sum = s2sum(cur_P->val[0]);
        } else if (cur_P){
            // another abbreviation for the same particle
            cur_P->nval++;
            cur_P->val = (char**)realloc(cur_P->val, sizeof(char*) * (cur_P->nval));
            cur_P->val[cur_P->nval - 1] = (char*)malloc(sizeof(char) * (strlen(abbreviation) + 1));
            strcpy(cur_P->val[cur_P->nval - 1], abbreviation);
        }
        step = sqlite3_step(stmt);
    }
    sqlite_stmt_free(stmt);
    /* particle format: id bar lep S C B T anti heavy_nuc classname m I3 Q orcode name: */
    return;
}


void *
s2t_P (char *s)
{
    if (!s){
        WARNING("s2t_P( null )");
        return NULL;
    }
    int i;
    char* upper_s = toUpper(s);
    for (i = 0; i < n_P; ++i){
        int j;
        for (j = 0; j < _P[i].nval; ++j){
            char* upper_val = toUpper(_P[i].val[j]);
            if (strcmp(upper_val, upper_s) == 0){
                free(upper_s);
                free(upper_val);
                return &_P[i];
            }
            free(upper_val);
        }
    }
    free(upper_s);
    WARNING("s2t_P(%s): unknown particle", s);
    return NULL;
}

char*
t2s_P(void *t)
{
    struct type_P *p = (struct type_P *) t;
    return p->val[0];
}


char*
t2h_P(void *t)
{
    struct type_P *p = (struct type_P *) t;
    char *hbuf = (char*)malloc(132*sizeof(char));
    sprintf(hbuf,"%044x", (p ? p->id : 0));
    return hbuf;
}

int
eq_P (void *t1,void *t2)
{
    return 0;
}

static
char *_s_P_unknown = "$ERROR";

char *
id2s_P(int id)
{
    int i;
    if (id == -1)
        return _s_P_unknown;
    for (i = 0; i < n_P; i++)
        if (_P[i].id == id)
            return _P[i].val[0];
    return _s_P_unknown;
}

struct type_P*
id2t_P(int id)
{
    int i;
    if (id == -1) {
        WARNING("id2t_P (id = %d): unknown particle",id);
        return 0;
    }
    for (i = 0; i < n_P; i++)
        if (_P[i].id == id)
            return &_P[i];
    WARNING("id2t_P (id = %d): unknown particle",id);
    return 0;
}

