#include "dg/DD.h"
#include "settings.h"
#include "util.h"

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "sqlite.h"


static
int n_DD = 0;

static
struct type_DD * _DD = NULL;


void
vy_init_DD (void) {
	char *vy_base = 0, *vy_fn = 0;
	FILE *vy_f = 0;
	if (_DD) {
		return;
	}
	char *line = (char*)malloc(sizeof(char) * 1024);
	if (!(vy_base = getenv("PPDS_VY"))) {
		ERROR(
		 "vy_init_DD(): PPDS_VY env. pointing to the "
		 "vocabulary directory is not defined [EXIT]\n"
		);
		exit(1);
	}
	vy_fn = (char *)malloc(sizeof(char)*(strlen(vy_base) + 1 + 2 + 1 + 2 + 1));
	sprintf(vy_fn,"%s/DD/vy",vy_base);
	DEBUG(" reading particle definitions from PPDS_VY = %s",vy_fn);
	if (!(vy_f = fopen(vy_fn,"r"))) {
		ERROR("vy_init_DD(): can't open %s [EXIT]",vy_fn);
		exit(1);
	}
	free(vy_fn);
	for (n_DD = 0; fgets(line, 1024, vy_f) != NULL; n_DD++){
		if (!_DD){
			_DD = (struct type_DD *) malloc(sizeof(struct type_DD));
		}
		else {
			struct type_DD *tmp = (struct type_DD*)realloc(_DD, sizeof(struct type_DD) * (n_DD + 1));
			if (tmp){
				_DD = tmp;
			}
			else{
				printf("Cannot allocate memory.");
				exit(0);
			}
		}
		_DD[n_DD].nval = 0;
		// split line to **val
		int nval = 0;
		char * word;
		char ** val = NULL;
		for (nval = 0; (word = strtok_r(line, " \n", &line)); nval++){
			if (!val)
				val = (char**)malloc(sizeof(char*));
			else {
				char ** tmp = (char**)realloc(val, sizeof(char*) * (nval + 1));
				if (tmp){
					val = tmp;
				}
				else {
					printf("Cannot allocate memory.\n");
					exit(0);
				}
			}
			val[nval] = (char*)malloc(sizeof(char) * (strlen(word) + 1));
			strcpy(val[nval], word);
		}
		_DD[n_DD].val = val;
		_DD[n_DD].nval = nval;
	}
	fclose(vy_f);
}

void *
s2t_DD (char *s) {
	if (!s) {
		WARNING("s2t_DD( null )");
		return NULL;
	}
    const char *sql = "select ABBREVIATION from vy_DD where ID = (SELECT ID FROM vy_DD where ABBREVIATION = ?);";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(
        get_db(),
        sql, -1,
        &stmt,
        0
         ) != SQLITE_OK){
        ERROR("Cannot prepare SQL: %s", sql);
        exit(1);
    }
    if (sqlite3_bind_text(stmt, 0, s, strlen(s), SQLITE_STATIC) != SQLITE_OK){
        ERROR("Cannot bind text '%s'", s);
        exit(1);
    }
    struct type_DD *result = (struct type_DD*)malloc(sizeof(struct type_DD));
    result->nval = 0;
    char found = 0;
    do {
        const int step = sqlite3_step(stmt);
        if (step == SQLITE_ROW){
        	found = 1;
            // sqlite3_column_test return const unsigned char *
            const char* value = (char*) sqlite3_column_text(stmt, 0);
            result->val = (char**)realloc(result->val, result->nval + 1);
            result->val[result->nval] = (char*)malloc(sizeof(char) * (strlen(value) + 1));
            strcpy(result->val[result->nval], value);
            result->nval++;
        } else {
        	if (found == 0){
            	ERROR("Cannot find values in vy_DD for input '%s'", s);
            	exit(1);
            } else {
            	break;
            }
        }
    } while (1);
    return result;
}

char*
t2s_DD(void *t) {
	struct type_DD *p = (struct type_DD *) t;
	return p->val[0];
}

char*
t2h_DD(void *t) {
	struct type_DD *p = (struct type_DD *) t;
	return s2hash(p->val[0]);
}

int
eq_DD (void *t1,void *t2) {
	int i;
	struct type_DD *a = (struct type_DD*) t1;
	struct type_DD *b = (struct type_DD*) t2;
	for (i = 0; i < a->nval; ++i){
		int j;
		for (j = 0; j < b->nval; ++j){
			if (strcmp(a->val[i], b->val[j]) == 0)
				return 1;
		}
	}
	return 0;
}