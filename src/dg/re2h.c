#include "stdlib.h"
#include "stdio.h"

#include "re.h"
#include "y.h" /* ../include/ */
#include "dg/RE.h"
#include "settings.h"

char* re2h(char* s){
	struct type_RE* re = (struct type_RE*)malloc(sizeof(struct type_RE));
	int r = s2listlist(s, &(re->is),&(re->fs));
	if (r > 0){
		ERROR("Error in reaction: %s", s);
		return NULL;
	}
	char* value = t2h_RE(re);
	free_RE(re);
	return value;
}