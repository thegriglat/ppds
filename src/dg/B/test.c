#include<stdio.h>
#include<stdlib.h>

#include "dg/B.h"

int 
main (int argc, char** argv) {
	int i;
	char *s1, *s2, *h1, *h2;
	struct type_B *b1 = (struct type_B*) s2t_B(argv[1]);
	struct type_B *b2 = (struct type_B*) s2t_B(argv[2]);
	for (i = 0; i < b1->n_range; i++)
		printf("%f - %f\n",b1->xmin[i],b1->xmax[i]);
	printf("unit: %d kin: %d\n",b1->unit,b1->kin);
	for (i = 0; i < b2->n_range; i++)
		printf("%f - %f\n",b2->xmin[i],b2->xmax[i]);
	printf("unit: %d kin: %d\n",b2->unit,b2->kin);
	s1 = t2s_B((void*)b1);
	s2 = t2s_B((void*)b2);
	h1 = t2h_B((void*)b1);
	h2 = t2h_B((void*)b2);
	printf("t2s_B(1):%s: %s\n",s1,h1);
	printf("t2s_B(2):%s: %s\n",s2,h2);
	printf("cross: %d\n",cross_B((void*)b1,(void*)b2));
	free((void*)b1->xmin);
	free((void*)b1->xmax);
	free((void*)b1);
	free((void*)b2->xmin);
	free((void*)b2->xmax);
	free((void*)b2);
	free((void*)s1);
	free((void*)s2);
	free((void*)h1);
	free((void*)h2);
	return 0;
}
