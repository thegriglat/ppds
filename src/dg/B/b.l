%{
#include <stdio.h>
#include <stdlib.h>

/**/
#include "y.tab.h"
/**/

#include "settings.h"

#include "dg/B.h"

int beam_lex_error ;

%}

%%

\+{0,1}([0-9]*\.[0-9]+|[0-9]+\.{0,1}[0-9]*|[0-9]+)([Ee][-\+]{0,1}[0-9]+){0,1} {
	DEBUG("lex: num: %s ret NUM",beamtext);
	beamlval.num = atof(beamtext);
	return NUM;
}

[ \t\n] { DEBUG("skip ' '"); }

-{1,2} { DEBUG("'-'"); return '-'; }

\, { DEBUG("','"); return ','; }

TEV  { DEBUG("TEV" ); beamlval.id = TEV;  return UNIT; }
GEV  { DEBUG("GEV" ); beamlval.id = GEV;  return UNIT; }
MEV  { DEBUG("MEV" ); beamlval.id = MEV;  return UNIT; }
GEV2 { DEBUG("GEV2"); beamlval.id = GEV2; return UNIT; }
MEV2 { DEBUG("GEV2"); beamlval.id = MEV2; return UNIT; }
EV   { DEBUG("EV"  ); beamlval.id = EV;   return UNIT; }
EV2  { DEBUG("EV2" ); beamlval.id = EV2;  return UNIT; }

F-2  { DEBUG("F-2" ); beamlval.id = F_2;  return UNIT; }

ECM     { DEBUG("ECM");    beamlval.id = ECM;   return KIN; }
PLAB\/N { DEBUG("PLAB_N"); beamlval.id = PLAB_N;return KIN; }
PLAB    { DEBUG("PLAB");   beamlval.id = PLAB;  return KIN; }
ELAB\/N { DEBUG("ELAB_N"); beamlval.id = ELAB_N;return KIN; }
ELAB    { DEBUG("ELAB");   beamlval.id = ELAB;  return KIN; }
TLAB\/N { DEBUG("TLAB_N"); beamlval.id = TLAB_N;return KIN; }
TLAB    { DEBUG("TLAB");   beamlval.id = TLAB;  return KIN; }
NU      { DEBUG("NU");     beamlval.id = NU;    return KIN; }
Q2      { DEBUG("Q2");     beamlval.id = Q2;    return KIN; }
S       { DEBUG("S" );     beamlval.id = S ;    return KIN; }
W       { DEBUG("W" );     beamlval.id = W ;    return KIN; }
W2      { DEBUG("W2");     beamlval.id = W2;    return KIN; }

\<   { DEBUG("<"); return '<'; }
\>   { DEBUG(">"); return '>'; }

\(   { DEBUG("("); return '('; }
\)   { DEBUG(")"); return ')'; }

.    { ERROR("beam: spurios '%c'",*beamtext); return B_LEX_ERROR; }



%%
int beamwrap(void) {
	return 1;
}


/**
int  main(void) {
	beamlex();
	return 0;
}
**/
