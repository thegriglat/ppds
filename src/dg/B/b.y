%{
#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include <math.h>
#include "float.h"

/*#include "dg/tree.h"*/

#include "settings.h"

#include "dg/B.h"

extern
int beam_lex_error;

#define BITSHIFT 5

static
struct type_B *blist = 0;
    
static
char *s_kinvars[] =  {
"ECM",
"PLAB/N",
"PLAB",
"ELAB/N",
"ELAB",
"TLAB/N",
"TLAB",
"NU",
"Q2",
"S",
"W",
"W2",
0
};

static
char *s_units[] = {
"TEV",
"GEV",
"GEV2",
"MEV",
"MEV2",
"EV",
"EV2",
"F-2",
0
};


void
add_range(struct type_B* b, const float xmin, const float xmax) {
    float *xmi = (float*)realloc(b->xmin, sizeof(float)*(b->n_range + 1));
    float *xma = (float*)realloc(b->xmax, sizeof(float)*(b->n_range + 1));
    if (xmi && xma){
        b->xmin = xmi;
        b->xmax = xma;
    } else {
        printf("Cannot allocate memory");
        exit(1);
    };
    b->xmin[b->n_range] = xmin;
    b->xmax[b->n_range] = xmax;
    (b->n_range)++;
}

/* lex stuff: */

typedef struct beambuffer_state *BEAM_BUFFER_STATE;

extern
BEAM_BUFFER_STATE
beam_scan_string (const char*);

extern
void
beam_delete_buffer (BEAM_BUFFER_STATE);

extern 
int 
yylex();

/***/



void
yyerror(char*);

%}

%token NUM UNIT KIN B_LEX_ERROR

%union r_union_type {
        int id;
    float num;
};


%%

expr:
    list UNIT '(' KIN ')' { 
        DEBUG("expr: %d %d",$<id>2,$<id>4); 
        blist->unit = $<id>2 ; 
    blist->kin = $<id>4; 
    DEBUG("list u:%d (v:%d)",blist->unit,blist->kin); 
    }
    |
    B_LEX_ERROR { ERROR("lexical error in beam"); return 1; }
    ;

list:
    range { DEBUG("list = range"); }
    |
    range ',' list { DEBUG("list := range, list"); }
    ;

range:
     NUM         { DEBUG("range: %f",$<num>1);               add_range(blist,$<num>1,$<num>1);  }
     |
     '-' NUM     { DEBUG("range: %f",-1 * $<num>2);          add_range(blist,-1 * $<num>2, -1 * $<num>2);  }
     |
     NUM '-' NUM { DEBUG("range: %f - %f", $<num>1,$<num>3); add_range(blist,$<num>1,$<num>3); }
     |
     '<' NUM     { DEBUG("range: < %f",$<num>2);             add_range(blist,FLT_MIN,$<num>2); }
     |
     '>' NUM     { DEBUG("range: > %f",$<num>2);             add_range(blist,$<num>2,FLT_MAX); }
     |
     ;




%%

void
beamerror(char* s) {
    ERROR("BEAM: %s",s);
}

void convert2eV(struct type_B *b){
    float scale = 1.;
    if (b->unit == TEV) {
        scale = 1e12;
        b->unit = EV;
    }
    else if (b->unit == GEV) {
        scale = 1e9;
        b->unit = EV;
    }
    else if (b->unit == MEV) {
        scale = 1e6;
        b->unit = EV;
    }
    else if (b->unit == GEV2) {
        scale = 1e18;
        b->unit = EV2;
    }
    else if (b->unit == MEV2) {
        scale = 1e12;
        b->unit = EV2;
    }
    if (scale != 1.) {
        int i;
        for (i = 0; i < b->n_range; i++) {
            if (b->xmin[i] != FLT_MIN)
                b->xmin[i] *= scale;
            if (b->xmax[i] != FLT_MAX)
                b->xmax[i] *= scale;
        }
    }
}

void normalizeUnits(struct type_B *b){
    float scale = 1.;
    // b->unit should be EV or EV2 or F_2
    int i;
    float min_val = FLT_MAX;
    enum units new_unit = b->unit;
    for (i = 0; i < b->n_range; ++i){
        if (b->xmin[i] > FLT_MIN && b->xmin[i] < min_val)
            min_val = b->xmin[i];
    }
    if (min_val == FLT_MAX)
        // all xmin are FLT_MIN
        for (i = 0; i < b->n_range; ++i){
            if (b->xmax[i] < min_val)
                min_val = b->xmax[i];
        }
    if (b->unit == EV){
        if (min_val > 1e12){
            scale = 1e-12;
            new_unit = TEV;
        };
        if (min_val >= 1e9 && min_val < 1e12){
            scale = 1e-9;
            new_unit = GEV;
        };
        if (min_val >= 1e6 && min_val < 1e9){
            scale = 1e-6;
            new_unit = MEV;
        }
    } else if (b->unit == EV2){
        if (min_val >= 1e18){
            scale = 1e-18;
            new_unit = GEV2;
        };
        if (min_val >= 1e12 && min_val < 1e18){
            scale = 1e-12;
            new_unit = MEV2;
        }
    } else if (b->unit != F_2){
        ERROR("Unsupported unit: %s", s_units[b->unit]);
        exit(1);
    }
    if (scale != 1.){
        b->unit = new_unit;
        for (i = 0; i < b->n_range; ++i){
            if (b->xmin[i] != FLT_MIN)
                b->xmin[i] *= scale;
            if (b->xmax[i] != FLT_MAX)
                b->xmax[i] *= scale;
        }
    }
}

void*
s2t_B (char* s) {
    BEAM_BUFFER_STATE buffer;
    int ret;
    blist = (struct type_B*)malloc(sizeof(struct type_B));
    if(!blist){
        printf("Cannot allocate memory\n");
        exit(1);
    }
    blist->n_range = 0;
    blist->xmin = blist->xmax = NULL;
    buffer = beam_scan_string(s);
    ret = beamparse();
    beam_delete_buffer(buffer);
    if (ret != 0) {
        ERROR("syntax error in KEY = B: %s",s);
        return 0;
    }
    convert2eV(blist);
    return (void*)blist;
}


char*
t2s_B (void* t) {
    struct type_B *b = (struct type_B*) t;
    char tmp[1024];
    char *s = 0;
    int i, l = 0;
    if (!b) {
        s = (char*)malloc(sizeof(char)*(6+1));
        strcpy(s,"[null]");
        return s;
    }
    normalizeUnits(b);
    for (i = 0; i < b->n_range; i++) {
        if (b->xmin[i] <= FLT_MIN)
            sprintf(tmp,"%c < %f", (i == 0 ? ' ' : ','), b->xmax[i]);
        else if (b->xmax[i] >= FLT_MAX)
            sprintf(tmp,"%c > %f", (i == 0 ? ' ' : ','), b->xmin[i]);
        else if (b->xmin[i] == b->xmax[i]) 
            sprintf(tmp,"%c %f", (i == 0 ? ' ' : ','),b->xmin[i]);
        else 
            sprintf(tmp,"%c %f - %f", (i == 0 ? ' ' : ','),b->xmin[i], b->xmax[i]);
        l = ( s ? strlen(s) : 0 );
        char *tt = (char*)realloc(s,sizeof(char)*(l+strlen(tmp)+1));
        if (tt){
            s = tt;
            strcpy(s+l,tmp);
        } else {
            free(tt);
        }
    }
    sprintf(tmp," %s (%s)",s_units[(int)b->unit], s_kinvars[(int)b->kin]);
    l = ( s ? strlen(s) : 0 );
    char *tt = (char*)realloc(s,sizeof(char)*(l+strlen(tmp)+1));
    if (tt){
        s = tt;
        strcpy(s+l,tmp);
    } else {
        free(tt);
    }
    convert2eV(b);
    return s;
}

extern
char* s2hash(char*);

unsigned char getNthByte(long long v, unsigned char byte){
    return (v >> (8 * byte)) & 0xff;
}

// after fill first 8 bytes of the hash with long long mask
// need to check that hash string is not broken as now there are null bytes in the string (at the start)
char*
t2h_B(void *t) {
  char *s = t2s_B(t);
  char *h = s2hash(s);
  /*
  long long magicnum = encodeBeam(p); // 8 bytes
  int i;
  for (i = 0; i < 8; ++i){
    // fill first 8 bytes of hash with magic number
    h[i] = getNthByte(magicnum, i);
  }
  */
  free((void*)s);
  return h;
}

int
eq_B (void* t1, void *t2) {
    char *h1 = t2h_B(t1);
    char *h2 = t2h_B(t2);
    int eq = 0;
    if (strcmp(h1,h2) == 0)
        eq = 1;
    free(h1);
    free(h2);
    return eq;
}

int 
cross_range(float xmin1, float xmax1, float xmin2, float xmax2) {
    if (xmax1 < xmin2 || xmax2 < xmin1)
        return 0;
    return 1;
}

int
cross_B (void *t1, void *t2) {
    struct type_B *b1 = (struct type_B*) t1;
    struct type_B *b2 = (struct type_B*) t2;
    int i, j;
    if (!b1 && !b2)
        return 1;
    if (!b1 || !b2)
        return 0;
    if (b1->unit != b2->unit || b1->kin != b2->kin)
        return 0;
    for (i = 0; i < b1->n_range; i++)
        for (j = 0; j < b2->n_range; j++)
            if (cross_range(b1->xmin[i],b1->xmax[i], b2->xmin[j],b2->xmax[j]))
                return 1;
    return 0;
}

void setBit(long long *val, const unsigned char bitpos){
    *val |= (1ULL << bitpos);
}

long long checkBit(const long long val, const unsigned char bitpos){
    return val & (1ULL << bitpos);
}

/*
  Bit mask for beams
    0    : sign
    1-4  : B kinvars 
    5-63 : value log2 interval
  
*/

long long encodeBeamCondition(const float xmin, const float xmax, enum kinvars k){
    long long r = 0;
    // sign bit
    if (xmin < 0 || xmax < 0)
         setBit(&r, 0);
    // kinvars, shift k index by 1 bit and apply, only 4 bits are matter
    r = r | ((int)k << 1);
    const char bitshift = 4 + 1;
    unsigned char bitlow = 0, bithigh = 63;
    if (xmin == FLT_MIN){
        // < 100
        bithigh = getMaskBit(xmax);
    }
    if (xmax == FLT_MAX){
        // > 100
        bitlow = getMaskBit(xmin);
    }
    if (xmax != FLT_MAX && xmin != FLT_MIN){
        // 5-100
        // 5
        bitlow = getMaskBit(xmin);
        bithigh = getMaskBit(xmax);
    }
    bithigh = (bithigh > 63 - bitshift) ? 63 - bitshift: bithigh;
    bitlow = (bitlow > 63 - bitshift) ? 63 - bitshift: bitlow;
    int i;
    for (i = bitlow; i <= bithigh; ++i)
        setBit(&r, i + bitshift);
    return r;
}

long long encodeBeam(struct type_B* b){
    long long result = 0;
    convert2eV(b);
    int i;
    for (i = 0; i < b->n_range; ++i)
        result = result | encodeBeamCondition(b->xmin[i], b->xmax[i], b->kin);
    return result;
}


void printBits(long long v){
    int i;
    for (i = sizeof(long long) * 8 - 1; i >= 0; --i){
        if (checkBit(v, i))
            printf("1");
        else
            printf("0");
        if (i % 4 == 0)
            printf(" ");
    }
    // printf("\n");
}

// TODO: does not work with huge energy like "10000000 TEV (PLAB)"
// long long overflow
unsigned char getMaskBit(float value){
    // which bit should be set for value in case log2 scale
    value = (value > 0) ? value : -value;
    unsigned char bit = 0;
    while (value > 1){
        value /= 2.;
        bit++;
    }
    return bit;
}

void
free_B(void* t) {
    struct type_B *b = (struct type_B*) t;
    if (!b)
        return;
    if (b->xmin)
        free(b->xmin);
    if (b->xmax)
        free(b->xmax);
    free(t);
    return ;
}

struct type_B** _B;
unsigned int n_B = 0;

char lowestBit(const long long v, unsigned char start){
    int i;
    for (i = start; i < 64; ++i)
        if (checkBit(v, i) != 0)
            return i;
    return 0;
}

char highestBit(const long long v){
    int i;
    for (i = 63; i >= 0; --i)
        if (checkBit(v, i) != 0)
            return i;
    return 0;
}

char getNbits(const long long v, unsigned char start){
    int i;
    char n = 0;
    for (i = start; i < 64; ++i){
        if (checkBit(v, i) != 0)
            n++;
    }
    return n;
}

int Bcmp(const void* a, const void* b){
    // sort by lowest set bit
    struct type_B *ab = *(struct type_B**)a;
    struct type_B *bb = *(struct type_B**)b;
    const unsigned long long v1 = encodeBeam(ab);
    const unsigned long long v2 = encodeBeam(bb);
    int r = lowestBit(v1, BITSHIFT) - lowestBit(v2, BITSHIFT);
    if (r != 0)
        return r;
    else {
        int nb1 = getNbits(v1, BITSHIFT);
        int nb2 = getNbits(v2, BITSHIFT);
        if (nb1 - nb2 != 0)
            return nb1 - nb2;
        else
            return (v1 > v2) ? 1 : -1;
    }
    return 0;
}

void sort_B(){
    if (!_B)
        return;
    qsort(_B, n_B, sizeof(struct type_B**), Bcmp);
}

// load _B from file
void load_B(){
    if (_B)
        return;
    struct type_B *b1 = 0;
    char *base = 0, *val_file_path, *hash_file_path;
    char hash[64], val[1024], *p, c;
    FILE *fv = 0;
    int pos, pos_end, pct, pct_prev, found = 0;
    if (!(base = getenv("PPDS_BASE")))
        base = ".";
    val_file_path = (char*)malloc(sizeof(char)*(strlen(base) + 132));
    hash_file_path = (char*)malloc(sizeof(char)*(strlen(base) + 132));
    sprintf(val_file_path,"%s/B/v",base);
    sprintf(hash_file_path,"%s/B/h",base);
    if ((fv = fopen(val_file_path,"r")) == 0) {
        ERROR("can't open %s",val_file_path);
        exit(1);
    }
    fseek(fv,0,SEEK_END);
    pos_end = ftell(fv);
    fseek(fv,0,SEEK_SET);
    pct_prev = 0;
    while (fscanf(fv,"%s",hash) == 1) {
        for (p = val; (c = fgetc(fv));)
            if (c != '\n')
                *p ++ = c;
        *p = '\0';
        if((b1 = (struct type_B*)s2t_B(val)) == 0) {
            ERROR("syntax error (ignored) in KEY = B: %s",val);
            free(b1);
            continue;
        }
        struct type_B ** tmp = 0;
        if (!_B)
            tmp = (struct type_B**)malloc(sizeof(struct type_B*) * 1);
        else
            tmp = (struct type_B**)realloc(_B, sizeof(struct type_B*) * (n_B + 1));
        if (tmp){
            _B = tmp;
            _B[n_B] = b1;
            n_B++;
        } else {
            printf("Cannot allocate memory");
            free(tmp);
            exit(1);
        }
        pos = ftell(fv);
        pct = 100*pos/pos_end;
        if (pct != pct_prev) {
            ERROR("%d%% beams read, %d found\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b",
                    pct, found
            );
            pct_prev = pct;
        }
    }
    sort_B();
    fclose(fv);
    free(val_file_path);
    free(hash_file_path);
}


void dump_B(){
    load_B();
    FILE* fo = fopen("B_v", "w");
    int i;
    for (i = 0; i < n_B; ++i){
        char *p;
        char * vhash = t2h_B(_B[i]);
        char * vval = t2s_B(_B[i]);
        for (p = vhash; *p; p++)
                fputc(*p,fo);
            fputc(' ',fo);
            for (p = vval; *p; p++)
                fputc(*p, fo);
            fputc('\0',fo);
            fputc('\n',fo);
        printBits(encodeBeam(_B[i]));
        printf("\t%s %s\n", vhash, vval);
    }
    fclose(fo);
}


struct type_B* getCurrentRecord(FILE *f, unsigned long pos, unsigned long *prevpos, unsigned long *nextpos){
    fseek(f, pos, SEEK_SET);
    char c;
    unsigned long cpos = pos;
    // read back to find start of record
    while ((c = fgetc(f))){
        cpos--;
        fseek(f, cpos, SEEK_SET);
    }
    char val[1024];
    char *p;
    p = val;
    // current pos is \0, use next
    unsigned long ppos = ftell(f) - 2;
    cpos++;
    fseek(f, cpos, SEEK_SET);
    char hash[64];
    if (fscanf(f, "%s", hash) != 1) {
    	ERROR("dg/B/b.y: getCurrentRecord(): fscanf(f, ... hash) != 1. EXIT(1)");
	exit(1);
    }
    while ((c = getc(f))){
        if (c != '\n'){
            *p++ = c;
        }
    }
    *p = '\0';
    struct type_B *ret = s2t_B(val);
    unsigned long npos = ftell(f);
    fseek(f, pos, SEEK_SET);
    if (prevpos){
        *prevpos = ppos;
    }
    if (nextpos){
        *nextpos = npos;
    }
    return ret;
}

void search_B(struct type_B *b){
    // check
    // load_B();
    // int j;
    // int found_simple = 0;
    // for (j = 0; j < n_B; ++j){
    //     if (cross_B(b,_B[j])){
    //         found_simple++;
    //     }
    // }
    // end check
    char *base = 0, *val_file_path;
    FILE *f = 0;
    if (!(base = getenv("PPDS_BASE")))
        base = ".";
    val_file_path = (char*)malloc(sizeof(char)*(strlen(base) + 132));
    sprintf(val_file_path, "%s/B/v", base);
    if ((f = fopen(val_file_path, "r")) == 0) {
        ERROR("can't open %s", val_file_path);
        exit(1);
    }
    free(val_file_path);
    struct type_B *b1 = NULL;
    fseek(f, 0, SEEK_END);
    unsigned long fsize = ftell(f);
    unsigned long last = fsize - 3; // first half
    unsigned long first = 0;
    fseek(f, 0, SEEK_SET);
    // find start of lowest bit
    const char vlatest = lowestBit(encodeBeam(b), BITSHIFT);
    const char vhighest = highestBit(encodeBeam(b));
    do {    
        last = first + (last - first) / 2.;
        b1 = getCurrentRecord(f, last, 0, 0);
        // printf("pos: %lu \t %s\n", last, t2h_B(b1));
    } while (lowestBit(encodeBeam(b1), BITSHIFT) > vlatest);
    struct type_B ** _B = NULL;
    unsigned int n_B = 0;
    int readed = 0;
    // go forward
    unsigned long prevpos = last;
    while (lowestBit(encodeBeam(b1), BITSHIFT) <= vhighest && last < fsize - 4){
        b1 = getCurrentRecord(f, last, 0, &last);
        readed++;
        // printf("forward: %s %s\n", t2h_B(b1), t2s_B(b1));
        if (!_B)
            _B = (struct type_B**)malloc(sizeof(struct type_B*));
        else {
            struct type_B** tmp = (struct type_B**)realloc(_B, sizeof(struct type_B*) * (n_B + 1));
            if (tmp){
                _B = tmp;
                _B[n_B] = b1;
                n_B++;
            } else{
                printf("Cannot allocate memory.\n");
                exit(1);
            }
        }
    }
    // go back until end of file
    unsigned long pprevpos = 0;
    while (pprevpos != prevpos){
        pprevpos = prevpos;
        // continuous read back file with the same lowest bit
        b1 = getCurrentRecord(f, prevpos, &prevpos, 0);
        readed++;
        if (!_B)
            _B = (struct type_B**)malloc(sizeof(struct type_B*));
        else {
            struct type_B** tmp = (struct type_B**)realloc(_B, sizeof(struct type_B*) * (n_B + 1));
            if (tmp){
                _B = tmp;
                _B[n_B] = b1;
                n_B++;
            } else{
                printf("Cannot allocate memory.\n");
                exit(1);
            }
        }
    }
    fclose(f);
    int i;
    int found = 0;
    for (i = 0; i < n_B; ++i){
        if (cross_B(b,_B[i])){
            printf("%s\n", t2h_B(_B[i]));
            found++;
        }
        free(_B[i]);
    }
    ERROR("found: %d records by %d reads\n", found, readed);
    free(_B);
}


