#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "settings.h"




static
char
*base = NULL,
*key  = NULL;


void
hash1(FILE *fh, int *pos1,   char* hash_fh) {
	int rec;
	for ( fseek(fh,*pos1,SEEK_SET);;) {
		if (*pos1 == 0)
			break;
		char c;
		if ((c = fgetc(fh)) == '\n') {
			DEBUG("%ld - 1: c == 0, break",ftell(fh));
			break;
		}
		DEBUG("hash1: p: %ld - 1: %c",ftell(fh),c);
		fseek(fh,-2,SEEK_CUR);
		*pos1 = ftell(fh);
	}
	if (fscanf(fh,"%s %d",hash_fh,&rec) != 2) {
		ERROR("dump_record: can't read {hash,rec} from the record index");
		exit (1);
	}
}

void
hash2(FILE *fh, int *pos2, int pos_end, char *hash_fh) {
	char c;
	for ( fseek(fh,*pos2,SEEK_SET);;) {
		if (*pos2 >= pos_end) {
			break;
		}
		if ((c = fgetc(fh)) == '\n') {
			DEBUG("%ld - 1: c == 0, break",ftell(fh));
			break;
		}
		*pos2 = ftell(fh);
		DEBUG("hash2: %d - 1: %c",*pos2,c);
	}
	if (*pos2 == pos_end) {
		for (;;) {
			if (*pos2 == 0)
				break;
			if ((c = fgetc(fh)) == '\n') {
				DEBUG("%ld - 1: c == 0, break",ftell(fh));
				break;
			}
			DEBUG("p: %ld - 1: %c",ftell(fh),c);
			fseek(fh,-2,SEEK_CUR);
			*pos2 = ftell(fh);
		}
	}
}

void
record_from_hash(FILE *fh, char* hash) {
	char h1[64],h2[64], hm[64], c;
	int rec, pos, pos1, pos2, posm, pos_end, cmp, nn;
	DEBUG("record_from_hash(...)");
	fseek(fh,0L,SEEK_SET);
	pos1 = ftell(fh);
	fseek(fh,0L,SEEK_END);
	pos_end = pos2 = ftell(fh);
	DEBUG("%s/%s/h: 1,2: %d %d",base,key,pos1,pos2);
	for (;;) {
		hash1(fh, &pos1, h1);
		DEBUG("pos1: %d hash: %s cmp: %d",pos1,h1, strcmp(hash,h1));
		if ((cmp = strcmp(hash,h1)) < 0) {
			ERROR("no %s in the record index",hash);
			exit(1);
		}
		if (cmp == 0) {
			fseek(fh,pos1,SEEK_SET);
			if (fscanf(fh,"%s %d",h1,&rec) != 2) {
				ERROR("can't read {hash,rec} from the record index");
				exit(1);
			}
			DEBUG("found: %s == %s rec: %d",hash,h1,rec);
			break;
		}
		hash2(fh,&pos2, pos_end, h2);
		if (fscanf(fh,"%s %d",h2,&rec) != 2) {
			ERROR("dump_record: can't read {hash,rec} from the record index");
			exit (1);
		}
		DEBUG("pos2: %d hash: %s rec: %d cmp: %d",pos2,h2,rec, strcmp(hash,h2));
		if ((cmp = strcmp(hash,h2)) > 0) {
			ERROR("INFO: no %s in the record file. Either element was never encoded or 'hash -> record' file is broken.\n",hash);
			exit(1);
		}
		if (cmp == 0) {
			DEBUG("found: %s == %s rec: %d",hash,h2,rec);
			break;
		}
		posm = (pos1 + pos2) / 2;
		hash1(fh, &posm, hm);
		if (fscanf(fh,"%s %d",hm,&rec) != 2) {
			ERROR("dump_record: can't read {hash,rec}");
			exit (1);
		}
		DEBUG("posm: %d hash: %s rec: %d cmp: %d",posm,hm,rec, strcmp(hash,hm));

		if ((cmp = strcmp(hash,hm)) < 0) {
			DEBUG("pos1 = %d , pos2 = posm = %d",pos1,posm);
			pos2 = posm;
		}
		else if (cmp > 0) {
			DEBUG("pos1 = posm = %d, pos2 = %d",posm,pos2);
			pos1 = posm;
		}
		else {
			DEBUG("found: %s == %s rec: %d", hash, hm, rec);
			break;
		}
	}
	if (rec == -1) {
		ERROR("can't find %s",hash);
		exit(1);
	}
	DEBUG("hash: %s rec: %d", hash,rec);
	for (;;) {
		for (nn = 0, fseek(fh,-1,SEEK_CUR);  ; fseek(fh,-2,SEEK_CUR)) {
			if ((pos = ftell(fh)) == 0L)
				break;
			if ((c = fgetc(fh)) == '\n')
				if (++nn == 2)
					break;
		}
		if (fscanf(fh,"%s %d",h1,&rec) != 2) {
			ERROR("dg/hash2record.c: record_from_hash(): fscanf(fh, h1,&rec) != 2); EXIT(1)");
			exit(1);
		}
		DEBUG("pos: %d {h,r}: %s %d",pos,h1,rec);
		if (strcmp(h1,hash) != 0) {
			DEBUG("break");
			while (fgetc(fh) != '\n');
			break;
		}
		else {
			if (pos > 0) {
				DEBUG("fh -> -1");
				fseek(fh,-1,SEEK_CUR);
			}
			else {
				printf("%d\n",rec);
				break;
			}
		}
	}
	for (; fscanf(fh,"%s %d",h1,&rec) == 2 && strcmp(h1,hash) == 0;)
		printf("%d\n",rec);
	return;
}



int
main(int argc, char** argv) {
	char r_file_path[1024], hash[64];
	FILE *fr = 0;

	if (argc < 2) {
		printf("\nUsage: %s key hash\n\n",argv[0]);
		return 1;
	}
	if (!base) {
		if (!(base = getenv("PPDS_BASE"))) {
			/*WARNING("no PPDS_BASE env. variable, using ./");*/
			base = ".";
		}
		else {
			DEBUG("PPDS_BASE = %s\n",base);
		}
	}
	key = argv[1];
	sprintf(r_file_path,"%s/%s/r",base,key);
	if ((fr = fopen(r_file_path,"r")) == 0) {
		ERROR("%s: can't open %s",argv[0],r_file_path);
		return 1;
	}
	while (scanf("%s",hash) == 1)
		record_from_hash(fr,hash);
	fclose(fr);
	return 0;
}
