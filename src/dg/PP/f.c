#include "dg/PP.h"
#include "settings.h"
#include "util.h"

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "sqlite.h"

static
int n_PP = 0;

static
struct type_PP * _PP = NULL;


void
vy_init_PP (void)
{
    char *vy_base = 0, *vy_fn = 0;
    FILE *vy_f = 0;
    if (_PP) {
        return;
    }
    char *line = (char*)malloc(sizeof(char) * 1024);
    if (!(vy_base = getenv("PPDS_VY"))) {
        ERROR(
                "vy_init_PP(): PPDS_VY env. pointing to the "
                "vocabulary directory is not defined [EXIT]\n"
               );
        exit(1);
    }
    vy_fn = (char *)malloc(sizeof(char)*(strlen(vy_base) + 1 + 2 + 1 + 2 + 1));
    sprintf(vy_fn,"%s/PP/vy",vy_base);
    DEBUG(" reading particle definitions from PPDS_VY = %s",vy_fn);
    if (!(vy_f = fopen(vy_fn,"r"))) {
        ERROR("vy_init_PP(): can't open %s [EXIT]",vy_fn);
        exit(1);
    }
    free(vy_fn);
    for (n_PP = 0; fgets(line, 1024, vy_f) != NULL; n_PP++) {
        if (!_PP) {
            _PP = (struct type_PP *) malloc(sizeof(struct type_PP));
        } else {
            struct type_PP *tmp = (struct type_PP*)realloc(_PP, sizeof(struct type_PP) * (n_PP + 1));
            if (tmp) {
                _PP = tmp;
            } else {
                printf("Cannot allocate memory.");
                exit(0);
            }
        }
        _PP[n_PP].nval = 0;
        // split line to **val
        int nval = 0;
        char * word;
        char ** val = NULL;
        for (nval = 0; (word = strtok_r(line, " \n", &line)); nval++) {
            if (!val)
                val = (char**)malloc(sizeof(char*));
            else {
                char ** tmp = (char**)realloc(val, sizeof(char*) * (nval + 1));
                if (tmp) {
                    val = tmp;
                } else {
                    printf("Cannot allocate memory.\n");
                    exit(0);
                }
            }
            val[nval] = (char*)malloc(sizeof(char) * (strlen(word) + 1));
            strcpy(val[nval], word);
        }
        _PP[n_PP].val = val;
        _PP[n_PP].nval = nval;
    }
    fclose(vy_f);
}

void *
s2t_PP (char *s)
{
    if (!s) {
        WARNING("s2t_PP( null )");
        return NULL;
    }
        const char *sql = "select ABBREVIATION from vy_PP where ID = (SELECT ID FROM vy_PP where ABBREVIATION = ?);";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(
        get_db(),
        sql, -1,
        &stmt,
        0
         ) != SQLITE_OK){
        ERROR("Cannot prepare SQL: %s", sql);
        exit(1);
    }
    if (sqlite3_bind_text(stmt, 0, s, strlen(s), SQLITE_STATIC) != SQLITE_OK){
        ERROR("Cannot bind text '%s'", s);
        exit(1);
    }
    struct type_PP *result = (struct type_PP*)malloc(sizeof(struct type_PP));
    result->nval = 0;
    do {
        const int step = sqlite3_step(stmt);
        if (step == SQLITE_ROW){
            // sqlite3_column_test return const unsigned char *
            const char* value = (char*) sqlite3_column_text(stmt, 0);
            result->val = (char**)realloc(result->val, result->nval + 1);
            result->val[result->nval] = (char*)malloc(sizeof(char) * (strlen(value) + 1));
            strcpy(result->val[result->nval], value);
            result->nval++;
        } else {
            ERROR("Cannot find values in vy_PP for input '%s'", s);
            exit(1);
        }
    } while (1);
    return result;
}

char*
t2s_PP(void *t)
{
    struct type_PP *p = (struct type_PP *) t;
    return p->val[0];
}

char*
t2h_PP(void *t)
{
    struct type_PP *p = (struct type_PP *) t;
    return s2hash(p->val[0]);
}

int
eq_PP (void *t1,void *t2)
{
    int i;
    struct type_PP *a = (struct type_PP*) t1;
    struct type_PP *b = (struct type_PP*) t2;
    for (i = 0; i < a->nval; ++i) {
        int j;
        for (j = 0; j < b->nval; ++j) {
            if (strcmp(a->val[i], b->val[j]) == 0)
                return 1;
        }
    }
    return 0;
}