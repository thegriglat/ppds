%{
#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include "util.h"
#include "dg/R.h"
#include "sqlite.h"

/**/
// #include "y.tab.h"
/**/

#include "settings.h"

int isValidR = 0;

int R_lex_error ;

int check_Rtoken(char* token);

%}

%%

^(?i:arxiv.*) {
    isValidR = 1;
}

^(?i:[a-z]+.*) {
    char *abbr = strtok_r(Rtext, " ,-", &Rtext);
    if (check_Rtoken(abbr))
        isValidR = 1;
    else
        isValidR = 0;
}

%%


int Rwrap(void) {
    return 1;
}

extern
YY_BUFFER_STATE
R_scan_string (const char*);

extern
void
R_delete_buffer (YY_BUFFER_STATE);

int
check_R(char *s){
    YY_BUFFER_STATE buffer;
    buffer = R_scan_string(s);
    Rlex();
    R_delete_buffer(buffer);
    return isValidR;
};

int check_Rtoken(char* s)
{
    if (!s) {
        WARNING("s2t_RR( null )");
        return 0;
    }
    const char *sql = "select count(*) from vy_R where ABBREVIATION = ?;";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(
        get_db(),
        sql, -1,
        &stmt,
        0
         ) != SQLITE_OK){
        ERROR("Cannot prepare SQL: %s", sql);
        exit(1);
    }
    if (sqlite3_bind_text(stmt, 1, s, strlen(s), SQLITE_STATIC) != SQLITE_OK){
        ERROR("Cannot bind text '%s'", s);
        exit(1);
    }
    int step = sqlite3_step(stmt);
    if (step != SQLITE_ROW){
        ERROR("Database error.");
        exit(1);
    }
    const int count = sqlite3_column_int(stmt, 0);
    sqlite_stmt_free(stmt);
    return (count == 0) ? 0 : 1;
}

/*
int  main(int argc, char** argv) {
    struct type_RFULL *a = s2t_RFULL(argv[1]);
    if (!a){
        printf("article is NULL\n");
    }
    printf("OUTPUT:\ntype: %d\nval: %s\n", a->type, a->val);
    return 0;
}
*/
