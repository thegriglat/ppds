%{
#include <stdio.h>
#include <stdlib.h>
#include "string.h"
#include "util.h"
#include "dg/RR.h"
#include "sqlite.h"

/**/
// #include "y.tab.h"
/**/

#include "settings.h"

int isValidRR = 0;

int RR_lex_error ;

int check_RR_token(char *s);

%}

%%

^(?i:arxiv.*|hepex-[0-9]+([.][0-9]+)?) {
    isValidRR = 1;
}

^(?i:(A|AIN|E|EIN|GEN|MORE|MORL|PC|PCIN|PS|PSBY|R|RIN|S|SBY|SEE)[ *]([\n]|.)*) {
    char* token = strtok_r(RRtext, " *\n", &RRtext);
    if (!token) {
        ERROR("Cannot extract token from '%s'", RRtext);
        exit(1);
    }
    if (check_RR_token(token))
        isValidRR = 1;
    else
        isValidRR = 0;
}

%%


int RRwrap(void) {
    return 1;
}

extern
YY_BUFFER_STATE
RR_scan_string (const char*);

extern
void
RR_delete_buffer (YY_BUFFER_STATE);

int
check_RR (char *s){
    YY_BUFFER_STATE buffer;
    buffer = RR_scan_string(s);
    RRlex();
    RR_delete_buffer(buffer);
    return isValidRR;
};

int check_RR_token(char *s){
    if (!s) {
        WARNING("s2t_RR( null )");
        return 0;
    }
    const char *sql = "select count(*) from vy_RR where ABBREVIATION = ?;";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(
        get_db(),
        sql, -1,
        &stmt,
        0
         ) != SQLITE_OK){
        ERROR("Cannot prepare SQL: %s", sql);
        exit(1);
    }
    if (sqlite3_bind_text(stmt, 1, s, strlen(s), SQLITE_STATIC) != SQLITE_OK){
        ERROR("Cannot bind text '%s'", s);
        exit(1);
    }
    int step = sqlite3_step(stmt);
    if (step != SQLITE_ROW){
        ERROR("Database error!");
        exit(1);
    }
    const int count = sqlite3_column_int(stmt, 0);
    sqlite_stmt_free(stmt);
    return (count == 0) ? 0 : 1;

}