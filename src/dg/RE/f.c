#include<string.h>
#include<stdlib.h>
#include<stdio.h>

#include "dg/RE.h"
#include "re.h"
#include "util.h"
#include "settings.h"
#include "tree_str.h"

//struct type_RE **r_list;
//int r_list_n = 0;

void
vy_init_RE (void) {
	DEBUG(": vy_init_RE() nothing to do");
  //r_list = (struct type_RE **)malloc(sizeof(struct type_RE*));
}

void *
s2t_RE (char *s) {
	struct type_RE *r = (struct type_RE *)malloc(sizeof(struct type_RE));
	DEBUG("(s:%s) ...",s);
#ifdef _DEBUG_
	int ret;
	ret =
	// dirty hack to avoid -Werror=unused-but-set-variable
#endif
	s2listlist(s, &(r->is), &(r->fs));
	DEBUG(" s2listlist(): ret: %d\n",ret);
	if (_DBG_) {
		if (!r->is) {
 			 DEBUG("is == null");
		}
		else {
			DEBUG("r->is");
			print_listlist(r->is);
		}
		if (!r->fs) {
			DEBUG("fs == null");
		}
		else {
			DEBUG("r->fs");
			print_listlist(r->fs);
		}
		DEBUG ("end of s2t_RE");
		char *s_is, *s_fs;
		s_is = _listlist2s(r->is);
		s_fs = _listlist2s(r->fs);
		DEBUG("listlist2s(r->is): %s", s_is);
		DEBUG("listlist2s(r->fs): %s", s_fs);
		free((void*)s_is);
		free((void*)s_fs);
	}
	DEBUG("return (void*) r ...");
	return (void *)r;
}


char*
t2s_RE(void *t) {
  struct type_RE *r = (struct type_RE *) t;
  char *si, *sf, *sre;
  si = _listlist2s(r->is);
  sf = _listlist2s(r->fs);
  sre = (char*)malloc(sizeof(char)*((si ? strlen(si) : 6) + (sf ? strlen(sf) : 6) + 5 + 1));
  sprintf(sre,"%s --> %s",(si ? si : "[null]"), (sf ? sf : "[null]"));
  free((void*)si);
  free((void*)sf);
  return sre;
}

char*
t2h_RE(void *t) {
  char *sre = 0, *hash = 0;
  DEBUG("t2h_RE");
  sre = t2s_RE(t);
  DEBUG("t2h_RE: t2s_RE:%s:",sre);
  hash = s2hash(sre);
  free((void*)sre);
  return hash;
}

int
eq_RE (void *t1,void *t2) {
  return 0;
}

void
free_RE (void *t) {
	struct type_RE *r = (struct type_RE *) t;
	if (r->is) free_listlist(r->is);
	if (r->fs) free_listlist(r->fs);
	free_re_intermediate();
	free(t);
	return;
}

int
cross_RE(struct type_RE *re1, struct type_RE *re2) {
	// char *h1, *h2;
	int xis, xfs, x;
	if (!re1 || !re2)
		return 0;
	/*
	h1 = t2h_RE(re1); //FIXME: SLOW id2s_P() !!!
	h2 = t2h_RE(re2);
	DEBUG("h1: %s",h1);
	DEBUG("h2: %s",h2);
	if (strcmp(h1,h2) == 0)  {
		x = 1;
	}
	else {
	*/
	if (eq_listlist(re1->is,re2->is)) {
		DEBUG("is1 == is2");
		xis = 1;
	}
	else {
		xis = cross_listlist(re2->is,re1->is);
		DEBUG("<is1|is2>: %d",xis);
	}
	if (eq_listlist(re1->fs,re2->fs)) {
		DEBUG("fs1 == fs2");
		xfs = 1;
	}
	else {
		xfs = cross_listlist(re2->fs,re1->fs);
		DEBUG("<fs1|fs2>: %d",xfs);
	}
	x = xis * xfs;
	/*
	}
	free(h1);
	free(h2);
	*/
	return x;
}

