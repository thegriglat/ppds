#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "re.h"
#include "y.h" /* ../include/ */
#include "dg/RE.h"

#include "sqlite.h"
#include "database.h"

#include "settings.h"

#include "eval_qn.h"

extern
void
vy_init_P(); /* ../dg/P/f.c */

/*
#define _DBG_ (1)
#define DEBUG(...){printf("\nDEBUG : %s, %d : ",__FUNCTION__, __LINE__);printf(__VA_ARGS__);printf("\n");}
*/


int
main (int argc, char** argv) {

	if (argc != 2){
		fprintf(stderr,"Usage: re_search_qn <condition>\n");
		exit(1);
	}
	int r;
	struct type_RE *re;
	int found = 0;
	int is_bar,is_lep,is_S,is_C,is_B,is_T;
	int fs_bar,fs_lep,fs_S,fs_C,fs_B,fs_T;
	double	is_m,is_I3,is_Q;
	double	fs_m,fs_I3,fs_Q;

	vy_init_P();

	const char *sql = "select dg_data.rec_id, dg_value.value from dg_data inner join dg_value on dg_data.vid = dg_value.id where dg_data.tid = (select id from dg_type where type = ?);";
	sqlite3_stmt *stmt;
	if (sqlite3_prepare(get_db(), sql, -1, &stmt, 0) != SQLITE_OK){
		ERROR("Cannot prepare SQL: %s", sql);
		exit(1);
	};
	sqlite3_bind_text(stmt, 1, "RE", 2, SQLITE_STATIC);
	int step = sqlite3_step(stmt);
	fprintf(stderr,"RECORD\tREACTION\n");
	while (step == SQLITE_ROW) {
		is_bar=is_lep=is_S=is_C=is_B=is_T=
		fs_bar=fs_lep=fs_S=fs_C=fs_B=fs_T=-9999;
		is_m=is_I3=is_Q=
		fs_m=fs_I3=fs_Q=-9999.;
		re = (struct type_RE*)malloc(sizeof(struct type_RE));
		const int db_id = sqlite3_column_int(stmt, 0);
		char* buf = (char *)sqlite3_column_text(stmt, 1);
		char* reaction = strtok_r(buf, "|", &buf);
		r = s2listlist(reaction, &(re->is),&(re->fs));
		if (r > 0) {
			ERROR("%s: skip broken reaction: %s",argv[0],buf);
			if (re)
				free_RE(re);
			step = sqlite3_step(stmt);
			continue;
		}
		DEBUG(" \n\n--- buf: %s\n",buf);
		if (re->is) {
			DEBUG("\n");
			_qn_listlist(_DBG_,
				re->is,
				&is_bar,&is_lep,&is_S,&is_C,&is_B,&is_T,
				&is_m,&is_I3,&is_Q
				);
			DEBUG("\n");
		}
		if (re->fs) {
			DEBUG("\n");
			_qn_listlist(_DBG_,
				re->fs,
				&fs_bar,&fs_lep,&fs_S,&fs_C,&fs_B,&fs_T,
				&fs_m,&fs_I3,&fs_Q
				);
			DEBUG("\n");
		}
		DEBUG(
		 "\n"
		 "IS: bar: %d lep: %d S:%d C:%d B:%d T:%d m:%lf I3:%lf Q:%lf\n"
		 "FS: bar: %d lep: %d S:%d C:%d B:%d T:%d m:%lf I3:%lf Q:%lf\n",
		 is_bar,is_lep,is_S,is_C,is_B,is_T, is_m,is_I3,is_Q,
		 fs_bar,fs_lep,fs_S,fs_C,fs_B,fs_T, fs_m,fs_I3,fs_Q
		 );
		free_RE(re);
		if (eval_qn(argv[1], is_bar,is_lep,is_S,is_C,is_B,is_T, is_m,is_I3,is_Q,
		 	    fs_bar,fs_lep,fs_S,fs_C,fs_B,fs_T, fs_m,fs_I3,fs_Q
			   )) {
			printf("%d\t%s\n", db_id, buf);
			found++;
		}
		step = sqlite3_step(stmt);
	}
	sqlite_stmt_free(stmt);
	freeState();
	return 0;
}
