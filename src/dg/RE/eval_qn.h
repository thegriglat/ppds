#ifndef _eval_h_

int
eval_qn(char *statement, int is_bar, int is_lep, int is_S, int is_C, int is_B, int is_T, double is_m, double is_I3, double is_Q,
        int fs_bar, int fs_lep, int fs_S, int fs_C, int fs_B, int fs_T, double fs_m, double fs_I3, double fs_Q
       );

void freeState();

#define _eval_h_
#endif