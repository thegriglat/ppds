#include "stdio.h"
#include "stdlib.h"
#include "string.h"

#include "settings.h"

#include "eval_qn.h"
#include "str.h"
#include "libtcc.h"

const static char header[] =
    "#include <tcclib.h>\n"
    "int compare("
    "int is_bar,"
    "int is_lep,"
    "int is_S,"
    "int is_C,"
    "int is_B,"
    "int is_T,"
    "double is_m,"
    "double is_I3,"
    "double is_Q,"
    "int fs_bar,"
    "int fs_lep,"
    "int fs_S,"
    "int fs_C,"
    "int fs_B,"
    "int fs_T,"
    "double fs_m,"
    "double fs_I3,"
    "double fs_Q){\n"
    "if (\n";

const static char footer[] = "\n) return 1; return 0;};";

static TCCState *s = NULL;

TCCState *getState()
{
    if (s)
        return s;
    s = tcc_new();
    if (!s) {
        ERROR("Could not create tcc state\n");
        exit(1);
    };
    tcc_set_output_type(s, TCC_OUTPUT_MEMORY);
    return s;
}

void freeState()
{
    tcc_delete(getState());
}

void showAvailableVariables()
{
    char text[] =
        "WARNING: Available variables are B, T, C, S, I3, Q, bar, lep for 'is' or 'fs' struct.\n"
        "WARNING: E.g.:  is.S != 0 && is.S == -fs.S";
    fprintf(stderr,"%s\n", text);
}

int isCompiled = 0;
void compile(const char *statement)
{
    if (isCompiled) return;
    if (tcc_compile_string(getState(), statement) == -1) {
        ERROR("TCC compile failed: %s\n", statement);
        showAvailableVariables();
        exit(1);
    };

    if (tcc_relocate(getState(), TCC_RELOCATE_AUTO) < 0) {
        ERROR("TCC relocate failed: %s\n", statement);
        exit(1);
    };
    isCompiled = 1;
}


char *transformStatement(char *statement)
{
    char *p, *ps;
    char *stmp = (char *)malloc(sizeof(char) * (strlen(statement) + 1));
    unsigned int pos = 0, len;
    len = strlen(statement);
    for (p = statement, ps = stmp; *p; ++p, ++pos) {
        if (
            ((*p == 'C' || *p == 'S' || *p == 'T' || *p == 'I' || *p == 'Q') && *(p - 1) == '.') ||
            (pos < len - 2 && *p == 'B' && *(p + 1) != 'a' && *(p + 1) != 'A')
        ) {
            // prevent converting to lower case for some cases
            // _[CSTIQ] _B (note _bar)
            *ps ++ = *p;
            continue;
        }
        if (pos < len - 1 && *p == '.' &&
                !((*(p + 1) >= '0' && *(p + 1) <= '9') || *(p + 1) == ' ')
           ) {
            // . -> _ except [0-9]*.[0-9]*
            *ps++ = '_';
            continue;
        }
        *ps ++ = toLowerChar(*p);
    }
    *ps = '\0';
    return stmp;
}

int eval_qn_tcc(char *statement, int is_bar, int is_lep, int is_S, int is_C, int is_B, int is_T, double is_m, double is_I3, double is_Q,
                int fs_bar, int fs_lep, int fs_S, int fs_C, int fs_B, int fs_T, double fs_m, double fs_I3, double fs_Q
               )
{
    char *program = (char *) malloc(sizeof(char) * (strlen(header) + strlen(footer) + strlen(statement) + 1));
    char *st_t = transformStatement(statement);
    strcpy(program, header);
    strcat(program, st_t);
    strcat(program, footer);
    free(st_t);

    // TCC stuff
    TCCState *st = getState();

    int (*func)(int, int, int, int, int, int, double, double, double, int, int, int, int, int, int, double, double, double);

    compile(program);

    func = tcc_get_symbol(st, "compare");
    free(program);
    return func(is_bar,  is_lep,  is_S,  is_C,  is_B,  is_T,  is_m,  is_I3,  is_Q / 3.f,
                fs_bar,  fs_lep,  fs_S,  fs_C,  fs_B,  fs_T,  fs_m,  fs_I3,  fs_Q / 3.f);
};

int
eval_qn(char *statement, int is_bar, int is_lep, int is_S, int is_C, int is_B, int is_T, double is_m, double is_I3, double is_Q,
        int fs_bar, int fs_lep, int fs_S, int fs_C, int fs_B, int fs_T, double fs_m, double fs_I3, double fs_Q
       )
{
    if (is_Q == -9999 || fs_Q == -9999 ||
            is_bar == -9999 || fs_bar == -9999 ||
            is_lep == -9999 || fs_lep == -9999 ||
            is_S == -9999 || fs_S == -9999 ||
            is_C == -9999 || fs_C == -9999 ||
            is_B == -9999 || fs_B == -9999 ||
            is_T == -9999 || fs_T == -9999
       )
        return 0;
    return
        eval_qn_tcc(statement,  is_bar, is_lep, is_S, is_C, is_B, is_T,  is_m, is_I3,  is_Q,
                    fs_bar, fs_lep, fs_S, fs_C, fs_B, fs_T,  fs_m, fs_I3,  fs_Q);
};
