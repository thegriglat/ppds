#include "dg/I.h"
#include "settings.h"
#include "util.h"

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "sqlite.h"

#define MAXBUF 1024*1024

static
int n_I = 0;

static
struct type_I * _I = NULL;


void
vy_init_I (void) {
	char *vy_base = 0, *vy_fn = 0;
	FILE *vy_f = 0;
	if (_I) {
		return;
	}
	char *line = (char*)malloc(sizeof(char) * MAXBUF);
	if (!(vy_base = getenv("PPDS_VY"))) {
		ERROR(
		 "vy_init_I(): PPDS_VY env. pointing to the "
		 "vocabulary directory is not defined [EXIT]\n"
		);
		exit(1);
	}
	vy_fn = (char *)malloc(sizeof(char)*(strlen(vy_base) + 1 + 1 + 1 + 2 + 1));
	sprintf(vy_fn,"%s/I/vy",vy_base);
	DEBUG(" reading particle definitions from PPDS_VY = %s",vy_fn);
	if (!(vy_f = fopen(vy_fn,"r"))) {
		ERROR("vy_init_I(): can't open %s [EXIT]",vy_fn);
		exit(1);
	}
	free(vy_fn);
	for (n_I = 0; fgets(line, MAXBUF, vy_f) != NULL; n_I++){
		if (!_I){
			_I = (struct type_I *) malloc(sizeof(struct type_I));
		}
		else {
			struct type_I *tmp = (struct type_I*)realloc(_I, sizeof(struct type_I) * (n_I + 1));
			if (tmp){
				_I = tmp;
			}
			else{
				printf("Cannot allocate memory.");
				exit(0);
			}
		}
		_I[n_I].nval = 0;
		// split line to **val
		// N ABBR,ABBR [AS] [URL]
		char* Ns = strtok_r(line, " \n", &line);
		const unsigned int N = atoi(Ns);
		char* abbrval = strtok_r(line, " \n", &line);
		char* asline = "";
		if (N & (1 << 1))
			asline = strtok_r(line, " \n", &line);
		char* url = "";
		if (N & (1 << 2))
			url = strtok_r(line, " \n", &line);
		int nval = 0;
		int nas = 0;
		char * word;
		char ** val = NULL;
		char ** as = NULL;
		for (nval = 0; (word = strtok_r(abbrval, ",", &abbrval)); nval++){
			if (!val)
				val = (char**)malloc(sizeof(char*));
			else {
				char ** tmp = (char**)realloc(val, sizeof(char*) * (nval + 1));
				if (tmp){
					val = tmp;
				}
				else {
					printf("Cannot allocate memory.\n");
					exit(0);
				}
			}
			val[nval] = (char*)malloc(sizeof(char) * (strlen(word) + 1));
			strcpy(val[nval], word);
		}
		for (nas = 0; (word = strtok_r(asline, ",", &asline)); nas++){
			if (!as)
				as = (char**)malloc(sizeof(char*));
			else {
				char ** tmp = (char**)realloc(as, sizeof(char*) * (nas + 1));
				if (tmp){
					as = tmp;
				}
				else {
					printf("Cannot allocate memory.\n");
					exit(0);
				}
			}
			as[nas] = (char*)malloc(sizeof(char) * (strlen(word) + 1));
			strcpy(as[nas], word);
		}
		_I[n_I].val = val;
		_I[n_I].nval = nval;
		_I[n_I].as = as;
		_I[n_I].nas = nas;
		_I[n_I].url = url;
	}
	fclose(vy_f);
}

void *
s2t_I (char *s) {
	if (!s) {
		WARNING("s2t_I( null )");
		return NULL;
	}
	const char *sql = "select ABBREVIATION, \"AS\", URL from vy_I where ID = (SELECT ID FROM vy_I where ABBREVIATION = ?);";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(
        get_db(),
        sql, -1,
        &stmt,
        0
         ) != SQLITE_OK){
        ERROR("Cannot prepare SQL: %s", sql);
        exit(1);
    }
    if (sqlite3_bind_text(stmt, 0, s, strlen(s), SQLITE_STATIC) != SQLITE_OK){
        ERROR("Cannot bind text '%s'", s);
        exit(1);
    }
    struct type_I *result = (struct type_I*)malloc(sizeof(struct type_I));
    result->nval = 0;
    char found = 0;
    do {
        const int step = sqlite3_step(stmt);
        if (step == SQLITE_ROW){
            found = 1;
            // sqlite3_column_test return const unsigned char *
            const char* abbr = (char*) sqlite3_column_text(stmt, 0);
            const char* as = (char*) sqlite3_column_text(stmt, 1);
            const char* url = (char*) sqlite3_column_text(stmt, 2);
            // abbr
            result->val = (char**)realloc(result->val, result->nval + 1);
            result->val[result->nval] = (char*)malloc(sizeof(char) * (strlen(abbr) + 1));
            strcpy(result->val[result->nval], abbr);

            // as
            result->as = (char**)realloc(result->as, result->nas + 1);
            result->as[result->nas] = (char*)malloc(sizeof(char) * (strlen(as) + 1));
            strcpy(result->as[result->nas], as);

            // url
            result->url = (char*)malloc(sizeof(char) * (strlen(url) + 1));
            strcpy(result->url, url);

            result->nval++;
            result->nas++;
        } else {
            if (found == 0){
                ERROR("Cannot find values in vy_I for input '%s'", s);
                exit(1);
            } else {
                break;
            }
        }
    } while (1);
    result->val = unique_char_list(result->val, result->nval);
    result->nval = charlist_len(result->val);
    result->as = unique_char_list(result->as, result->nas);
    result->nas = charlist_len(result->as);
    return result;
}

char*
t2s_I(void *t) {
	struct type_I *p = (struct type_I *) t;
	return p->val[0];
}

char*
t2h_I(void *t) {
	struct type_I *p = (struct type_I *) t;
	return s2hash(p->val[0]);
}

int
eq_I (void *t1,void *t2) {
	int i;
	struct type_I *a = (struct type_I*) t1;
	struct type_I *b = (struct type_I*) t2;
	for (i = 0; i < a->nval; ++i){
		int j;
		for (j = 0; j < b->nval; ++j){
			if (strcmp(a->val[i], b->val[j]) == 0)
				return 1;
		}
	}
	return 0;
}