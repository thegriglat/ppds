#include "dg/tree.h"

#include "dg/B.h"
#include "dg/P.h"
#include "dg/RE.h"
#include "dg/DD.h"
#include "dg/PP.h"
#include "dg/AC.h"
#include "dg/DE.h"
#include "dg/I.h"
#include "dg/R.h"
#include "dg/RR.h"
#include "string.h"
#include "settings.h"

struct record_key
	keys[] = {

	{"SC",_SC_,_RECORD_, 0, 0,0,0,0,0,0},
	{"PDGSC",_PDGSC_,_SC_, 0, 0,0,0,0,0,0},
	{"IRN",_IRN_,_RECORD_, 0, 0,0,0,0,0,0},
	{"IRNW",_IRNW_,_IRN_, 0, 0,0,0,0,0,0},
	{"R",_R_,_RECORD_, 0, 0,0,0,0,0,0},
	{"COD",_COD_,_R_, 0, 0,0,0,0,0,0},
	{"TY",_TY_,_R_, 0, 0,0,0,0,0,0},
	{"D",_D_,_R_, 0, 0,0,0,0,0,0},
	{"KTO",_KTO_,_RECORD_, 0, 0,0,0,0,0,0},
	{"KRD",_KRD_,_RECORD_, 0, 0,0,0,0,0,0},
	{"CONF",_CONF_,_RECORD_, 0, 0,0,0,0,0,0},
	{"AUTHORS",_AUTHORS_,_RECORD_, 1, 0,0,0,0,0,0},
	{"A",_A_,_AUTHORS_, 0, 0,0,0,0,0,0},
	{"SPOKESMAN",_SPOKESMAN_,_A_, 1, 0,0,0,0,0,0},
	{"SPP",_SPP_,_RECORD_, 0, 0,0,0,0,0,0},
	{"I",_I_,_AUTHORS_, 0, 0,0,0,0,0,0},
	{"ILBL",_ILBL_,_I_, 0, 0,0,0,0,0,0},
	{"NA",_NA_,_RECORD_, 0, 0,0,0,0,0,0},
	{"T",_T_,_RECORD_, 0, 0,0,0,0,0,0},
	{"TW",_TW_,_T_, 0, 0,0,0,0,0,0},
	{"TITLE-TEX",_TITLE_TEX_,_T_, 0, 0,0,0,0,0,0},
	{"TA",_TA_,_T_, 0, 0,0,0,0,0,0},
	{"TA-TEX",_TA_TEX_,_TA_, 0, 0,0,0,0,0,0},
	{"ABS",_ABS_,_RECORD_, 0, 0,0,0,0,0,0},
	{"ABS-TEX",_ABS_TEX_,_ABS_, 0, 0,0,0,0,0,0},
	{"DK",_DK_,_RECORD_, 0, 0,0,0,0,0,0},
	{"L",_L_,_RECORD_, 0, 0,0,0,0,0,0},
	{"CIT",_CIT_,_RECORD_, 0, 0,0,0,0,0,0},
	{"CCOD",_CCOD_,_RECORD_, 0, 0,0,0,0,0,0},
	{"CB",_CB_,_RECORD_, 0, 0,0,0,0,0,0},
	{"CB-TEX",_CB_TEX_,_CB_, 0, 0,0,0,0,0,0},
	{"URL",_URL_,_RECORD_, 0, 0,0,0,0,0,0},
	{"PPF",_PPF_,_RECORD_, 0, 0,0,0,0,0,0},
	{"OR",_OR_,_RECORD_, 0, 0,0,0,0,0,0},
	{"SB",_SB_,_RECORD_, 0, 0,0,0,0,0,0},
	{"AV",_AV_,_RECORD_, 0, 0,0,0,0,0,0},
	{"COMP",_COMP_,_RECORD_, 0, 0,0,0,0,0,0},
	{"ES",_ES_,_RECORD_, 0, 0,0,0,0,0,0},
	{"EXPERIMENT",_EXPERIMENT_,_RECORD_, 1, 0,0,0,0,0,0},
	{"AC",_AC_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"DE",_DE_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"PR",_PR_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"COL",_COL_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"RR",_RR_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"RCOD",_RCOD_,_RR_, 0, 0,0,0,0,0,0},
	{"PUBLISHED-PAPERS",_PUBLISHED_PAPERS_,_RECORD_, 0, 0,0,0,0,0,0},
	{"PL",_PL_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"CE",_CE_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"CE-TEX",_CE_TEX_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"CD",_CD_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"CD-TEX",_CD_TEX_,_EXPERIMENT_, 0, 0,0,0,0,0,0},
	{"REAC-DATA",_REAC_DATA_,_EXPERIMENT_, 1, 0,0,0,0,0,0},
	{"RE",_RE_,_REAC_DATA_, 0, 0,0,0,0,0, 0},
	{"FSP",_FSP_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"BP",_BP_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"TP",_TP_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"B",_B_,_REAC_DATA_, 0, 0,0,0,0, 0, 0},
	{"PLAB",_PLAB_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"ECM",_ECM_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"Q2",_Q2_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"W2",_W2_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"NU",_NU_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"DD",_DD_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"INR",_INR_,_REAC_DATA_, 0, 0,0,0,0,0,0},
	{"PART-PROP",_PART_PROP_,_EXPERIMENT_, 1, 0,0,0,0,0,0},
	{"P",_P_,_PART_PROP_, 0, vy_init_P,s2t_P,t2s_P,t2h_P,eq_P ,0},
	{"PP",_PP_,_PART_PROP_, 0, 0,0,0,0,0,0},
	{"INP",_INP_,_PART_PROP_, 0, 0,0,0,0,0,0},
	{"CR",_CR_,_RECORD_, 0, 0,0,0,0,0,0},
	{"CR-TEX",_CR_TEX_,_RECORD_, 0, 0,0,0,0,0,0},
	{"CRW",_CRW_,_CR_, 0, 0,0,0,0,0,0},
	{"INDEX",_INDEX_,_RECORD_, 0, 0,0,0,0,0,0},
	{"RD",_RD_,_RECORD_, 0, 0,0,0,0,0,0},
	{"CS",_CS_,_RECORD_, 0, 0,0,0,0,0,0},
	{"RPP",_RPP_,_RECORD_, 0, 0,0,0,0,0,0},
	{"DEN",_DEN_,_RECORD_, 0, 0,0,0,0,0,0},
	{"DCH",_DCH_,_RECORD_, 0, 0,0,0,0,0,0},
	{"ACTION",_ACTION_,_RECORD_, 0, 0,0,0,0,0,0},
	{"QUESTION",_QUESTION_,_RECORD_, 0, 0,0,0,0,0,0},
	{"ANSWER",_ANSWER_,_QUESTION_, 0, 0,0,0,0,0,0},
	{"ERROR",_ERROR_,_RECORD_, 0, 0,0,0,0,0,0},
	{"RECORD",_RECORD_,_UNKNOWN_, 0, 0,0,0,0,0,0},
	{0,0,0, 0, 0,0,0,0,0,0}
};

char *
k2s(enum keytype k) {   /*FIXME быстрый поиск?*/
	int ik;
	for (ik = 0; keys[ik].s; ik++) {
		if (keys[ik].key == k) {
			return keys[ik].s;
		}
	}
	return 0;
}

int
k2noval(enum keytype k) {   /*FIXME: быстрый поиск? */
	int ik, noval = 0;
	for (ik = 0; keys[ik].s; ik++) {
		if (keys[ik].key == k) {
			noval = keys[ik].noval;
			break;
		}
	}
	return noval;
}


enum keytype
k2par(enum keytype k) { /*FIXME: быстрый поиск */
	int ik;
	for (ik = 0; keys[ik].s; ik++) {
		if (keys[ik].key == k) {
			return (keys[ik].par) ? keys[ik].par : _UNKNOWN_;
		}
	}
	return _UNKNOWN_;
}

int
k2ik (enum keytype k) {
	int ik;
	for (ik = 0; keys[ik].s; ik++) {
		//DEBUG("k=%d\tkeys[%d].s=%s\tkeys[%d].key = %d ? %d", k, ik, keys[ik].s, ik, keys[ik].key, k);
		if(keys[ik].key == k) {
			//DEBUG("return %d", ik);
			return ik;
		}
	}
	//DEBUG("return -1");
	return -1;
}


enum keytype
s2k(char *s) {
	int k;
	for (k = 0; keys[k].s; k++) {
		if (strcmp(keys[k].s, s) == 0) {
			return keys[k].key;
		}
	}
	return _UNKNOWN_;
}

int
s2ik(char *s) {
	int ik;
	for (ik = 0; keys[ik].s; ik++) {
		if (strcmp(keys[ik].s, s) == 0) {
			return ik;
		}
	}
	return -1;
}
