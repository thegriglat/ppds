#include "dg/DE.h"
#include "settings.h"
#include "util.h"

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "sqlite.h"

#define MAXBUF 1024*16

static
int n_DE = 0;

static
struct type_DE * _DE = NULL;


void
vy_init_DE (void)
{
    char *vy_base = 0, *vy_fn = 0;
    FILE *vy_f = 0;
    if (_DE) {
        return;
    }
    char *line = (char*)malloc(sizeof(char) * MAXBUF);
    if (!(vy_base = getenv("PPDS_VY"))) {
        ERROR(
                "vy_init_DE(): PPDS_VY env. pointing to the "
                "vocabulary directory is not defined [EXIT]\n"
               );
        exit(1);
    }
    vy_fn = (char *)malloc(sizeof(char)*(strlen(vy_base) + 1 + 2 + 1 + 2 + 1));
    sprintf(vy_fn,"%s/DE/vy",vy_base);
    DEBUG(" reading particle definitions from PPDS_VY = %s",vy_fn);
    if (!(vy_f = fopen(vy_fn,"r"))) {
        ERROR("vy_init_DE(): can't open %s [EXIT]",vy_fn);
        exit(1);
    }
    free(vy_fn);
    for (n_DE = 0; fgets(line, MAXBUF, vy_f) != NULL; n_DE++) {
        if (!_DE) {
            _DE = (struct type_DE *) malloc(sizeof(struct type_DE));
        } else {
            struct type_DE *tmp = (struct type_DE*)realloc(_DE, sizeof(struct type_DE) * (n_DE + 1));
            if (tmp) {
                _DE = tmp;
            } else {
                printf("Cannot allocate memory.");
                exit(0);
            }
        }
        _DE[n_DE].nval = 0;
        // split line to **val
        // line is "val,val,...,val ident"
        int nval = 0;
        char * word;
        char ** val = NULL;
        char * valline = strtok_r(line, "|\n", &line);
        char * ident = strtok_r(line, "|\n", &line);
        for (nval = 0; (word = strtok_r(valline, ",", &valline)); nval++) {
            if (!val)
                val = (char**)malloc(sizeof(char*));
            else {
                char ** tmp = (char**)realloc(val, sizeof(char*) * (nval + 1));
                if (tmp) {
                    val = tmp;
                } else {
                    printf("Cannot allocate memory.\n");
                    exit(0);
                }
            }
            val[nval] = (char*)malloc(sizeof(char) * (strlen(word) + 1));
            strcpy(val[nval], word);
        }
        _DE[n_DE].ident = ident ? ident : "";
        _DE[n_DE].val = val;
        _DE[n_DE].nval = nval;
    }
    fclose(vy_f);
}

void *
s2t_DE (char *s)
{
    if (!s) {
        WARNING("s2t_DE( null )");
        return NULL;
    }
    const char *sql = "select ABBREVIATION, IDENT from vy_DE where ID = (SELECT ID FROM vy_DE where ABBREVIATION = ?);";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(
        get_db(),
        sql, -1,
        &stmt,
        0
         ) != SQLITE_OK){
        ERROR("Cannot prepare SQL: %s", sql);
        exit(1);
    }
    if (sqlite3_bind_text(stmt, 0, s, strlen(s), SQLITE_STATIC) != SQLITE_OK){
        ERROR("Cannot bind text '%s'", s);
        exit(1);
    }
    struct type_DE *result = (struct type_DE*)malloc(sizeof(struct type_DE));
    result->nval = 0;
    char found = 0;
    do {
        const int step = sqlite3_step(stmt);
        if (step == SQLITE_ROW){
            found = 1;
            // sqlite3_column_test return const unsigned char *
            const char* value = (char*) sqlite3_column_text(stmt, 0);
            result->val = (char**)realloc(result->val, result->nval + 1);
            result->val[result->nval] = (char*)malloc(sizeof(char) * (strlen(value) + 1));
            strcpy(result->val[result->nval], value);
            result->nval++;
            // ident
            const char* ident = (char*) sqlite3_column_text(stmt, 1);
            result->ident = (char*)malloc(sizeof(char) * (strlen(ident) + 1));
            strcpy(result->ident, ident);
        } else {
            if (found == 0){
                ERROR("Cannot find values in vy_DE for input '%s'", s);
                exit(1);
            } else {
                break;
            }
        }
    } while (1);
    return result;
}

char*
t2s_DE(void *t)
{
    struct type_DE *p = (struct type_DE *) t;
    return p->val[0];
}

char*
t2h_DE(void *t)
{
    struct type_DE *p = (struct type_DE *) t;
    char* text = (char*)malloc(sizeof(char) * (strlen(p->val[0]) + (p->ident ? strlen(p->ident): 0) + 1 + 1));
    sprintf(text, "%s@%s", p->val[0], p->ident);
    return s2hash(text);
}

int
eq_DE (void *t1,void *t2)
{
    int i;
    struct type_DE *a = (struct type_DE*) t1;
    struct type_DE *b = (struct type_DE*) t2;
    if (strcmp(a->ident, b->ident) != 0)
        return 0;
    for (i = 0; i < a->nval; ++i) {
        int j;
        for (j = 0; j < b->nval; ++j) {
            if (strcmp(a->val[i], b->val[j]) == 0)
                return 1;
        }
    }
    return 0;
}