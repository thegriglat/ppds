#include "dg/AC.h"
#include "settings.h"
#include "util.h"

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

#include "sqlite.h"

#define MAXBUF 1024*16

static
int n_AC = 0;

static
struct type_AC * _AC = NULL;

void
vy_init_AC (void)
{
    char *vy_base = 0, *vy_fn = 0;
    FILE *vy_f = 0;
    if (_AC) {
        return;
    }
    char *line = (char*)malloc(sizeof(char) * MAXBUF);
    if (!(vy_base = getenv("PPDS_VY"))) {
        ERROR(
                "vy_init_AC(): PPDS_VY env. pointing to the "
                "vocabulary directory is not defined [EXIT]\n"
               );
        exit(1);
    }
    vy_fn = (char *)malloc(sizeof(char)*(strlen(vy_base) + 1 + 2 + 1 + 2 + 1));
    sprintf(vy_fn,"%s/AC/vy",vy_base);
    DEBUG(" reading particle definitions from PPDS_VY = %s",vy_fn);
    if (!(vy_f = fopen(vy_fn,"r"))) {
        ERROR("vy_init_AC(): can't open %s [EXIT]",vy_fn);
        exit(1);
    }
    free(vy_fn);
    for (n_AC = 0; fgets(line, MAXBUF, vy_f) != NULL; n_AC++) {
        if (!_AC) {
            _AC = (struct type_AC *) malloc(sizeof(struct type_AC));
        } else {
            struct type_AC *tmp = (struct type_AC*)realloc(_AC, sizeof(struct type_AC) * (n_AC + 1));
            if (tmp) {
                _AC = tmp;
            } else {
                printf("Cannot allocate memory.");
                exit(0);
            }
        }
        _AC[n_AC].nval = 0;
        // split line to **val
        int nval = 0;
        char * word;
        char ** val = NULL;
        for (nval = 0; (word = strtok_r(line, " \n", &line)); nval++) {
            if (!val)
                val = (char**)malloc(sizeof(char*));
            else {
                char ** tmp = (char**)realloc(val, sizeof(char*) * (nval + 1));
                if (tmp) {
                    val = tmp;
                } else {
                    printf("Cannot allocate memory.\n");
                    exit(0);
                }
            }
            val[nval] = (char*)malloc(sizeof(char) * (strlen(word) + 1));
            strcpy(val[nval], word);
        }
        _AC[n_AC].val = val;
        _AC[n_AC].nval = nval;
    }
    fclose(vy_f);
}

void *
s2t_AC (char *s)
{
    if (!s) {
        WARNING("s2t_AC( null )");
        return NULL;
    }
    const char *sql = "select ABBREVIATION from vy_AC where ID = (SELECT ID FROM vy_AC where ABBREVIATION = ?);";
    sqlite3_stmt *stmt;
    if (sqlite3_prepare(
        get_db(),
        sql, -1,
        &stmt,
        0
         ) != SQLITE_OK){
        ERROR("Cannot prepare SQL: %s", sql);
        exit(1);
    }
    if (sqlite3_bind_text(stmt, 0, s, strlen(s), SQLITE_STATIC) != SQLITE_OK){
        ERROR("Cannot bind text '%s'", s);
        exit(1);
    }
    struct type_AC *result = (struct type_AC*)malloc(sizeof(struct type_AC));
    result->nval = 0;
    char found = 0;
    do {
        const int step = sqlite3_step(stmt);
        if (step == SQLITE_ROW){
            found = 1;
            // sqlite3_column_test return const unsigned char *
            const char* value = (char*) sqlite3_column_text(stmt, 0);
            result->val = (char**)realloc(result->val, result->nval + 1);
            result->val[result->nval] = (char*)malloc(sizeof(char) * (strlen(value) + 1));
            strcpy(result->val[result->nval], value);
            result->nval++;
        } else {
            if (found == 0){
                ERROR("Cannot find values in vy_AC for input '%s'", s);
                exit(1);
            } else {
                break;
            }
        }
    } while (1);
    return result;
}

char*
t2s_AC(void *t)
{
    struct type_AC *p = (struct type_AC *) t;
    return p->val[0];
}

char*
t2h_AC(void *t)
{
    struct type_AC *p = (struct type_AC *) t;
    return s2hash(p->val[0]);
}

int
eq_AC (void *t1,void *t2)
{
    int i;
    struct type_AC *a = (struct type_AC*) t1;
    struct type_AC *b = (struct type_AC*) t2;
    for (i = 0; i < a->nval; ++i) {
        int j;
        for (j = 0; j < b->nval; ++j) {
            if (strcmp(a->val[i], b->val[j]) == 0)
                return 1;
        }
    }
    return 0;
}