%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dg/tree.h"
#include "dg/R.h"
#include "dg/RR.h"
/*#include "tree2base.h"*/

#include "settings.h"
#include "sqlite.h"

#ifndef COMMIT_EVERY
#define COMMIT_EVERY 50
#endif

extern
int
tree2base (struct node* no); /* tree2base.c */

char* re2h(char*);

int rec_loaded;
int record_counter = 0;

int current_rec_id = 0;
unsigned int s_parent_id = -1;

char buf[65536], *p, *py;

enum { _val_, _inkey_, _ex_, _er_ } st;

int mult_val;

enum keytype key, key_par;

extern
struct record_key
keys[];

struct node
*no_root, *no_cur, *no_par;

int isDictNode(struct node* no){
	if (no->type == _AC_ ||
		no->type == _DD_ ||
		no->type == _DE_ ||
		no->type == _I_  ||
		no->type == _P_  ||
		no->type == _PP_ ||
		no->type == _R_  ||
		no->type == _RR_
		)
		return 1;
	return 0;
}

int isDictNodeWithLexer(struct node* no){
	if (no->type == _R_ ||no->type == _RR_)
		return 1;
	return 0;
}

int getDictValueId(char* table, char* value){
	sqlite3_stmt* stmt;
	char* sql = (char*)malloc(sizeof(char) * 1024);
	sprintf(sql, "select id from vy_%s where abbreviation = ?;", table);
	if (sqlite3_prepare(get_db(), sql, -1, &stmt, 0) != SQLITE_OK){
		ERROR("Cannot prepare SQL: %s", sql);
		return -1;
	};
	sqlite3_bind_text(stmt, 1, value, strlen(value), SQLITE_STATIC);
	int step = sqlite3_step(stmt);
	if (step != SQLITE_ROW){
		//
		ERROR("Value %s not found in table vy_%s", value, table);
		return -1;
	}
	const int id = sqlite3_column_int(stmt, 0);
	sqlite_stmt_free(stmt);
	free(sql);
	return id;
}

char * getRE_db_value(char * reaction){
	char* hash = re2h(reaction);
	char* value = (char*)malloc(sizeof(char) * (strlen(reaction) + strlen(hash) + 1 + 1));
	sprintf(value, "%s|%s", reaction, hash);
	if (!value){
		ERROR("Reaction parse error: %s", reaction);
		return NULL;
	}
	return value;
}

struct node*
new_node_dg(enum keytype type, struct node *par) {
	struct node *no = (struct node*)malloc(sizeof(struct node));
	no->type = type;
	no->par = par;
	no->ch = NULL;
	no->nch = 0;
	no->val = NULL;
	no->nval = 0;
	if (par) {
		par->ch = (struct node**)realloc(par->ch, sizeof(struct node*) * (++ par->nch));
		par->ch[ par->nch - 1 ] = no;
	}
	return no;
}

void
val_node(struct node* no, char* val) {
	if (!no) {
		ERROR("val_node(NULL, %s) called. EXIT", val);
		exit(1);
	}
	no->val = (char**)realloc(no->val, sizeof(char*) * (no->nval + 1));
	if (val){
		while (*val == ' ' || *val == '\t' || *val == '\n')
			val++;
		no->val[ no->nval ] = (char*)malloc(sizeof(char) * (strlen(val) + 1));
		strcpy(no->val[ no->nval++ ], val );
	}
	return;
}

void
free_tree_dg (struct node* no) {
	if (!no)
		return;
	if (no->val) {
		for (; no->nval > 0; no->nval--)
			free((void*)no->val[ no->nval - 1 ]);
		free((void*)no->val);
	}
	if (no->ch) {
		for (; no->nch > 0; no->nch --)
			free_tree_dg(no->ch[ no->nch - 1 ]);
		free((void*)no->ch);
	}
	free((void*)no);
	return;
}

int write_tree_dg(struct node* no){
	// return 0 if fail, 1 if okay
	int i;
	if (!no) {
		printf("[NULL]\n");
		return 0;
	}
	++record_counter;
	// sqlite stuff
	if (!no->par){
		// root node
		if (record_counter % COMMIT_EVERY == 0){
			sqlite3_exec(get_db(), "END TRANSACTION;", 0, 0, 0);
			sqlite3_exec(get_db(), "BEGIN TRANSACTION;", 0, 0, 0);
		}
		s_parent_id = -1;
	};
	if (no->type == _RECORD_)
		++current_rec_id;
	if (no->nval == 0){
			const char* sql = "insert into dg_data (REC_ID, PID, TID) select ?, ?, ID from dg_type where type = ?;";
			const void * params[] = {(void*)&current_rec_id, (void*)&s_parent_id, (void*)(k2s(no->type))};
			const enum SQLITE_TYPE types[] = {INTEGER, INTEGER, TEXT};
			int rc = db_exec_noresult(
				sql,
				3,
				params,
				types
				);
			if (rc != SQLITE_DONE){
				ERROR("SQLITE ERROR! insert into dg_data (REC_ID, PID, TID) select %d, %d, TID from dg_type where type = '%s';", current_rec_id, s_parent_id, k2s(no->type));
				ERROR("rc = %d", rc);
				return 0;
			}
	}
	else {
		int value_id;
		const int is_dict_node = (isDictNode(no) && !(isDictNodeWithLexer(no))) ? 1 : 0;
		for (i = 0; i < no->nval; ++i){
			char* value = NULL;;
			if (is_dict_node){
				value_id = getDictValueId(k2s(no->type), no->val[i]);
				if (value_id == -1)	return 0;
				continue;
			}
			const char *value_sql = "select id from dg_value where value = ? and tid = (select id from dg_type where type = ?);";
			sqlite3_stmt *stmt;
			if (sqlite3_prepare(get_db(), value_sql, -1, &stmt, 0) != SQLITE_OK){
				ERROR("Cannot prepare SQL: %s", value_sql);
				exit(1);
			};
			if (no->type == _RE_){
				value = getRE_db_value(no->val[i]);
				if (!value) return 0;
			}
			else
				value = no->val[i];
			sqlite3_bind_text(stmt, 1, value, strlen(value), SQLITE_STATIC);
			sqlite3_bind_text(stmt, 2, k2s(no->type), strlen(k2s(no->type)), SQLITE_STATIC);
			const int step = sqlite3_step(stmt);
			if (step == SQLITE_ROW){
				// id available, value found
				value_id = sqlite3_column_int(stmt, 0);
			} else if (step == SQLITE_DONE){
				// id not found
				// insert
				/*
				if (isDictNode(no) && isDictNodeWithLexer(no)){
					// at least check that value is properly parsed
					if (no->type == _R_){
						if (!check_R(no->val[i])){
							ERROR("Cannot parse '%s' against R dictionary", no->val[i]);
							exit(1);
						}
					} else if (no->type == _RR_){
						if (!check_RR(no->val[i])){
							ERROR("Cannot parse '%s' against RR dictionary", no->val[i]);
							exit(1);
						}
					}
				}
				*/
				const void * params[] = {(void*)(value), (void*)(k2s(no->type))};
				const enum SQLITE_TYPE types[] = {TEXT, TEXT};
				db_exec_noresult("insert into dg_value (tid, value) select id, ? from dg_type where type = ?;",
			 2,
			 params,
			 types);
				if (no->type == _RE_ && value) free(value);
				value_id = db_get_last_id("dg_value", 0);
			} else {
				// have to exit
				ERROR("Cannot execute sql: %s", value_sql);
				return 0;
			};
			sqlite_stmt_free(stmt);
		}
		int rc;
		if (is_dict_node){
			const void * params[] = {(void*)&current_rec_id, (void*)&s_parent_id, (void*)(&value_id), (void*)k2s(no->type)};
			const enum SQLITE_TYPE types[] = {INTEGER, INTEGER, INTEGER, TEXT};
        	const char* sql = "insert into dg_data (rec_id, pid, tid, vid) select ?, ?, ID, ? from dg_type where type = ?;";
			rc = db_exec_noresult(
				sql,
				4,
				params,
				types
				);
		} else {
			const void * params[] = {(void*)&current_rec_id, (void*)&s_parent_id, (void*)(&value_id)};
			const enum SQLITE_TYPE types[] = {INTEGER, INTEGER, INTEGER};
        	const char* sql = "insert into dg_data (rec_id, pid, tid, vid) select ?, ?, TID, ID from dg_value where id = ? ";
			rc = db_exec_noresult(
				sql,
				3,
				params,
				types
				);
		}
		if (rc != SQLITE_DONE){
			ERROR("SQLITE ERROR! insert into dg_data (rec_id, pid, vid) values (%d, %d, %d);", current_rec_id, s_parent_id, value_id);
			ERROR("rc = %d", rc);
			return 0;
		}
	}
	const int pid = s_parent_id;
	s_parent_id = db_get_last_id("dg_data", 0);
	for (i = 0; i < no->nch; i++){
		if (!write_tree_dg(no->ch[i])){
			return 0;
		}
	}
	s_parent_id = pid;
	return 1;
}

void
print_tree_dg_io (FILE* io, struct node *no, int ident) {
	if (!no) {
		fprintf(io, "[NULL]\n");
		return;
	}
	int ik, i;
	// char *h;
	// void *t;
	for (i = 0; i < ident; i++)
		fputc(' ', io);

	if (k2noval(no->type)) {
		fprintf(io, "%s;\n",k2s(no->type));
	}
	else {
		ik = k2ik(no->type);
		if (ik != -1){ // key not found
			fprintf(io, "%s = ", keys[ik].s);
			for (i = 0; i < no->nval; i++) {
				fprintf(io, "%s", no->val[i]);
				// if (keys[ik].s2t && keys[ik].t2h) {
				// 	h = keys[ik].t2h( t = keys[ik].s2t(no->val[i]) );
				// 	DEBUG("ik: %d, HASH l.l = %s", ik, h);
				// 	printf(" { %s } ", h);
				// 	free((void*)h);
				// 	if (keys[ik].t_free)
				// 		keys[ik].t_free( t );
				// }
				fprintf(io, ";");
			}
		}
		fprintf(io, "\n");
	}
	for (i = 0; i < no->nch; i++)
		print_tree_dg_io(io, no->ch[i], ident+1);
	if (ident == 0) fprintf(io, "*E\n");
}

void
print_tree_dg (struct node *no, int ident){
	print_tree_dg_io (stdout, no, ident);
}

void
print_tree_dg_stderr (struct node *no, int ident){
	print_tree_dg_io (stderr, no, ident);
}


struct node*
find_par(enum keytype key, struct node* no) {
	if (!no)
		return NULL;
	if (no->type == k2par(key) || no->type == _RECORD_)
		return no;
	else
		return find_par(key,no->par);
}


%}

%%

SC|PDGSC|IRN|IRNW|R|COD|TY|D|KTO|KRD|CONF|AUTHORS|A|SPOKESMAN|SPP|I|ILBL|NA|T|TW|TITLE-TEX|TA|TA-TEX|ABS|ABS-TEX|DK|L|CIT|CCOD|CB|CB-TEX|URL|PPF|OR|SB|AV|COMP|ES|EXPERIMENT|AC|DE|PR|COL|RR|RCOD|PUBLISHED-PAPERS|PL|CE|CE-TEX|CD|CD-TEX|REAC-DATA|RE|FSP|BP|TP|B|PLAB|ECM|Q2|W2|NU|DD|INR|PART-PROP|P|PP|INP|CR|CR-TEX|CRW|INDEX|RD|CS|RPP|DEN|DCH|ACTION|QUESTION|ANSWER|ERROR {
/*	printf("dbg: ppdsdgtext: %s\n",ppdsdgtext);*/
	switch (st) {
		case _val_:
			for (py = ppdsdgtext; *py; *p++ = *py++);
			break;
		case  _ex_ :
		case  _er_ :
			if ((key = s2k(ppdsdgtext)) == _UNKNOWN_) {
				ERROR("unknown key: %s. EXIT",ppdsdgtext);
				printf("^^^^^^^^ broken record follows >>>>>>>>>>>>>>>>>");
				print_tree_dg(no_root,0);
				exit(1);
			}
			mult_val = 0;
			st = _inkey_;
			break;
		case  _inkey_:
			ERROR("spurious \"%s\" on the input. EXIT",ppdsdgtext);
			printf("^^^^^^^^ broken record follows >>>>>>>>>>>>>>>>>");
			print_tree_dg(no_root,0);
			exit(1);
			break;
	}
}

"." {
	switch(st) {
		case _val_:
			*p++ = *ppdsdgtext;
			break;
		case _ex_:
		case _er_:
			ERROR("spurious '.' after ';' or '*E'. EXIT");
			printf("^^^^^^^^ broken record follows >>>>>>>>>>>>>>>>>");
			print_tree_dg(no_root,0);
			exit(1);
			break;
		case _inkey_:
			mult_val = 1;
			break;
	}
}

"=" {
	switch (st) {
		case  _val_:
			*p++ = *ppdsdgtext;
			break;
		case  _ex_:
			p = buf;
			*p++ = *ppdsdgtext;
			st = _val_;
			break;
		case  _er_:
			ERROR("spurious '=' after '*E'. EXIT");
			printf("^^^^^^^^ broken record follows >>>>>>>>>>>>>>>>>");
			print_tree_dg(no_root,0);
			exit (1);
			break;
		case  _inkey_:
			if ((no_par = find_par(key,no_cur))) {
				no_cur = new_node_dg(key,no_par);
			}
			else {
				if (k2noval(key_par = k2par(key))) {
					if ((no_par = find_par(key_par,no_cur))) {
						no_cur = new_node_dg(key_par,no_par);
					}
					else {
						ERROR(
						 "can't find parent to insert implied %s",
						 k2s(key_par)
						);
						printf("^^^^^^^^ broken record tree follows >>>>>>>>>>>>>>>>>");
						print_tree_dg(no_root,0);
						exit(1);
					}
					no_cur = new_node_dg(key,no_cur);
				}
				else {
					ERROR("can't find parent for %s, %d. EXIT",
						k2s(key), key
					);
					printf("^^^^^^^^ broken record tree follows >>>>>>>>>>>>>>>>>");
					print_tree_dg(no_root,0);
					exit(1);
				}
			}
			p = buf;
			st = _val_;
			break;
	}
}

";" {
	switch (st) {
		case  _val_:
			if (p > buf && *(p-1) == '\\') {
				*p++ = *ppdsdgtext;
			}
			else {
				*p = '\0';
				DEBUG("key: %s%sval: %s\n",k2s(key), mult_val ? " +" : "  ",  buf);
				val_node(no_cur,buf);
				p = buf;
				st = _ex_;
			}
			break;
		case  _ex_:
			break;
		case  _er_:
			ERROR("spurious ';' after '*E'. EXIT");
			printf("^^^^^^^^ broken record tree follows >>>>>>>>>>>>>>>>>");
			print_tree_dg(no_root,0);
			exit(1);
			break;
		case  _inkey_:
			if (k2noval(key) == 0) {
				ERROR("spurious ';' after '='. EXIT");
				printf("^^^^^^^^ broken record tree follows >>>>>>>>>>>>>>>>>");
				print_tree_dg(no_root,0);
				exit(1);
			}
			else {
				if ((no_par = find_par(key,no_cur))) {
					no_cur = new_node_dg(key,no_par);
					st = _ex_;
				}
				else {
					ERROR("can't find parent node for %s. EXIT",
						k2s(key)
					);
					printf("^^^^^^^^ broken record tree follows >>>>>>>>>>>>>>>>>");
					print_tree_dg(no_root,0);
					exit(1);
				}
			}
			break;
	}
}

"*E" {
	switch (st) {
		case  _val_:
			for (py = ppdsdgtext; *py; *p++ = *py++);
			break;
		case  _ex_:
			key = _NOKEY_;
			st = _er_;
			print_tree_dg(no_root,0);
			if (!write_tree_dg(no_root)){
				print_tree_dg_stderr(no_root, 0);
			}
			// if (!tree2base(no_root)) {
			// 	printf("INFO: ^^^^^^ broken record tree follows >>>>>>>>\n");
			// 	print_tree_dg(no_root,0);
			// 	ERROR("^^^^^ Invalid record [EXIT]");
			// 	exit(1);
			// }
			rec_loaded ++;
			if (rec_loaded % 100 == 0)
				printf("INFO: %d records loaded ..................... \n",rec_loaded);
			DEBUG("======================================\nNew record ...........\n");
			free_tree_dg(no_root);
			no_par = no_cur = no_root = new_node_dg(_RECORD_, NULL);
			break;
		case  _er_:
			break;
		case  _inkey_:
			if (k2noval(key) == 0) {
				ERROR("spurious '*E' after key. EXIT");
				printf("^^^^^^^^ broken record tree follows >>>>>>>>>>>>>>>>>");
				print_tree_dg(no_root,0);
				exit(1);
			}
			else {
				printf("*E immediately after %s\n",k2s(key));
				printf("^^^^^^^^^^^ record tree follows >>>>>>>>>\n");
				print_tree_dg(no_root,0);
				if (!write_tree_dg(no_root)){
					print_tree_dg_stderr(no_root, 0);
				}
			}
			break;
	}
}

[ \t\n] {
	switch (st) {
		case  _val_:
			*p++ = *ppdsdgtext;
			break;
		case  _ex_:
		case  _er_:
		case  _inkey_:
			break;
	}
}

[^ \t\n] {
	switch (st) {
		case  _val_:
			*p++ = *ppdsdgtext;
			break;
		case  _ex_:
			st = _val_;
			*p++ = *ppdsdgtext;
			break;
		case  _er_:
			ERROR("spurious '%c' after '*E'. EXIT",*ppdsdgtext);
			printf("^^^^^^^^ broken record tree follows >>>>>>>>>>>>>>>>>");
			print_tree_dg(no_root,0);
			exit(1);
		case  _inkey_:
			ERROR("spurious '%c' after key. EXIT",*ppdsdgtext);
			printf("^^^^^^^^ broken record tree follows >>>>>>>>>>>>>>>>>");
			print_tree_dg(no_root,0);
			exit(1);
	}
}




%%
int ppdsdgwrap(void) {
	return 1;
}


int
main(void) {
	int ik;
	DEBUG("init ....\n");
	for (ik = 0; keys[ik].s; ik++) {
		DEBUG("reading vocabulary: %s: ->vy_init() ...\n",keys[ik].s);
		if (keys[ik].vy_init) {
			keys[ik].vy_init();
		}
		DEBUG("... done\n");
	}
	printf("bp00\n");
	sqlite3_exec(get_db(), "BEGIN TRANSACTION;", 0, 0, 0);
	// get last rec_id from database
	sqlite3_stmt *stmt;
	if (sqlite3_prepare(get_db(), "select max(rec_id) as m from dg_data;", -1, &stmt, 0) != SQLITE_OK){
		ERROR("Cannot prepare SQL: select max(rec_id) from dg_data;");
		exit(1);
	};
	sqlite3_step(stmt);
	current_rec_id = sqlite3_column_int(stmt, 0);
	sqlite_stmt_free(stmt);
	// end
	rec_loaded = 0;
	st = _er_;
	no_par = no_cur = no_root = new_node_dg(_RECORD_,NULL);
	ppdsdglex();
	sqlite3_exec(get_db(), "END TRANSACTION;", 0, 0, 0);
	db_close();
	return 0;
}
