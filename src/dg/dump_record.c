#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dg/tree.h"

#include "settings.h"



enum { _ex_record_, _ex_bra_, _ex_cket_, _val_, _inkey_, _ex_, _er_ } st;

int mult_val;

enum keytype key, key_par;

extern
struct record_key
keys[];

struct node
*no_root = 0, 
*no_cur  = 0, 
*no_par  = 0;

static 
char 
*base = NULL, 
*curdir = ".", 
*record_path = NULL,
*val_file_path = NULL,
*hash_file_path = NULL
;

struct node*
new_node_dg(enum keytype type, struct node *par) {
	struct node *no = (struct node*)malloc(sizeof(struct node));
	no->type = type;
	no->par = par;
	no->ch = NULL;
	no->nch = 0;
	no->val = NULL;
	no->nval = 0;
	if (par) {
		par->ch = (struct node**)realloc(par->ch, sizeof(struct node*) * (++ par->nch));
		par->ch[ par->nch - 1 ] = no;
	}
	return no;
}

void
val_node(struct node* no, char* val) {
	if (!no) {
		ERROR("val_node(NULL, %s) called. EXIT", val);
		exit(1);
	}
	no->val = (char**)realloc(no->val, sizeof(char*) * (no->nval + 1));
	if (val)
		while (*val == ' ' || *val == '\t' || *val == '\n')
			val++;
	no->val[ no->nval ] = (char*)malloc(sizeof(char) * (strlen(val) + 1));
	strcpy(no->val[ no->nval++ ], val );
	return;
}

void
free_tree_dg (struct node* no) {
	if (!no)
		return;
	if (no->val) {
		for (; no->nval > 0; no->nval--)
			free((void*)no->val[ no->nval - 1 ]);
		free((void*)no->val);
	}
	if (no->ch) {
		for (; no->nch > 0; no->nch --)
			free_tree_dg(no->ch[ no->nch - 1 ]);
		free((void*)no->ch);
	}
	free((void*)no);
	return;
}

char val_buf[1024];

int n_vh_files = 0;

struct fvh { 
	enum keytype key;
	FILE *fv, *fh;
} *vh_files = 0;

FILE*
fv_open (enum keytype key) {
	int i;
	char path[1024];
	for (i = 0; i < n_vh_files; i++) 
		if (vh_files[i].key == key) 
			break;
	if (i == n_vh_files) {
		struct fvh *tmp = (struct fvh*)realloc(vh_files, sizeof(struct fvh) * (n_vh_files + 1));
		if (tmp)
			vh_files = tmp;
		else {
			ERROR("Cannot allocate memory.");
			exit(1);
		}
		sprintf(path,"%s/%s/v",base,k2s(key));
		vh_files[ n_vh_files ].fv = fopen(path,"r");
		sprintf(path,"%s/%s/h",base,k2s(key));
		vh_files[ n_vh_files ].fh = fopen(path,"r");
		vh_files[ n_vh_files ].key = key;
		n_vh_files ++;
	}
	DEBUG("fv_open: ret fv[ %d ]",i);
	return vh_files[i].fv;
}
FILE* 
fh_open (enum keytype key) {
	int i;
	for (i = 0; i < n_vh_files; i++)
		if (vh_files[i].key == key)
			break;
	if (i == n_vh_files)
		return NULL;
	DEBUG("fv_open: ret fh[ %d ]",i);
	return vh_files[i].fh;
}

void 
hash1(FILE *fh, int *pos1,   char* hash_fh) {
	int rec_pos;
	char c;
	for ( fseek(fh,*pos1,SEEK_SET);;) {
		if (*pos1 == 0)
			break;
		if ((c = fgetc(fh)) == '\n') {
			DEBUG("%ld - 1: c == 0, break",ftell(fh));
			break;
		}
		DEBUG("hash1: p: %ld - 1: %c",ftell(fh),c);
		fseek(fh,-2,SEEK_CUR);
		*pos1 = ftell(fh);
	}
	if (fscanf(fh,"%s %d",hash_fh,&rec_pos) != 2) {
		ERROR("dump_record: can't read {hash,rec_pos} from %s/%s/h",base,k2s(key));
		exit (1);
	}
}

void
hash2(FILE *fh, int *pos2, int pos_end, char *hash_fh) {
	char c;
	for ( fseek(fh,*pos2,SEEK_SET);;) {
		if (*pos2 >= pos_end) {
			break;
		}
		if ((c = fgetc(fh)) == '\n') {
			DEBUG("%ld - 1: c == 0, break",ftell(fh));
			break;
		}
		*pos2 = ftell(fh);
		DEBUG("hash2: %d - 1: %c",*pos2,c);
	}
	if (*pos2 == pos_end) {
		for (;;) {
			if (*pos2 == 0)
				break;
			if ((c = fgetc(fh)) == '\n') {
				DEBUG("%ld - 1: c == 0, break",ftell(fh));
				break;
			}
			DEBUG("p: %ld - 1: %c",ftell(fh),c);
			fseek(fh,-2,SEEK_CUR);
			*pos2 = ftell(fh);
		}
	}
}

char*
val_from_hash(enum keytype key, char* hash) {
	FILE *fv = NULL, *fh = NULL;
	char h1[64],h2[64], hm[64], *p, c;
	int rec_pos, pos1, pos2, posm,  pos_end, cmp;
	DEBUG("val_from_hash(%s,%s): v: %s h: %s",k2s(key),hash,val_file_path,hash_file_path);
	if (!(fv = fv_open(key))) {
		ERROR("can't open %s/%s/v",base,k2s(key));
		exit(1);
	}
	if (!(fh = fh_open(key))) {
		ERROR("can't open %s/%s/h",base,k2s(key));
		exit(1);
	}
	/*
	fseek(fh,0L,SEEK_SET);
	while (fscanf(fh,"%s %d",hash_fh,&rec_pos) == 2) {
		if (strcmp(hash_fh,hash) == 0) {
			val_pos = rec_pos;
			break;
		}
	}
	*/
	fseek(fh,0L,SEEK_SET);
	pos1 = ftell(fh);
	fseek(fh,0L,SEEK_END);
	pos_end = pos2 = ftell(fh);
	DEBUG("%s/%s/h: 1,2: %d %d",base,k2s(key),pos1,pos2);

	for (;;) {
		hash1(fh, &pos1, h1);
	
		DEBUG("pos1: %d hash: %s cmp: %d",pos1,h1, strcmp(hash,h1));
		if ((cmp = strcmp(hash,h1)) < 0) {
			ERROR("no %s in %s/%s/h (unsorted?)",hash,base,k2s(key));
			exit(1);
		}
		if (cmp == 0) {
			fseek(fh,pos1,SEEK_SET);
			if (fscanf(fh,"%s %d",h1,&rec_pos) != 2) {
				ERROR("can't read {has,rec_pos} from %s/%s/h",base,k2s(key));
				exit(1);
			}
			DEBUG("found: %s == %s rec_pos: %d",hash,h1,rec_pos);
			break;
		}

		hash2(fh,&pos2, pos_end, h2);

		if (fscanf(fh,"%s %d",h2,&rec_pos) != 2) {
			ERROR("dump_record: can't read {hash,rec_pos} from %s/%s/h",base,k2s(key));
			exit (1);
		}
		DEBUG("pos2: %d hash: %s rec_pos: %d cmp: %d",pos2,h2,rec_pos, strcmp(hash,h2));
		if ((cmp = strcmp(hash,h2)) > 0) {
			ERROR("no %s in %s/%s/h (unsorted?)",hash,base,k2s(key));
			exit(1);
		}
		if (cmp == 0) {
			DEBUG("found: %s == %s rec_pos: %d",hash,h2,rec_pos);
			break;
		}

		posm = (pos1 + pos2) / 2;
		hash1(fh, &posm, hm);

		if (fscanf(fh,"%s %d",hm,&rec_pos) != 2) {
			ERROR("dump_record: can't read {hash,rec_pos} from %s/%s/h",base,k2s(key));
			exit (1);
		}
		DEBUG("posm: %d hash: %s rec_pos: %d cmp: %d",posm,hm,rec_pos, strcmp(hash,hm));

		if ((cmp = strcmp(hash,hm)) < 0) {
			DEBUG("pos1 = %d , pos2 = posm = %d",pos1,posm);
			pos2 = posm;
		}
		else if (cmp > 0) {
			DEBUG("pos1 = posm = %d, pos2 = %d",posm,pos2);
			pos1 = posm;
		}
		else {
			DEBUG("found: %s == %s rec_pos: %d", hash, hm, rec_pos);
			break;
		}
	}

	if (rec_pos == -1) {
		ERROR("can't find %s in %s/%s/h",hash,base,k2s(key));
		exit(1);
	}
	DEBUG("hash: %s/%s/h %s rec_pos: %d",base,k2s(key), hash,rec_pos);
	fseek(fv,rec_pos,SEEK_SET);
	if (fscanf(fv,"%s",val_buf) != 1) {
		ERROR("dg/dump_record.c: val_from_hash(): fscanf(fv, ... val_buf) != 1. EXIT(1)");
		exit(1);
	}
	DEBUG("v:hash:%s:",val_buf);
	if ((c = fgetc(fv)) != ' ') {
		ERROR("no ' ' after %s in %s/%s/v",hash,base,k2s(key));
		exit(1);
	}
	for (p = val_buf; (*p = fgetc(fv)) != '\0'; p++){};
	DEBUG("v:val:%s:",val_buf);
	return val_buf;
}

void
dump_tree_dg (struct node *no, int ident) {
	int ik, i;
	char *v;
	int first_child = 1;
	struct node *par;
	for (i = 0; i < ident; i++)
		putchar(' ');
	if (!no) {
		printf("[NULL]\n");
		return;
	}
	if ((par = no->par)) {
		for (i = 0; i < par->nch; i++) {
			if (par->ch[i]->type == no->type) {
				first_child = (par->ch[i] == no);
				break;
			}
		}
	}
	if (k2noval(no->type)) {
		printf(first_child ? "%s;\n" : "%s.;\n",k2s(no->type));
	}
	else {
		ik = k2ik(no->type);
		if (ik != -1){ // key not found
			for (i = 0; i < no->nval; i++) {
				v = val_from_hash(no->type,no->val[i]);
				printf(first_child ? "%s = %s;\n" : "%s.= %s;\n",keys[ik].s,v);
				DEBUG("(hash): %s",no->val[i]);
			}
		}
	}
	for (i = 0; i < no->nch; i++)
		dump_tree_dg(no->ch[i], ident+1);
	if (no->type == _RECORD_)
		printf("*E\n");
	return;
}




struct node*
find_par(enum keytype key, struct node* no) {
	if (!no)
		return NULL;
	if (no->type == k2par(key) || no->type == _RECORD_)
		return no;
	else
		return find_par(key,no->par);
}


char buf[1024];

int 
is_hash(char *s) {
	char *p = s;
	for (; *p; p++)
		if (!( (*p >= 'a' && *p <= 'f') || (*p >= '0' && *p <= '9') ))
			return 0;
	return 1;
}


int 
read_record(char* path) {
	FILE * f = fopen(path,"r");
	char *hash;
	if (!f) 
		return 0;
	st = _ex_record_;
	while (fscanf(f,"%s",buf) == 1) {
		DEBUG("buf:%s:\n",buf);
		if (buf[0] == '{' && buf[1] == '\0') {
			DEBUG(" skip { ");
		}
		else 
		if (buf[0] == '}' && buf[1] == '\0') {
			DEBUG(" skip } ");
		}
		else 
		if (buf[0] == 'v' && buf[1] == ':' && is_hash(buf+2)) {
			hash = buf+2;
			DEBUG("hash:%s",hash);
			switch (st) {
				case _val_:
					DEBUG("val_node(%s,%s), st -> _ex_", k2s(no_cur->type), hash);
					val_node(no_cur,hash);
					st = _ex_;
					break;
				default:
					ERROR(	"%s: unexpected 'v:%s'",path,hash);
					exit(1);
					break;
			}
		}
		else {
			DEBUG("key?:%s:",buf);
			if ((key = s2k(buf)) == _UNKNOWN_) {
				ERROR("unknown key:%s: EXIT(1)",buf);
				exit(1);
			}
			DEBUG("key: %d",(int)key);
			switch (st) {
				case _ex_record_:
					if (key == _RECORD_)  {
						DEBUG("... no_root = new_node_dg(...), st -> _inkey_");
						no_par = no_cur = no_root = new_node_dg(_RECORD_, NULL);
						st = _inkey_;
					}
					else {
						ERROR(
							"%s: %s read while RECORD { ... } expected",
							path, buf);
						exit (1);
					}
					break;
				case _ex_:
				case _inkey_:
					DEBUG(" st == _ex_ || _inkey_ ...");
					if ((no_par = find_par(key,no_cur))) {
						DEBUG("no_par found, no_cur = new_node_dg(%s,%s)",buf,k2s(no_par->type));
						no_cur = new_node_dg(key,no_par);
					}
					else {
						DEBUG("can't find parent ...");
						if (k2noval(key_par = k2par(key))) {
							DEBUG("%s parent == %s: noval",buf, k2s(key_par));
							if ((no_par = find_par(key_par,no_cur))) {
								DEBUG("no_cur = new(%s,%s) ...",k2s(key_par), k2s(no_par->type));
								no_cur = new_node_dg(key_par,no_par);
							}
							else {
								ERROR(
									"%s: can't find parent to insert %s",
									path, k2s(key_par)
								       );
								exit(1);
							}
							DEBUG("no_cur = new(%s,%s)",buf, k2s(no_cur->type));
							no_cur = new_node_dg(key,no_cur);
						}
						else {
							ERROR(
								"%s: can't find parent for %s, %d",
								path, k2s(key), key
							       );
							exit(1);
						}
					}
					if (k2noval(key)) {
						DEBUG("%s: noval, st -> _ex_",buf);
						st = _ex_;
					}
					else {
						DEBUG("st -> _val_");
						st = _val_;
					}
					break;
				default:
					ERROR(
						"%s: '%s' read while key expected",
						path, buf
					       );
					exit(1);
			}
		}
	}
	dump_tree_dg(no_root,0);
	free_tree_dg(no_root);
	fclose(f);
	return 1;
}

int 
main(int argc, char** argv) {
	int i;
	if (argc < 2) {
		printf("\nUsage: %s rec-id [ rec-id1 ... ]\n\n",argv[0]);
		return 1;
	}
	if (!base) {
		if (!(base = getenv("PPDS_BASE"))) {
			WARNING("no PPDS_BASE env. variable, using ./");
			base = curdir;
		}
		else {
			DEBUG("PPDS_BASE = %s\n",base);
		}
	}
	n_vh_files = 0;
	record_path = (char*)malloc(sizeof(char)*(strlen(base) + 50));
	val_file_path = (char*)malloc(sizeof(char)*(strlen(base) + 132));
	hash_file_path = (char*)malloc(sizeof(char)*(strlen(base) + 132));
	for (i = 1; i < argc; i++) {
		sprintf(record_path,"%s/records/%s",base,argv[i]);
		DEBUG("record file: %s\n",record_path);
		printf("RECORD = %s;\n",argv[i]);
		if (!read_record(record_path)) {
			ERROR("%s: can't read %s EXIT(1)",argv[0],record_path);
			return 1;
		}
	}
	for (i = 0; i < n_vh_files; i++) {
		fclose(vh_files[i].fv);
		fclose(vh_files[i].fh);
	}
	return 0;
}
