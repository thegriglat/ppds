begin transaction;

CREATE TABLE vy_data (
    ID integer PRIMARY KEY AUTOINCREMENT,
    ABBREVIATION TEXT,
    AT TEXT,
    "AS" TEXT,
    NAME TEXT,
    USAGE TEXT,
    COMMENT TEXT,
    'COMMENT-TEX' TEXT,
    DESCRIPTION TEXT,
    ORCODE INTEGER,
    QN_Q INTEGER,
    QN_BAR INTEGER,
    IDENT TEXT,
    Q INTEGER,
    MASS REAL,
    RC TEXT,
    RPP TEXT,
    CRPP TEXT,
    PN TEXT,
    SB TEXT,
    DEN TEXT,
    DCH TEXT,
    QUESTION TEXT,
    URL TEXT
);

CREATE TABLE vy_DD (ID INTEGER, ABBREVIATION TEXT);

CREATE TABLE vy_PP (ID INTEGER, ABBREVIATION TEXT);
CREATE TABLE vy_AC (ID INTEGER, ABBREVIATION TEXT);
CREATE TABLE vy_DE (ID INTEGER, ABBREVIATION TEXT, IDENT TEXT);
CREATE TABLE vy_RR (ID INTEGER, ABBREVIATION TEXT, IDENT TEXT);
CREATE TABLE vy_I (ID INTEGER, ABBREVIATION TEXT, 'AS' TEXT, URL TEXT);
CREATE TABLE vy_R (ID INTEGER, ABBREVIATION TEXT, IDENT TEXT, 'AS' TEXT, AT TEXT);
CREATE TABLE vy_P (ID INTEGER, BAR INTEGER, LEP INTEGER, S INTEGER, C INTEGER, B INTEGER, T INTEGER, ANTI INTEGER, HEAVY_NUC INTEGER, classname INTEGER, M REAL, 'I3' REAL, Q REAL, ORCODE INTEGER, ABBREVIATION TEXT);

CREATE UNIQUE INDEX vy_dd_abbreviation ON vy_DD(abbreviation);
CREATE UNIQUE INDEX vy_pp_abbreviation ON vy_PP(abbreviation);
CREATE UNIQUE INDEX vy_ac_abbreviation ON vy_ac(abbreviation);
CREATE UNIQUE INDEX vy_de_abbreviation_ident ON vy_de(abbreviation, IDENT);
CREATE UNIQUE INDEX vy_rr_abbreviation_ident ON vy_rr(abbreviation, IDENT);
CREATE UNIQUE INDEX vy_i_abbreviation_as_url ON vy_I(abbreviation, "as", url);
CREATE UNIQUE INDEX vy_r_abbreviation_ident_as_at ON vy_R(abbreviation, ident,"as", at);

CREATE INDEX vy_P_abbreviation ON vy_P(abbreviation);
CREATE INDEX vy_P_id ON vy_P(id);

CREATE TABLE dg_type (
    ID integer PRIMARY KEY AUTOINCREMENT,
    TYPE text NOT NULL
);

CREATE UNIQUE INDEX dg_type_type ON dg_type(type);

CREATE TABLE dg_value (
    ID integer PRIMARY KEY AUTOINCREMENT,
    TID integer NOT NULL,
    VALUE TEXT NOT NULL,
    FOREIGN KEY(TID) REFERENCES dg_type(tid)
);

CREATE INDEX dg_value_value ON dg_value(value);
CREATE INDEX dg_value_tid ON dg_value(tid);

CREATE TABLE dg_data (
    ID integer PRIMARY KEY AUTOINCREMENT,
    REC_ID INTEGER NOT NULL,
    PID integer NOT NULL,
    TID integer NOT NULL,
    VID integer,
    FOREIGN KEY(TID) REFERENCES dg_type(id)
    FOREIGN KEY(REC_ID) REFERENCES dg_data(id)
);

CREATE INDEX dg_data_pid ON dg_data(pid);
CREATE INDEX dg_data_recid ON dg_data(rec_id);
CREATE INDEX dg_data_tid ON dg_data(tid);
CREATE INDEX dg_data_vid ON dg_data(vid);

insert into dg_type (type) values ('A');
insert into dg_type (type) values ('ABS');
insert into dg_type (type) values ('ABS-TEX');
insert into dg_type (type) values ('AC');
insert into dg_type (type) values ('ACTION');
insert into dg_type (type) values ('ANSWER');
insert into dg_type (type) values ('AUTHORS');
insert into dg_type (type) values ('AV');
insert into dg_type (type) values ('B');
insert into dg_type (type) values ('BP');
insert into dg_type (type) values ('CB');
insert into dg_type (type) values ('CB-TEX');
insert into dg_type (type) values ('CCOD');
insert into dg_type (type) values ('CD');
insert into dg_type (type) values ('CD-TEX');
insert into dg_type (type) values ('CE');
insert into dg_type (type) values ('CE-TEX');
insert into dg_type (type) values ('CIT');
insert into dg_type (type) values ('COD');
insert into dg_type (type) values ('COL');
insert into dg_type (type) values ('COMP');
insert into dg_type (type) values ('CONF');
insert into dg_type (type) values ('CR');
insert into dg_type (type) values ('CR-TEX');
insert into dg_type (type) values ('CRW');
insert into dg_type (type) values ('CS');
insert into dg_type (type) values ('D');
insert into dg_type (type) values ('DCH');
insert into dg_type (type) values ('DD');
insert into dg_type (type) values ('DE');
insert into dg_type (type) values ('DEN');
insert into dg_type (type) values ('DK');
insert into dg_type (type) values ('ECM');
insert into dg_type (type) values ('ERROR');
insert into dg_type (type) values ('ES');
insert into dg_type (type) values ('EXPERIMENT');
insert into dg_type (type) values ('FSP');
insert into dg_type (type) values ('I');
insert into dg_type (type) values ('ILBL');
insert into dg_type (type) values ('INDEX');
insert into dg_type (type) values ('INP');
insert into dg_type (type) values ('INR');
insert into dg_type (type) values ('IRN');
insert into dg_type (type) values ('IRNW');
insert into dg_type (type) values ('KRD');
insert into dg_type (type) values ('KTO');
insert into dg_type (type) values ('L');
insert into dg_type (type) values ('NA');
insert into dg_type (type) values ('NU');
insert into dg_type (type) values ('OR');
insert into dg_type (type) values ('P');
insert into dg_type (type) values ('PART-PROP');
insert into dg_type (type) values ('PDGSC');
insert into dg_type (type) values ('PL');
insert into dg_type (type) values ('PLAB');
insert into dg_type (type) values ('PP');
insert into dg_type (type) values ('PPF');
insert into dg_type (type) values ('PR');
insert into dg_type (type) values ('PUBLISHED-PAPERS');
insert into dg_type (type) values ('Q2');
insert into dg_type (type) values ('QUESTION');
insert into dg_type (type) values ('R');
insert into dg_type (type) values ('RCOD');
insert into dg_type (type) values ('RD');
insert into dg_type (type) values ('RE');
insert into dg_type (type) values ('REAC-DATA');
insert into dg_type (type) values ('RECORD');
insert into dg_type (type) values ('RPP');
insert into dg_type (type) values ('RR');
insert into dg_type (type) values ('SB');
insert into dg_type (type) values ('SC');
insert into dg_type (type) values ('SPOKESMAN');
insert into dg_type (type) values ('SPP');
insert into dg_type (type) values ('T');
insert into dg_type (type) values ('TA');
insert into dg_type (type) values ('TA-TEX');
insert into dg_type (type) values ('TITLE-TEX');
insert into dg_type (type) values ('TP');
insert into dg_type (type) values ('TW');
insert into dg_type (type) values ('TY');
insert into dg_type (type) values ('URL');
insert into dg_type (type) values ('W2');

end transaction;